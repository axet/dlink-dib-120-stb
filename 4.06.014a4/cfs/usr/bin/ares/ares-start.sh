#!/bin/sh

insmod /usr/bin/ares/modules/arescrypt.ko
/usr/bin/ares/bin/thttpd -C /usr/bin/ares/etc/thttpd.conf
AUTH_URI=`sed -n -e 's/SERVERADDRESS.*= //p' /var/program/VERIMATRIX.INI | tr -d '\r\n'`
#AUTH_URI=`sed -n -e 's/.*<vm><comp>[^<]*<\/comp><server>//;s/<\/server>.*//p' /etc/sysmgmt/bak_config.xml | tr -d '\r\n'`
if [ -n "${AUTH_URI}" ]; then
/usr/bin/ares/bin/arescam -a "https://${AUTH_URI}" --suspend 45 -k -d
fi
