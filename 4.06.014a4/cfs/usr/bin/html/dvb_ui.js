
//If No Channels Go Scan Directly
if(dvb.channels.length<=0)
	document.location.href="setup/scan.htm";

 var chinfo_timer=null;
 var sch_timer=null;
 var key_quick_lock=false;
 var realtime_recstate=false;
 var init_timer=null;
 var sch_check_timer=null;
 var chno_switch_len=0;
 var chno_switch=0;
 var chno_switch_timer=null;
 var chno_switch_lock=false;

function clearChInfo()
{
	stb.print("clearChInfo()");
	if(document.getElementById("chinfo_top").className!="info_top0")
		document.getElementById("chinfo_top").className="info_top0";
}

function updateChInfo(ch_id)
{
	stb.print("updateChInfo()");
	try{
	if(ch_id>0)
	{
		clear_chno_switch();
		document.getElementById("chinfo_top").getElementsByTagName("span")[0].innerText="";
		document.getElementById("chinfo_top").getElementsByTagName("span")[1].innerText="";

		for(var i=0;i<dvb.channels.length;i++)
		{
			if(dvb.channels[i].ch_id==ch_id)
			{
				//document.getElementById("chinfo_top").getElementsByTagName("span")[1].lang=dvb.channels[i].ch_name_lang;
				document.getElementById("chinfo_top").getElementsByTagName("span")[1].innerText=dvb.channels[i].ch_name.substr(0,18);
				document.getElementById("chinfo_top").getElementsByTagName("span")[0].innerText=dvb.channels[i].ch_no;
				EPGlocateCh(i);
			}
		}

		if(document.getElementById("chinfo_top").className!="info_top1")
			document.getElementById("chinfo_top").className="info_top1";
		
		if(chinfo_timer)
			clearTimeout(chinfo_timer);

		chinfo_timer=setTimeout(clearChInfo,3000);//3 secs
	}
	}catch(err){stb.print("ERR:"+err)}
	stb.print("updateChInfo() exit");
}

function updateChNoSwitch()
{
	var output="";
	for(var i=0;i<chno_switch_len-1;i++)
	{
		if(chno_switch<Math.pow(10,chno_switch_len-1-i))
			output=output+"0";
	}

	document.getElementById("chinfo_top").getElementsByTagName("span")[0].innerText=output+chno_switch;
	document.getElementById("chinfo_top").getElementsByTagName("span")[1].innerText="";

	if(document.getElementById("chinfo_top").className!="info_top1")
		document.getElementById("chinfo_top").className="info_top1";

	if(chinfo_timer)
		clearTimeout(chinfo_timer);

	chinfo_timer=setTimeout(clearChInfo,3000);//3 secs
}

function clear_chno_switch(no_reset)
{
	if(chno_switch_timer)
	{
		clearTimeout(chno_switch_timer);
		chno_switch_timer=null;
		chno_switch_lock=false;
		if(!no_reset)
		{
			chno_switch=0;
			chno_switch_len=0;
		}
	}
}

function chno_switch_timeout(){
	if(chno_switch>0)
	{
		chno_switch_lock=true;
		for(var i=0;i<dvb.channels.length;i++)
		{
			if(dvb.channels[i].ch_no==chno_switch)
			{
				if(exitRecConfirm())
				{
					dvb.play_channel(dvb.channels[i].ch_id);
					updateChInfo(dvb.channels[i].ch_id);
					EPGlocateCh(i);
				}
			}
		}
		chno_switch=0;
		chno_switch_len=0;
		chno_switch_lock=false;
		i=null;
	}
}

function ch_no_switch_add_key(num)
{
	if(!chno_switch_lock)
	{
		clear_chno_switch(true);
		chno_switch=((chno_switch)*10+num)%1000;
		chno_switch_len=chno_switch_len+1;
		chno_switch_len=(chno_switch_len>3)?3:chno_switch_len;
		updateChNoSwitch();
		if(chno_switch_timer)
			clearTimeout(chno_switch_timer);
		chno_switch_timer=setTimeout(chno_switch_timeout,1700);
	}
}

function key_quick_unlock()
{ //In Order to prevent Some Keys be pressed quickly,add a shorttime-lock between key & key
  //During lock key  won't be processed
  key_quick_lock=false;
}
function exitRecConfirm(skip_check_schedule){
	var isRecording=(player.recstate==2)?true:false;

	if(!skip_check_schedule)
	{
		isRecording=isRecording|(player.sch_recstate==2);
		//Check Prepare Rec 
		var ch_id=dvb.schprepare_ch_id;
		var ch_idx=locateCh(ch_id);

		if(ch_idx>=0)
		{//Check Is Now Playing,If yes Lock it.
			if(dvb.channels[ch_idx].isplaying)
				isRecording=true;
		}
	}

	var exitconfirmed=!isRecording;

	if(isRecording)
	{
		exitconfirmed=(confirm("Stop Record?"));//Yes -set no recording No -set recording
		if((!skip_check_schedule)&&(dvb.schprepare_ch_id>0)&&(exitconfirmed))
		{
			//Check Prepare Rec 
			var ch_id=dvb.schprepare_ch_id;
			var ch_idx=locateCh(ch_id);
			if(ch_idx>=0)
			{
				pvr.del_prepare_schedule();
				stb.print("Del Schedule Record");
			}
		}
	}

	return exitconfirmed;
}

function submitRec(){
	player.rec();
}

function keyListener(event) {
	var key = event.keyCode ? event.keyCode : event.which;
	var handled=true;

	    switch(key)
	    {
	    	//Numeric Key
	    	case 48:case 49:case 50:case 51:case 52:case 53:case 54:case 55:case 56:case 57:
	    		try{
	    		ch_no_switch_add_key(key-48);//Convert to Real number
	    		}catch(err){
	    			stb.print("ERROR!");
	    			stb.print("err:"+err);
	    		}finally{
	    		}
	    		break;
			//Arrow Key
	    	case 0x25:case 0x26:case 0x27:case 0x28:
				if(!EPGIsOn())
		    		handled=(document.getElementById("msgbox").className=="msgbox0")?true:false;
		    	else
		    		handled=false;
	    		break;
	    	case 0x1A0:/* RECORD */
				//Check HD Worked?
				if(stb.hd_exist)
				{
					if(player.sch_recstate==2)//Schedule Recording
					{
						if(exitRecConfirm())
						{
							player.stop_sch_rec();
							key_quick_lock=true;
							setTimeout(key_quick_unlock,1000);
						}
					}
					else if((player.status==4)&&(!key_quick_lock))/* not pause or stopping or stopped*/
					{
						if(document.getElementById("recicon").className=="rec1")
							document.getElementById("recicon").className="rec0";
						else
							document.getElementById("recicon").className="rec1"

						setTimeout(submitRec,1000);

						key_quick_lock=true;
						setTimeout(key_quick_unlock,1000);
					}
				}
	    		break;
	    	case 0x1CA:// EPG
			case 101:
			case 0x20:
				if(key==0x20)
				{
					if(!event.ctrlKey)
						break;
				}
	    		if(!key_quick_lock)
	    		{
					key_quick_lock=true;
		    		EPGShowHide();
					setTimeout(key_quick_unlock,1000);
	    		}else stb.print("Quick Key Lock");
	    		break;
	    	case 0x21:/* PAGE UP*/
	    		stb.print("PAGE UP");
	    		try{
	    		if(EPGIsOn())
		    	{
		    		EPG_PAGE_UPDOWN(0);
		    	}
		    	else
	    		{
					if(exitRecConfirm())
					{
						stb.print("Channel UP");
				   		var newchid=dvb.channel_up();
			    		updateChInfo(newchid);
			   		}
	    		}
	    		}catch(err){
	    			stb.print("Error Occurs");
	    			stb.print("ERR:"+err);
	    		}finally{
	    			stb.print("Page down done");
	    		}
	    		break;
	    	case 0x22:/* PAGE DOWN*/
	    		stb.print("PAGE DOWN");
	    		try{
	    		if(EPGIsOn())
		    	{
		    		EPG_PAGE_UPDOWN(1);
		    	}
		    	else
	    		{
					if(exitRecConfirm())
					{
						stb.print("Channel DOWN");
				   		var newchid=dvb.channel_down();
				    	updateChInfo(newchid);
				   	}
	    		}
	    		}catch(err){
	    			stb.print("Error Occurs");
	    			stb.print("ERR:"+err);
	    		}finally{
	    			stb.print("Page down done");
	    		}
	    			
	    		break;
            case 0x1cc:/* subtitle */
                stb.print("a");
                if (stb.subtitle)
                    stb.subtitle=0;
                else
                    stb.subtitle=1;
                stb.print("b");
                break;
			case 0x0305:/* BACK */
				if(exitRecConfirm())
				{
		    		var newchno=dvb.channel_jump();
			    	updateChInfo(newchno);
		    	}
				break;
	    	default:
	    		handled=false;
	    		break;
	    }
	if(handled)
		event.preventDefault();
	return !handled;
}

function show_status_icon()
{	//Check Recording or Not
	var state=(player.recstate==2)?1:0;
	document.getElementById("recicon").className="rec"+state;
}

function statechange_callback(e)
{/* On State change callback */
	show_status_icon();
}

function exitDVB()
{
	if(sch_check_timer)
		clearInterval(sch_check_timer);

	clear_chno_switch();

	dvb.save_last_ch_id();
	player.stop();
}

function locateCh(ch_id)
{
	var ch_idx=-1;
	if(ch_id>0)
	{
		for(var i=0;i<dvb.channels.length;i++)
		{
			if(dvb.channels[i].ch_id==ch_id)
				ch_idx=i;
		}
	}
	return ch_idx;
}

function forceSwitch(){//Force to Switch Prepare Record Channel Automatically
	stb.print("forceSwitch( START )");
	//var now=new Date();
	//stb.print("AutoSwitch-"+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds());
	var ch_id=dvb.schprepare_ch_id;
	var ch_idx=locateCh(ch_id);

	if(ch_idx>=0)
	{
		stb.print("Schedule Rec Channel Found CH:"+dvb.channels[ch_idx].ch_no);
		if(!dvb.channels[ch_idx].isplaying)
		{
			clear_chno_switch();
			dvb.play_channel(ch_id);
			updateChInfo(ch_id);
		}
	}
	ch_id=null;
	ch_idx=null;
	stb.print("forceSwitch( EXIT )");
}

function scheduleTimer(){//Msgbox Confirm Timeout
	hideMsgbox();
	forceSwitch();
}

function hideMsgbox(){
	document.getElementById("msgbox").className="msgbox0";
	EPGinitFocus();//Function in 'epg_ui.js'
}
function showMsgbox(text){
	document.getElementById("msgbox").getElementsByTagName("TD")[1].innerText=text;
	document.getElementById("msgbox").className="msgbox1";
	document.getElementById("btnok").focus();
}

function processMsgbox(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0:
		break;
		case 1://Click
		if(idx==1)//Cancel Record
		{
			if(sch_timer)
			{
				stb.print("Cancel Schedule Record");
				pvr.del_prepare_schedule();
				clearTimeout(sch_timer);
				sch_timer=null;
			}
		}
		hideMsgbox();
		break;
	}
}

function schRecConfirm()
{	//AutoSwitch Confirm
	var ch_id=dvb.schpredict_ch_id;
	var ch_idx=locateCh(ch_id);
	if((ch_id>0)&&(ch_idx>=0))
	{
		showMsgbox(GetLangStr(STR_TYPE_CONFIRM,"SCH_REC")+dvb.channels[ch_idx].ch_no+GetLangStr(STR_TYPE_CONFIRM,"SCH_REC2"));
	}
}

function schedulePrepare(){
	stb.print("Schedule Record Prepare Detected.");
	//Auto Switch to Prepare Channel After 40 Seconds less.
	if(sch_timer)
	{
		clearTimeout(sch_timer);
		sch_timer=null;
	}
	sch_timer=setTimeout(scheduleTimer,55*1000);

	var now=new Date();
	//stb.print("Prepare:"+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds());
	schRecConfirm();
}

function initChannel(){
	if(player.status==6)
	{
		init_timer=setTimeout(initChannel,1000);
		stb.print("Waiting Previous Action to Stop OK");
	}
	else
	{
		//Use First Channel as default
		var ch_id=dvb.channels[0].ch_id;

		if(locateCh(dvb.schrec_ch_id)>=0)//Check Schedule Recing Channel
			ch_id=dvb.schrec_ch_id;
		else if(locateCh(dvb.schprepare_ch_id)>=0)//Check Prepare Schedule Recing Channel
			ch_id=dvb.schprepare_ch_id;
		else if(locateCh(dvb.last_ch_id)>=0)//Check Last View Channel
			ch_id=dvb.last_ch_id;

		clear_chno_switch();
		dvb.play_channel(ch_id);
		updateChInfo(ch_id);
	}
}

function initPage() 
{
	//document.getElementById("msgbox").lang=LANG_NAME;
	document.getElementById("msgbox").getElementsByTagName("TD")[0].innerText=GetLangStr(STR_TYPE_TEXT,"SCH_REC_NOTIFY");
	document.getElementById("btnok").addEventListener("click", function(event){processMsgbox(event,-1,1)}, false);
	document.getElementById("btnok").innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_OK");
	document.getElementById("btnno").addEventListener("click", function(event){processMsgbox(event,1,1)}, false);
	document.getElementById("btnno").innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_CANCEL");
    document.addEventListener("keypress", keyListener, false);
    document.addEventListener("scheduleprepare", schedulePrepare, false);
   	stb.menutype=3;/* Set as DVB */
   	document.addEventListener("playerstatechange",statechange_callback,false);/* Set Callback*/
	initEPG();
	player.resize(0,0,720,480);
	init_timer=initChannel();
	sch_check_timer=setInterval(forceSwitch,3000);
}

window.onload = initPage;
window.onunload=exitDVB;
