﻿try{
if(typeof(stb)=='undefined')
	var stb=new Stb();
if(typeof(device)=='undefined')
	var device=new Device();
if(typeof(network)=='undefined')
	var network=new Network();
if(typeof(webbrowser)=='undefined')
	var webbrowser=new Webbrowser();
if(typeof(drm)=='undefined')
	var drm=new Drm();
if(typeof(player)=='undefined')
	var player=new Player();
}catch(err){
	alert("Error:"+err)
}finally{
	stb.print("Continue without break");
}

    var STR_TYPE_MENU=0;
    var STR_TYPE_TITLE=1;
    var STR_TYPE_BUTTON=2;
    var STR_TYPE_TEXT=3;
    var STR_TYPE_CONFIRM=4;
    var STR_TYPE_ERROR=5;

 STR_TYPE_MAP=new Array("UI_MENU_","UI_TITLE_","UI_BUTTON_","UI_TEXT_","UI_CONFIRM_","UI_ERROR_");

//For Browser Display MultiLanguage Text Locale String 
var FULLNAME_ARR=new Array("en","zh-TW","zh-CN");
var LANG_NAME="en";//reference upper FULLNAME_ARR
//Look Reference galio/src/unicode/languages.h

//Config
var LANG_MAP=new Array("en","tw","cn");
var LANG_TEXT=new Array("English","繁體中文","简体中文");
//Current Language 
//so we can't temporary change it 
//when using wizard or something else
var LANG_CUR=(stb.language)?stb.language:"en";

function LANGINIT()
{
	for(var i=0;i<LANG_MAP.length;i++)
	{if(LANG_CUR==LANG_MAP[i])	LANG_NAME=FULLNAME_ARR[i];}
}
LANGINIT();

// Setting Pages
// if you don't enabled it ,please leavel blank in "" rather than delete it...
var UI_PAGES = {
    MENU_HOME:		"main.htm",
    MENU_VOD:		"vod.htm",
    MENU_IPTV:		"iptv.htm",
    MENU_DVB:		"dvb.htm",
    MENU_PVR:		"pvr.htm",
    MENU_SCHEDULE:	"sch.htm",
    MENU_REC:		"rec.htm",
    MENU_PVRLIST:	"pvrlist.htm",
    MENU_MEDIA:		"player.htm",
    MENU_PICVIEWER:	"pic.htm"
};

var UI_MENU_EN={
    MENU_HOME:		"Home",
    MENU_VOD:		"My Video",
    MENU_IPTV:		"My Channel",
    MENU_DVB:		"My TV",
    MENU_PVR:		"My Recorder",
    MENU_SETTING:	"Settings",
    MENU_SCHEDULE:	"Schedule List",
    MENU_REC:		"Manual Record",
    MENU_PVRLIST:	"Recorded",
    MENU_MEDIA:		"Player",
    MENU_PICVIEWER:	"Photo Viewer"
}

var UI_MENU_TW={
    MENU_HOME:		"首頁",
    MENU_VOD:		"隨選視訊",
    MENU_IPTV:		"網路頻道",
    MENU_DVB:		"數位電視",
    MENU_PVR:		"影音錄製",
    MENU_SETTING:	"系統設定",
    MENU_SCHEDULE:	"預約錄製列表",
    MENU_REC:		"手動錄製節目",
    MENU_PVRLIST:	"已錄製的節目",
    MENU_MEDIA:		"音樂播放器",
    MENU_PICVIEWER:	"圖片瀏覽"
}

var UI_MENU_CN={
    MENU_HOME:		"主页",
    MENU_VOD:		"随选视讯",
    MENU_IPTV:		"网络频道",
    MENU_DVB:		"数码电视",
    MENU_PVR:		"音视频录制",
    MENU_SETTING:	"装置组态",
    MENU_SCHEDULE:	"预约录制清单",
    MENU_REC:		"手动录制节目",
    MENU_PVRLIST:	"已录制的节目",
    MENU_MEDIA:		"音频播放器",
    MENU_PICVIEWER:	"图像播放器"
}

var UI_TITLE_EN={
	REC_TYPE:"Type",
	REC_CH:"Channel",
	REC_DATE:"Date",
	REC_TIME:"Rec Time",
	REC_MONTH:"Month",
	REC_DAY:"Day",
	REC_HOUR:"Hour",
	REC_MIN:"Minute",
	REC_DURATION:"Duration",
	REC_CYCLE:"Cycle",
	REC_TIMES:"Times"
}

var UI_TITLE_TW={
	REC_TYPE:"種類",
	REC_CH:"頻道",
	REC_DATE:"日期",
	REC_TIME:"錄製時間",
	REC_MONTH:"月",
	REC_DAY:"日",
	REC_HOUR:"時",
	REC_MIN:"分",
	REC_DURATION:"長度",
	REC_CYCLE:"循環",
	REC_TIMES:"錄製次數"
}

var UI_TITLE_CN={
	REC_TYPE:"种类",
	REC_CH:"频道",
	REC_DATE:"日期",
	REC_TIME:"录制时间",
	REC_MONTH:"月",
	REC_DAY:"日",
	REC_HOUR:"时",
	REC_MIN:"分",
	REC_DURATION:"长度",
	REC_CYCLE:"循环",
	REC_TIMES:"录制次数"
}

var UI_BUTTON_EN={
    BTN_OK:"OK",
    BTN_CANCEL:"CANCEL",
    BTN_YES:"YES",
    BTN_NO:"NO",
    BTN_PREV:"BACK",
    BTN_NEXT:"NEXT",
    BTN_FINISH:"FINISH",
    BTN_HOME:"HOME",
    BTN_CONNECT:"CONNECT"
}

var UI_BUTTON_TW={
    BTN_OK:"確定",
    BTN_CANCEL:"取消",
    BTN_YES:"是",
    BTN_NO:"否",
    BTN_PREV:"上一頁",
    BTN_NEXT:"下一頁",
    BTN_FINISH:"完成",
    BTN_HOME:"首頁",
	BTN_CONNECT:"連線"
}

var UI_BUTTON_CN={
    BTN_OK:"确定",
    BTN_CANCEL:"取消",
    BTN_YES:"是",
    BTN_NO:"否",
    BTN_PREV:"上一页",
    BTN_NEXT:"下一页",
    BTN_FINISH:"完成",
    BTN_HOME:"主页",
    BTN_CONNECT:"连接"
}

var UI_TEXT_EN={
	DAY_0:"Sunday",
	DAY_1:"Monday",
	DAY_2:"Tuesday",
	DAY_3:"Wednesday",
	DAY_4:"Thursday",
	DAY_5:"Friday",
	DAY_6:"Saturday",
	TIME_UNIT_YEAR:"/",
	TIME_UNIT_MONTH:"/",
	TIME_UNIT_DAY:"",
	TIME_UNIT_HOUR:" hr.",
	TIME_UNIT_MIN:" mins.",
	CYCLE_0:"Once",
	CYCLE_1:"Daily",
	CYCLE_2:"Weekly",
	CHANNEL_UNKNOWN:"Unknown",
	PROGRAM_UNKNOWN:"Unknown Program",
	PROGRAM_END:"The End",
	EPG_TIPS_CH:"ENTER:Watch Channel&nbsp;&nbsp;&nbsp;&nbsp;RIGHT:Program Schedule",
	EPG_TIPS_PG:"LEFT:Channel list&nbsp;&nbsp;PageUp/Down&nbsp;&nbsp;ENTER:Rec&nbsp;&nbsp;RIGHT:Info",
	EPG_TIPS_PGINFO:"LEFT:Hide Info",
	EPG_TIPS_NAVDAY:"LEFT:Previous Day&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RIGHT:Next Day",
	EPG_WAITING:"EPG is coming soon...",
	TIPS_PLAYLIST_ITEM:"ENTER:Play&nbsp;&nbsp;DELETE:Remove&nbsp;&nbsp;Slow REW:Move Up&nbsp;&nbsp;Slow FF:Move Down",
	TIPS_FILELIST_GOPARENT:"ENTER:Go to up folder",
	TIPS_FILELIST_DIR:"ENTER:Switch to this folder",
	TIPS_FILELIST_FILE:"ENTER:Add to playlist",
	TIPS_CTRL_DEVICE:"Select Device",
	TIPS_CTRL_FUNC:"Clear Playlist",
	TIPS_CTRL_REPEAT0:"Set up Repeat",
	TIPS_CTRL_REPEAT1:"Cancel Repeat",
	TIPS_CTRL_ADD:"Add Files",
	TIPS_CTRL_FF:"Fast Froward",
	TIPS_CTRL_REW:"Rewind",
	TIPS_CTRL_PLAY:"Play",
	TIPS_CTRL_PAUSE:"Pause",
	TIPS_CTRL_STOP:"Stop Playing",
	DEV_USB:"USB Device",
	DEV_USB_WAIT:"Please Insert USB Flashdisk",
	DEV_INT_HD:"Internal Disk",
	SCH_REC_NOTIFY:"Schedule Record Remind"
}

var UI_TEXT_TW={
	DAY_0:"星期日",
	DAY_1:"星期一",
	DAY_2:"星期二",
	DAY_3:"星期三",
	DAY_4:"星期四",
	DAY_5:"星期五",
	DAY_6:"星期六",
	TIME_UNIT_YEAR:"年",
	TIME_UNIT_MONTH:"月",
	TIME_UNIT_DAY:"日",
	TIME_UNIT_HOUR:" 小時",
	TIME_UNIT_MIN:" 分",
	CYCLE_0:"只錄製一次",
	CYCLE_1:"每日錄製",
	CYCLE_2:"每個禮拜錄製",
	CHANNEL_UNKNOWN:"未知頻道",
	PROGRAM_UNKNOWN:"節目未知",
	PROGRAM_END:"影片結束",
	EPG_TIPS_CH:"ENTER:切換頻道  →:顯示頻道節目表",
	EPG_TIPS_PG:"→:顯示節目的相關資訊",
	EPG_TIPS_PGINFO:"←:隱藏節目相關資訊",
	EPG_TIPS_NAVDAY:"←:往前一天　　　→:往後一天",
	EPG_WAITING:"電子節目表取得中..",
	TIPS_PLAYLIST_ITEM:"ENTER:播放&nbsp;&nbsp;DELETE:刪除&nbsp;&nbsp;Slow REW:上移&nbsp;&nbsp;Slow FF:下移",
	TIPS_FILELIST_GOPARENT:"ENTER:回上層資料夾",
	TIPS_FILELIST_DIR:"ENTER:切換到資料夾",
	TIPS_FILELIST_FILE:"ENTER:加入播放清單",
	TIPS_CTRL_DEVICE:"選取裝置",
	TIPS_CTRL_FUNC:"清除播放清單",
	TIPS_CTRL_REPEAT0:"取消重復播放",
	TIPS_CTRL_REPEAT1:"設定重復播放",
	TIPS_CTRL_ADD:"加入檔案",
	TIPS_CTRL_FF:"快轉",
	TIPS_CTRL_REW:"倒轉",
	TIPS_CTRL_PLAY:"開始播放",
	TIPS_CTRL_PAUSE:"暂停",
	TIPS_CTRL_STOP:"停止播放",
	DEV_USB:"USB 磁碟",
	DEV_USB_WAIT:"請插入 USB 隨身碟",
	DEV_INT_HD:"內建硬碟",
	SCH_REC_NOTIFY:"預約錄影提醒"
}

var UI_TEXT_CN={
	DAY_0:"星期日",
	DAY_1:"星期一",
	DAY_2:"星期二",
	DAY_3:"星期三",
	DAY_4:"星期四",
	DAY_5:"星期五",
	DAY_6:"星期六",
	TIME_UNIT_YEAR:"年",
	TIME_UNIT_MONTH:"月",
	TIME_UNIT_DAY:"日",
	TIME_UNIT_HOUR:" 小时",
	TIME_UNIT_MIN:" 分",
	CYCLE_0:"只录制一次",
	CYCLE_1:"每日录制",
	CYCLE_2:"每个礼拜录制",
	CHANNEL_UNKNOWN:"未定频道",
	PROGRAM_UNKNOWN:"未定",
	PROGRAM_END:"影片结束",
	EPG_TIPS_CH:"ENTER:切换频道　　→:显示频道节目表",
	EPG_TIPS_PG:"→:显示节目的相关资讯",
	EPG_TIPS_PGINFO:"←:隐藏节目相关资讯",
	EPG_TIPS_NAVDAY:"←:往前一天　　→:往後一天",
	EPG_WAITING:"电子节目表取得中..",
	TIPS_PLAYLIST_ITEM:"ENTER:播放&nbsp;&nbsp;DELETE:删除&nbsp;&nbsp;Slow REW:上移&nbsp;&nbsp;Slow FF:下移",
	TIPS_FILELIST_GOPARENT:"ENTER:回上层文件夹",
	TIPS_FILELIST_DIR:"ENTER:切换到文件夹",
	TIPS_FILELIST_FILE:"ENTER:加入播放列表",
	TIPS_CTRL_DEVICE:"选择装置",
	TIPS_CTRL_FUNC:"清除播放清单",
	TIPS_CTRL_REPEAT0:"取消重复播放",
	TIPS_CTRL_REPEAT1:"设定重复播放",
	TIPS_CTRL_ADD:"加入文件",
	TIPS_CTRL_FF:"快进",
	TIPS_CTRL_REW:"快退",
	TIPS_CTRL_PLAY:"开始播放",
	TIPS_CTRL_PAUSE:"暂停",
	TIPS_CTRL_STOP:"停止播放",
	DEV_USB:"USB 快闪盘",
	DEV_USB_WAIT:"请插入 USB 快闪盘",
	DEV_INT_HD:"内置硬盘",
	SCH_REC_NOTIFY:"预约录影提醒"
}


var UI_CONFIRM_EN={
	DVB_NOCHANNEL:"No Channels Found, Scan Channels?",
	PVR_DEL:"Delete This Item?",
	PVR_RESUME:"Resume Last Play",
	SCH_REC:"There is a recording due to start on channel ",
	SCH_REC2:" in 90 seconds. Do you want to change to the record channel or cancel the record."
}

var UI_CONFIRM_TW={
	DVB_NOCHANNEL:"No Channels Found, Scan Channels?",
	PVR_DEL:"Delete This Item?",
	PVR_RESUME:"Resume Last Play",
	SCH_REC:"系統將於 90 秒後錄製第 ",
	SCH_REC2:" 頻道, 是否要切到欲錄影的頻道?"
}

var UI_CONFIRM_CN={
	DVB_NOCHANNEL:"No Channels Found, Scan Channels?",
	PVR_DEL:"Delete This Item?",
	PVR_RESUME:"Resume Last Play",
	SCH_REC:"系统将於 90 秒後录制第 ",
	SCH_REC2:" 频道, 是否要切到欲录影的频道?"
}

var UI_ERROR_EN={
	ERROR_UNKNOWN:"Unknown Error",
	SCH_REC_FATAL:"Record Engine Failed",
	SCH_REC_TIME:"Record time is invalid",
	SCH_REC_CONFLICT:"Time Conflict"
}

var UI_ERROR_TW={
	ERROR_UNKNOWN:"Unknown Error",
	SCH_REC_FATAL:"Record Engine Failed",
	SCH_REC_TIME:"Record time is invalid",
	SCH_REC_CONFLICT:"Time Conflict"
}

var UI_ERROR_CN={
	ERROR_UNKNOWN:"Unknown Error",
	SCH_REC_FATAL:"Record Engine Failed",
	SCH_REC_TIME:"Record time is invalid",
	SCH_REC_CONFLICT:"Time Conflict"
}

function GetLangStr(type,id)
{
    var rv="";
    var pending=LANG_CUR.toUpperCase();

    try {
        eval("rv="+STR_TYPE_MAP[type]+pending+"['"+id+"'];");
    } catch(err) {
        //Err Process
        stb.print('#FATAL ERR# No String Matched '+STR_TYPE_MAP[type]+pending+"["+id+"]");
        rv="";
    }
    finally{
        return rv;
    }
}

function GetUrlParamValue(str,param)
{
    if(str.length>2)
    {
    	var tmpstr=str.substr(1,str.length-1);
    	var tmparr=tmpstr.split('&');
    	for(var i=0;i<tmparr.length;i++)
    	{
    		if(tmparr[i].indexOf(param+'=')==0)
    		{
				var start=param.length+1;
				var end=tmparr[i].length-start;
				//stb.print("Param"+i+"["+param+"]:"+tmparr[i].substr(start,end));
    			return tmparr[i].substr(start,end);
    		}
    	}
    }
}
