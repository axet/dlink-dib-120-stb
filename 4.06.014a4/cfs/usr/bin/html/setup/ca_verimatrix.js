var MENULIST=new Array("CA_COMP","CA_SERVER","CA_VKS","CA_UPDATE");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);

var METHODLIST=new Array("CA_UPDATE_USB");//,"CA_UPDATE_NETWORK");
var monitor_timer=null;
var CONST_MAX_DEVICE=4;
var updatemethod=0;//default from USB

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_CA").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			var isinput=(CurItem<3)?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			break;
		case 1://Click
			var rv=true;
			if(direction>0)
				rv=saveSetting();

			if(rv)
				window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_CA_VERIMATRIX"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			var isinput=(CurItem<3)?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			CurItem=idx;
			var isinput=(CurItem<3)?true:false;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className=(isinput)?"tbedit_over":"tbnav_over";
			break;
		case 1://Click
			if(idx==3)
			{
				if(updatemethod==0)
				{
				//Copy License Key From USB to SYSTEM
				var rv=drm.drmkeyUpdate(0,0);
				alert(rv?'Sucess':'Fail');
				}
			}
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
		case 3://Mouse Over
			if(idx<3)
				listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
	}
}

function monitorUSB(){
	var certfile_exist=false;
	//Check USB certfile exist 
	//1st param-DRM type
	//2nd param-Check Method
	certfile_exist=drm.drmkeyCheck(0,0);
	if(certfile_exist)
	{
		listobj[listobj.length-1].style.visibility="inherit";
		listobj[listobj.length-2].style.visibility="inherit";
	}
	else
	{
		listobj[listobj.length-1].style.visibility="hidden";
		listobj[listobj.length-2].style.visibility="hidden";
		if(CurItem==(listobj.length/2)-1)
			initFocus();
	}
}

function saveSetting()
{
	var comp=listobj[1].getElementsByTagName("input")[0].value;
	var server=listobj[3].getElementsByTagName("input")[0].value;
	var vks=listobj[5].getElementsByTagName("input")[0].value;
	var rv=drm.setDRM(0,server,comp,vks);
	if(!rv)
		alert('Save Failed! Parameter Error');

	return rv;
}

function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
	if(monitor_timer)
		clearInterval(monitor_timer);
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	vallist.push(drm.drmComp);
	vallist.push(drm.drmServer);
	vallist.push(drm.drmVks);

	for(var i=0;i<(listobj.length/2);i++)
	{
		var obj
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		if(i<3)
		{
			obj=listobj[i*2+1].getElementsByTagName("input")[0];
			obj.value=vallist[i];
		}
		else
		{
			obj=listobj[2*i+1];
			if(i==3)
				obj.innerText=GetLangStr(STR_TYPE_TITLE,METHODLIST[updatemethod]);
		}

		obj.style.navRight="#"+navnextobj.id;
		obj.style.navLeft="#"+navprevobj.id;

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();

	//Start a monitor to check USB status
	monitor_timer=setInterval(monitorUSB,1000);
}

window.onload=initPage;
window.onunload=exitPage;
