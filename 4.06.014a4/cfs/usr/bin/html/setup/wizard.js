var titleobj,navprevobj,navnextobj;
var selectedIndex=0;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SYSTEM"];
var tourl=new Array(SETUP_PAGES["MENU_SETUP"]);
var WizardPages=new Array("setup_language","setup_network_mode","setup_network_static","setup_network_dhcp","setup_network_pppoe","setup_media");
var CurPageIdx=(LANG_MAP.length<=1)?1:0;

function SETTING(){
	this.language="en";
	this.netmode=0;//0-Static 1-DHCP 2-PPPOE
	this.netip=network.netip;
	this.netmask=network.netmask;
	this.netgateway=network.netgateway;
	this.netdns=network.netdns;
	this.netdns2=network.netdns2;
	this.pppoeUserName=network.pppoeUserName;
	this.pppoePassword=network.pppoePassword;
	this.vodServer=webbrowser.vodServer;
	this.iptvServer=webbrowser.iptvServer;
}
var WizardSetting=new SETTING;

function PagePrev(){
	document.getElementById(WizardPages[CurPageIdx]).style.display="none";
	navprevobj.focus();

	if(CurPageIdx==5)
	{
		CurPageIdx -= (3-WizardSetting.netmode);
	}
	else if((CurPageIdx==2)||(CurPageIdx==3)||(CurPageIdx==4))
	{
		CurPageIdx -=(WizardSetting.netmode+1);
	}
	else
		CurPageIdx--;

	if((CurPageIdx<0)||((LANG_MAP.length<=1)&&(CurPageIdx==0)))//Cancel Wizard
		window.location.replace(fromurl);
	else
	{
		navnextobj.className=(CurPageIdx==WizardPages.length-1)?"funcfinish":"funcnext";
		navnextobj.innerText=(CurPageIdx==WizardPages.length-1)?GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH"):GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
		document.getElementById(WizardPages[CurPageIdx]).style.display="";	
		initWizardPage();
	}
}

function PageNext(){
	saveSetting();

	document.getElementById(WizardPages[CurPageIdx]).style.display="none";
	navprevobj.focus();

	if(CurPageIdx==1)
	{
		CurPageIdx +=WizardSetting.netmode+1;
	}
	else if((CurPageIdx==2)||(CurPageIdx==3)||(CurPageIdx==4))//Network Type Sub Pages
	{
		CurPageIdx += 3-WizardSetting.netmode;
	}
	else
		CurPageIdx++;

	if(CurPageIdx==WizardPages.length)//All Done
		completeWizard();
	else
	{
		navnextobj.className=(CurPageIdx==WizardPages.length-1)?"funcfinish":"funcnext";
		navnextobj.innerText=(CurPageIdx==WizardPages.length-1)?GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH"):GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");

		document.getElementById(WizardPages[CurPageIdx]).style.display="";
		initWizardPage();
	}
}

function initWizardPage()
{
	var pageid=WizardPages[CurPageIdx];
	var mainobj=document.getElementById(pageid);
	var firstfocusitem=null;//Record Page's First focus item in order to focus on it
	var listobj;
	if(mainobj.getElementsByTagName("UL")[0]!=null)
	{
		listobj  = mainobj.getElementsByTagName("UL")[0].getElementsByTagName("LI");
	}

	//Reset CurItem
	CurItem=0;

	var titlelist=new Array();
	var vallist=new Array();

	switch(CurPageIdx)
	{
		case 0://Language
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"CHOOSE_LANGUAGE");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(LANG_TEXT[i]);}
			document.getElementById("l0").focus();
			break;
		case 1://Network Type
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"CHOOSE_NETWORK_TYPE");
			var MENULIST=new Array("NETWORK_STATIC","NETWORK_DHCP","NETWORK_PPPOE");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]).toUpperCase());}
			document.getElementById("n0").focus();
			break;
		case 2://Static IP
			menutitleobj.innerText="";
			var MENULIST=new Array("NETWORK_IP","NETWORK_MASK","NETWORK_GATEWAY","NETWORK_DNS1","NETWORK_DNS2");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			vallist.push(WizardSetting.netip);
			vallist.push(WizardSetting.netmask);
			vallist.push(WizardSetting.netgateway);
			vallist.push(WizardSetting.netdns);
			vallist.push(WizardSetting.netdns2);
			break;
		case 3://DHCP	
			menutitleobj.innerText="";
			document.getElementById("setup_network_dhcp").getElementsByTagName("DIV")[0].innerText=GetLangStr(STR_TYPE_TEXT,"DHCP_ENABLED");
			break;
		case 4://PPPOE
			menutitleobj.innerText="";
			var MENULIST=new Array("NETWORK_PPPOE_USER","NETWORK_PPPOE_PWD");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			vallist.push(WizardSetting.pppoeUserName);
			vallist.push(WizardSetting.pppoePassword);
		case 5://MEDIA
			menutitleobj.innerText="";
			var MENULIST=new Array("MEDIA_VOD","MEDIA_IPTV");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			vallist.push(WizardSetting.vodServer);
			vallist.push(WizardSetting.iptvServer);
			break;
	}

	if(typeof(listobj)=='object')
	{
		for(var i=0;i<listobj.length;i++)
		{
			listobj[i].getElementsByTagName("div")[0].innerText=titlelist[i];
			inp=listobj[i].getElementsByTagName("input")[0];
			if(inp&&(i<vallist.length))
			{
				eval('inp.addEventListener("focus", function(event){processItem(event,"'+inp.id+'",0)}, false);');
				inp.value=vallist[i];
				if(firstfocusitem==null)
					firstfocusitem=inp;
				inp.style.navRight="#"+navnextobj.id;
				inp.style.navLeft="#"+navprevobj.id;
			}
			else
			{
				eval('listobj[i].addEventListener("focus", function(event){processItem(event,"'+listobj[i].id+'",0)}, false);');
				eval('listobj[i].addEventListener("click", function(event){processItem(event,"'+listobj[i].id+'",1)}, false);');
				eval('listobj[i].addEventListener("keypress", function(event){processItem(event,"'+listobj[i].id+'",2)}, false);');
				if(firstfocusitem==null)
					firstfocusitem=listobj[i];
				listobj[i].style.navRight="#"+listobj[i].id;
				listobj[i].style.navLeft="#"+navprevobj.id;
			}

		}

		listobj[0].style.navUp="#"+listobj[listobj.length-1].id;
		listobj[listobj.length-1].style.navDown="#"+listobj[0].id;
	}

	if(typeof(firstfocusitem)=='object')
		firstfocusitem.focus();
	else
		navnextobj.focus();

	if((CurPageIdx==0)||(CurPageIdx==1))
	{
		navnextobj.className="funcnone";
		navnextobj.innerHTML="&nbsp;";
	}
}

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_WIZARD").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
	navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				PageNext();
			else
				PagePrev();
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var direction=1;//Default Right ->
	var tmp=eventflag;
	CurItem=parseInt(tmp.substr(1,10));
	var item_id_prefix=tmp.charAt(0);
	var input_prefix_filter=new Array("s","p","m");
	var PagePrefix=new Array("l","n","s","p","m");
	var isinputitem=false;

	if(item_id_prefix!=PagePrefix[CurPageIdx])
		return false;

	for(var i=0;i<input_prefix_filter.length;i++)
	{
		if(item_id_prefix.indexOf(input_prefix_filter[i])>=0)
			isinputitem=true;
	}

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(!isinputitem)
			{
				if(direction>0)
					PageNext();
			}
			break;
		case 2://KeyPress
			break;
	}
}

function completeWizard()
{
	try{
		//Save Setting to System
		stb.language=WizardSetting.language;
		webbrowser.vodServer=WizardSetting.vodServer;
		webbrowser.iptvServer=WizardSetting.iptvServer;
		//Save Relative Network Setting By Netmode
		switch(WizardSetting.netmode)
		{
			case 0://Static IP
				network.netip=WizardSetting.netip;
				network.netmask=WizardSetting.netmask;
				network.netgateway=WizardSetting.netgateway;
				network.netdns=WizardSetting.netdns;
				network.netdns2=WizardSetting.netdns2;
				break;
			case 1://DHCP
				break;
			case 1://PPPOE
				network.pppoeUserName=WizardSetting.pppoeUserName;
				network.pppoePassword=WizardSetting.pppoePassword;
				break;
		}
		network.netmode=WizardSetting.netmode;
	}catch(err){
		//When Save Error
	}finally{
		//Always do this
		window.location.replace(tourl[0]);
	}
}

function saveSetting(cancel_update_screen)
{
	var result=true;

	switch(CurPageIdx)
	{
		case 0://Language
			WizardSetting.language=LANG_MAP[CurItem];
			LANG_CUR=LANG_MAP[CurItem];
			LANGINIT();
			initLanguage();
			update();
			break;
		case 1://Network type
			WizardSetting.netmode=CurItem;
			break;
		case 2://Static IP
		    WizardSetting.netip=document.getElementById("s0").value;
		    WizardSetting.netmask=document.getElementById("s1").value;
		    WizardSetting.netgateway=document.getElementById("s2").value;
		    WizardSetting.netdns=document.getElementById("s3").value;
		    WizardSetting.netdns2=document.getElementById("s4").value;
			break;
		case 3://DHCP
			break;
		case 4://PPPoE
	        WizardSetting.pppoeUserName=document.getElementById("p0").value;
	        WizardSetting.pppoePassword=document.getElementById("p1").value;
			break;
		case 5://Media
	        WizardSetting.vodServer=document.getElementById("m0").value;
	        WizardSetting.iptvServer=document.getElementById("m1").value;
			break;
	}

	if(!cancel_update_screen)
		update();

	return result;
}


function initLanguage()
{
	titleobj.lang=LANG_NAME;
	menutitleobj.lang=LANG_NAME;

	navprevobj.lang=LANG_NAME;
	navnextobj.lang=LANG_NAME;

	//Becasue 0 is Language Switch,so don't set
	for(var i=1;i<WizardPages.length;i++)
	{document.getElementById(WizardPages[i]).lang=LANG_NAME;}
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title").getElementsByTagName("UL")[0].getElementsByTagName("LI")[0];

	menutitleobj = document.getElementById("menu_title").getElementsByTagName("UL")[0].getElementsByTagName("LI")[0];

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);

	initLanguage();//Initial Display Language and Setting


	for(var i=0;i<WizardPages.length;i++)
	{document.getElementById(WizardPages[i]).style.display="none";}
	//First Page
	document.getElementById(WizardPages[CurPageIdx]).style.display="";
	update();
	initWizardPage();
}

window.onload=initPage;
window.onunload=exitPage;