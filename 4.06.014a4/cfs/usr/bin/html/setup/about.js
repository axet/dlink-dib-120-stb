var MENULIST=new Array("ABOUT_MODEL","ABOUT_VER","ABOUT_BUILDDATE","ABOUT_HDFREE","ABOUT_NETWORK_TYPE","ABOUT_IPADDR","ABOUT_GWADDR","ABOUT_MACADDR","ABOUT_SN");
var SKIPLIST=new Array();
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SYSTEM"];
var tourl=new Array(SETUP_PAGES["MENU_SETUP"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_ABOUT").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
}

function saveSetting()
{
}

function initFocus()
{
	navprevobj.focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
//	listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navprevobj.style.navUp="#"+navprevobj.id;
	navprevobj.style.navDown="#"+navprevobj.id;
	navprevobj.style.navLeft="#"+navprevobj.id;
	navprevobj.style.navRight="#"+navprevobj.id;

	var WIRELIST=new Array("NETWORK_WIRELESS","NETWORK_WIRE");
	var TYPELIST=new Array("NETWORK_STATIC","NETWORK_DHCP","NETWORK_PPPOE");
	var HDLIST=new Array("BTN_YES","BTN_NO");

	var vallist=new Array();
	vallist.push(device.model);
	vallist.push(device.version);
	vallist.push(device.builddate);
	if(stb.hd_exist)
	{	var freesize=parseInt(stb.hd_freesize);
		tmp=parseInt(freesize/1024)+" GB";
		vallist.push(tmp);
	}
	else
		SKIPLIST.push("ABOUT_HDFREE");

	vallist.push(GetLangStr(STR_TYPE_TITLE,TYPELIST[network.netmode]));
	vallist.push(network.netip);
	vallist.push(network.netgateway);
	vallist.push(device.macaddr);
	vallist.push(device.sn);
	var shiftval=0;

	for(var i=0;i<listobj.length/2;i++)
	{
		var obj=listobj[i];
		var menuidx=i;
		
		if(menuidx>MENULIST.length-1-shiftval)
			break;

		for(var j=0;j<SKIPLIST.length;j++)
		{
			if(SKIPLIST[j]==MENULIST[menuidx])
				shiftval++;
		}

//		obj.getElementsByTagName("span")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i+shiftval])+":";
//		obj.getElementsByTagName("span")[1].innerText=vallist[i];
		listobj[2*i].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i+shiftval])+":";
		listobj[2*i+1].innerText=vallist[i];

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
	}
	
	while(i<(listobj.length/2))
	{
		listobj[i*2].style.visibility="hidden";
		listobj[i*2+1].style.visibility="hidden";
		i++;
	}

	update();
	initFocus();

}

window.onload=initPage;
window.onunload=exitPage;