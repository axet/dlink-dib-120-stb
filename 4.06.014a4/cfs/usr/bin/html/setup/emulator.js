
function SCHLIST(val){
	var text=new Array('News','Sport','Spider Man 3','Test','Matrix 3','Sky','Asia Travel','Space Attack','Science Knowledge','Mystery','Thriller','Talk Show');
	var tval=parseInt(Math.random()*10*val);
	this.prog_name=text[tval%text.length];
	this.rec_time=1200000000+1380200*val;
	this.ch_no=val+1;
	this.chan_type=0;//DVB
	this.duration=5*parseInt((Math.random()*10)+1);
	this.repeat=1;
	this.cycle=0;
	this.status=1;
}

function PVRFILEIDX(val){
	var text=new Array('News','Sport','Spider Man 3','Test','Matrix 3','Sky','Asia Travel','Space Attack','Science Knowledge','Mystery','Thriller','Talk Show');
	var tval=parseInt(Math.random()*10*val);
	this.ch_no=val;
	this.ch_name=text[tval%text.length];
	this.ch_length=(Math.random()*27)*300;
	this.ch_length -=  (this.ch_length%300);
}

function PVRFILELIST(val,basetime){
	var text=new Array('News','Sport','Spider Man 3','Test','Matrix 3','Sky','Asia Travel','Space Attack','Science Knowledge','Mystery','Thriller','Talk Show');
	var tval=parseInt(Math.random()*10*val);
	this.validinfo=true;
	this.idxHeader=new PVRFILEIDX(val);
	var tmp=basetime-100000*val;
	tmp -=parseInt(Math.random()*100000*val);
	this.rectime=tmp;
	this.ch_name=text[tval%text.length];
}

PVRFILELIST.prototype.del=function(){
	return true;
}
function PVR(){
	this.scheduleLists=new Array();
	this.pvrFiles=new Array();
	var now=new Date();
	now.setSeconds(0);
	now.setMinutes(0);
	var basetime=now.getTime()/1000;
	for(var i=0;i<30;i++)
	{
		var schlist=new SCHLIST(i);
		this.scheduleLists.push(schlist);
	}
}

PVR.prototype.initialize=function(){
	var now=new Date();
	now.setSeconds(0);
	now.setMinutes(0);
	var basetime=now.getTime()/1000;
	for(var i=0;i<30;i++)
	{
		var file=new PVRFILELIST(i,basetime);
		this.pvrFiles.push(file);
	}
}

PVR.prototype.print=function(msg){
}
PVR.prototype.set_schedule=function(idx, chan_type, ch_id, rec_time, duration, times, cycle){
	return true;
}
pvr=new PVR;

Array.prototype.get_program_idx_of_time=function(){
	return 0;
}
Array.prototype.get_program_idx_of_periodtime=function(){
	return 0;
}
Array.prototype.get_program_count_of_periodtime=function(){
	return 35;
}
function PROGRAM(chidx,idx,basetime){
	var text=new Array('News','Sport','Music','Test','Drama','Sky','Travel','Art','Science','Mystery','Thriller','Bubble');
	var times=new Array(30,40,60,90);
	var timeidx=parseInt(Math.random()*10);
	var tmpidx=parseInt(Math.random()*100);
	this.proglength=times[timeidx%times.length];
	this.ch_name=text[tmpidx%text.length];
	this.ch_no=chidx;
	this.starttime=basetime+this.proglength*60;
	this.isnowprogram=(idx==0)?true:false;
	this.isvalid=true;
}
function CHANNEL(idx){
	var chs=new Array('CNBC','MTV','Discovery','AXN','CNN','Bloomberg','STC','HBO','GoodTV','ESPN');
	var now=new Date();
	now.setMinutes(30);
	now.setSeconds(0);
	var btime=now.getTime()/1000;

	this.progs=new Array();
	for(var i=0;i<23;i++)
	{
		var pg=new PROGRAM(idx,i,btime);
		btime=pg.starttime;
		this.progs.push(pg);
	}
	
	this.ch_no=parseInt(Math.random()*100)+idx;
	this.ch_id=idx;
	this.ch_name=chs[idx%chs.length];
}

function DVB(){
	this.channels=new Array();
	for(var i=0;i<10;i++)
	{
		var ch=new CHANNEL(i+1);
		this.channels.push(ch);
	}
	this.ch_idx=0;
	this.dvb_type=0;
	this.dvb_t_param="5330000000,5930000000,60000000,6000000,0";
	this.dvb_s_param="0,1";
}
DVB.prototype.get_cur_scan_param_string=function(dvbtype){
	switch(dvbtype)
	{
		case 0:
			return this.dvb_t_param;
			break;
		case 2:
			return this.dvb_s_param;
			break;
	}
}
DVB.prototype.set_cur_scan_param=function(dvbtype,start,end,span,interval,mod_type){
	switch(dvbtype)
	{
		case 0:
			this.dvb_t_param=start+","+end+","+span+","+interval+","+mod_type;
			break;
		case 2:
			break;
	}
}
DVB.prototype.play_channel=function(newchno){
	for(var i=0;i<this.channels.length;i++)
	{
		if(this.channels[i].ch_no==newchno)
		{
			this.ch_idx=i;
			break;
		}
	}
	if(player.play_timer)
		player.stop();

	if(player.status==0)
	{
		player.status=3;
	}

	player.play_timer=setTimeout(player.timer_play,1000);
}

DVB.prototype.channel_up=function(){
	this.play_channel();
	this.ch_idx++;
	this.ch_idx %=this.channels.length;
	return this.channels[this.ch_idx].ch_no;
}
DVB.prototype.channel_down=function(){
	this.play_channel();
	this.ch_idx = this.ch_idx-1+this.channels.length;
	this.ch_idx %=this.channels.length;
	return this.channels[this.ch_idx].ch_no;
}

DVB.prototype.Scanning=function(){
	dvb.ScanPercent ++;

	this.ScanStatus=1;
	if(dvb.ScanPercent<100)
		setTimeout(dvb.Scanning,200);
	else
	{
		dvb.ScanStatus=0;
	}
}
DVB.prototype.get_scan_status=function(){
	return this.ScanStatus;
}
DVB.prototype.get_scan_percent=function(){
	return this.ScanPercent;
}
DVB.prototype.scan=function(){
	var me=this;
	this.ScanPercent=0;
	this.ScanStatus=1;
	this.update_timer=setTimeout(me.Scanning,1000);
}
dvb=new DVB;

function STB(){
	this.menutype=0;
	this.fwUpgradeStatus=0;
	this.fwUpdateUrl="http://2.60.9.1/firmware";
	this.ismodal=0;
	this.hd_freesize=113000;
	this.hd_exist=true;
	this.adminPassword="123";
	this.adminVerified=true;

	//Display Setting
	this.displaymode=0;
	this.outputtype=1;
	this.aspect=0;
	this.content=3;
	this.macrovision=0;
	

	//Video Setting
	this.contrast=11;
	this.brightness=11;
	this.sharpness=11;
	this.saturation=11;
	this.hue=11;
	
	this.timestringType=0;
	this.timeMethod=1;
	this.timeServer="";
	this.timeZone=69;
	this.language="en";
}
STB.prototype.contrast_add=function(direction){}
STB.prototype.brightness_add=function(direction){}
STB.prototype.sharpness_add=function(direction){}
STB.prototype.saturation_add=function(direction){}
STB.prototype.hue_add=function(direction){}
STB.prototype.voldown=function(){}
STB.prototype.volup=function(){}
STB.prototype.mute=function(){}
STB.prototype.setSystemTime=function(yy,mm,dd,hh,nn,ss){
alert('Set System Time:'+yy+"/"+mm+"/"+dd+" "+hh+":"+nn+":"+ss);
}
STB.prototype.ntpUpdate=function(){alert('NTP Update Called')}
STB.prototype.restoreDefault=function(){alert('Config Reset')}
STB.prototype.save_osd=function(){
	alert('OSD Saved');
}

STB.prototype.fwUpdating=function(){
	stb.fwUpgradeWritePercent ++;
	
	this.fwUpgradeStatus=0;
	if(stb.fwUpgradeWritePercent<100)
		setTimeout(stb.fwUpdating,200);
	else
	{
		stb.fwUpgradeStatus=8;
	}
		
}

STB.prototype.fwUpgrade=function(){
	var me=this;
	this.fwUpgradeDownloadPercent=0;
	this.fwUpgradeWritePercent=0;
	this.fwUpgradeStatus=7;
	this.update_timer=setTimeout(me.fwUpdating,1000);
}

STB.prototype.cancelFWUpgrade=function (){
	if(this.ismodal==1)
	{
		alert("Can't Cancel because writing now");
		return false;	
	}
	else
	{
		clearTimeout(this.update_timer);
		return true;
	}
}
STB.prototype.usb_path_get=function(idx){
	var tmp=parseInt(Math.random()*1000)%2;
	
	if((typeof(DEV_IDX)!='undefined')&&(DEV_IDX>0))
		tmp=1;

	if(tmp>0)
		return "/mnt/usb"+idx;
	else
		return "";
}
STB.prototype.print=function (msg){
//alert(msg);
}
STB.prototype.autologout=function (){
	stb.adminVerified=false;
}
stb=new STB;

function DEVICE(){
	this.sn="0123-4567";
	this.model="AST-1100-C1";
	this.version="V1.0.0.10";
	this.macaddr="00-0D-60-74-C6-A0";
	this.builddate="200803101400";
}
device=new DEVICE;

function WEBBROWSER()
{
	this.mainpage="";
	this.homepage="http://2.60.9.1";
	this.configpage="";
	this.httpProxy="proxy.alphanetworks.com:8080";
	this.vodServer="http://2.60.9.11/demo/vod.htm";
	this.iptvServer="http://2.60.9.11/demo/iptv.htm";
	this.dvbServer="";
	this.pvrServer="";
	this.bookmarks=new Array();
	this.bookmarks.push("http://www.google.com.tw");
	this.bookmarks.push("http://www.pchome.com.tw");
	this.bookmarks.push("http://www.yam.com.tw");
	this.bookmarks.push("http://www.yahoo.com");
	this.bookmarks.push("http://www.ebay.com");
	this.bookmarks.push("http://www.abc.com.123456789012345678901234567890");
	this.bookmarks.push("http://www.pchome.com.tw");
	this.bookmarks.push("http://www.pchome.com.tw");
	this.bookmarks.push("http://www.pchome.com.tw");
	this.drm_type=0;
	this.drmComp="Alphanetworks";
	this.drmServer="http://2.42.9.252:8031";
	this.drmVks="http://2.42.9.253:4096";
}
WEBBROWSER.prototype.getBookmark=function(idx){
	return this.bookmarks[idx-1];
}

WEBBROWSER.prototype.saveBookmark=function(idx,val){
	if(idx<=this.bookmarks.length)
	{
		this.bookmarks[idx-1]=val;
	}
}
WEBBROWSER.prototype.setDRM=function(dtype,server,comp,vks){
	switch(dtype)
	{
		case 0:
			this.drmComp=comp;
			this.drmServer=server;
			this.drmVks=vks;
			break;
	}
}
WEBBROWSER.prototype.drmkeyCheck=function(dtype,method){
	return true;
}
WEBBROWSER.prototype.drmkeyUpdate=function(dtype,method){
	return true;
}

webbrowser=new WEBBROWSER;

function NETWORK(){
	this.mac="00:10:18:01:B3:01";
	this.netip="2.60.9.11";
	this.netmask="255.0.0.0";
	this.netgateway="2.42.9.254";
	this.netdns="172.19.10.99";
	this.netdns2="168.95.1.1";
	this.netmode=0;
	this.wired=0;
	this.igmp=3;
	this.pppoeUserName="adsluser";
	this.pppoePassword="adslpwd";
}
network=new NETWORK;

function PLAYITEM(filename){
	this.filename=filename;
	this.title=filename;
	this.lang="zh-tw";
}

function PLAYER(){
	this.status=0;
	this.ch_type=0;
	this.win_x=0;
	this.win_y=0;
	this.win_w=720;
	this.win_h=480;
	this.url="";
	this.isfullscreen=false;
	this.recstate=0;
	this.speed=1;
	this.play_timer=null;
	this.tricktype=0;
	this.playlist=new Array();
}

PLAYER.prototype.onstatechange=function(){
}

PLAYER.prototype.response=function(){
}

PLAYER.prototype.resize=function(x,y,w,h){
	this.win_x=x;
	this.win_y=y;
	this.win_w=w;
	this.win_h=h;

	if((x==0)&&(y==0)&&(w==720)&&(h==480))
		this.isfullscreen=true;
	else	
		this.isfullscreen=false;
	
	//alert('player resize to ('+this.win_x+","+this.win_y+") "+this.win_w+"X"+this.win_h);
}
PLAYER.prototype.timer_play=function(){
	if(player.status==3)
		player.status=4;
	
	if(player.onstatechange)
	{
		player.onstatechange();
	}
}
PLAYER.prototype.play=function(url,delay){
	//alert("PLAYER GOT PLAY URL:"+url+((delay>0)?"\nafter "+delay/1000+" secs":""));
	if(this.play_timer)
		this.stop();

	if(stb.menutype==4)
	{
		player.status=(player.status==4)?5:this.status;
		alert(player.status);
	}

	if(this.status==0)
	{
		this.status=3;
		this.url=url;
	}

	delay=(delay)?delay:1000;
	this.play_timer=setTimeout(player.timer_play,delay);
}

PLAYER.prototype.stop=function(){
	this.status=0;
	this.url="";
	this.speed=1;
	this.tricktype=0;

	if(this.recstate!=0)
		this.recstate=0;

	if(this.onstatechange)
		this.onstatechange();
}
PLAYER.prototype.rec=function(){
	if(this.status==4)
	{
		this.recstate=(this.recstate==2)?0:2;
	}
		
	if(this.onstatechange)
		this.onstatechange();
}
PLAYER.prototype.trick=function(direction){
	this.speed *=2;
	this.speed=(this.speed>32)?1:this.speed;
	this.tricktype=(this.speed!=1)?direction:0;
	if(this.onstatechange)
		this.onstatechange();
}
PLAYER.prototype.playlist_add=function(fl){
	var pl=new PLAYITEM(fl.filename);
	this.playlist.push(pl);
}
PLAYER.prototype.playlist_remove=function(idx){
	if(idx<this.playlist.length)
	{
		var tmpArr=new Array();
		var revidx=this.playlist.length-idx;
		var i=this.playlist.length;
		while(i--)
		{
			var item=this.playlist.pop();
			if(i==revidx)
				break;
			tmpArr.push(item);
		}
		
		i=tmpArr.length-1;
		while(i--)
		{	var item=tmpArr.pop();
			this.playlist.push(item);
		}
	}
}
PLAYER.prototype.playlist_clear=function(){
	this.playlist=new Array();
}
player=new PLAYER;


function FILELIST(idx){
	var isdir=false;
	var mainf="";
	var mainext="";
	var tmpd=new Array("Abba","Enya","Eimem","Kelly","Wheny","Michael","Snoopy","Emille");
	var tmpf=new Array("test","abb","ccda","ebbh","jioo","piaa","quutt","ztgg","rgqq","looa","krre","tghy","nvbf","cweg","pnwl","umie","huna");
	var i=parseInt(Math.random()*100)%tmpf.length;
	var j=parseInt(Math.random()*100)%tmpd.length;
	if((parseInt(Math.random()*1000))%2==0)
	{
		mainf=tmpf[i];
		mainext=".mp3";
		isdir=false;
	}
	else
	{
		mainf=tmpd[j];
		mainext="";
		isdir=true;
	}
	this.isdir=isdir;
	this.filename=mainf+idx+mainext;
}

function LocalFilesystem(path){
	this.path=path;
	this.filelist=new Array();
}
LocalFilesystem.prototype.readfilelist=function(){
	this.filelist=new Array();
	for(var i=0;i<100;i++)
	{
		var file=new FILELIST(i);
		this.filelist.push(file);
	}
}
LocalFilesystem.prototype.closefilelist=function(){
	this.filelist=new Array();
}
LocalFilesystem.prototype.changedir=function(path){
	this.path=path;
	this.filelist=new Array();
}