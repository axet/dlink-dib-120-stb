/* Password Check */
if((stb.adminPassword!="")&&(!stb.adminVerified))
	window.location.replace(SETUP_PAGES["MENU_PASSWORD"]);

var MENULIST=new Array("MENU_NETWORK","MENU_AV","MENU_SERVER","MENU_INTERNET","MENU_DATETIME","MENU_SYSTEM");
var listobj,titleobj,navprevobj,navnextobj;
var fromurl=def_main_page;
var tourl=new Array(SETUP_PAGES[MENULIST[0]],SETUP_PAGES[MENULIST[1]],SETUP_PAGES[MENULIST[2]],SETUP_PAGES[MENULIST[3]],SETUP_PAGES[MENULIST[4]],SETUP_PAGES[MENULIST[5]]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_SETUP").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_HOME");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			window.location.replace(tourl[eventflag]);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
	}
}


function initFocus(){
	listobj[0].focus();
}
function saveSetting(){
}
function exitPage(){
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	for(var i=0;i<listobj.length;i++)
	{
		var obj=listobj[i];
		obj.innerText=GetLangStr(STR_TYPE_MENU,MENULIST[i]);
		obj.style.navLeft="#"+navprevobj.id;
		obj.style.navRight="#"+((navnextobj)?navnextobj.id:obj.id);
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		
	}

	listobj[0].style.navUp="#"+listobj[listobj.length-1].id;
	listobj[listobj.length-1].style.navDown="#"+listobj[0].id;

	navprevobj.style.navRight="#"+listobj[listobj.length-1].id;

	update();
	initFocus();
}


window.onload=initPage;
window.onunload=exitPage;
