﻿
/* For Opera We must new object */

if(typeof(stb)=='undefined')
	var stb=new Stb();
if(typeof(device)=='undefined')
	var device=new Device();
if(typeof(network)=='undefined')
	var network=new Network();
if(typeof(webbrowser)=='undefined')
	var webbrowser=new Webbrowser();
if(typeof(drm)=='undefined')
	var drm=new Drm();

	
/* In order Keep Login State After Loggin In We Reset Login State when page loaded */
if(stb.adminVerified)
	stb.adminVerified=1;

    var STR_TYPE_MENU=0;
    var STR_TYPE_TITLE=1;
    var STR_TYPE_BUTTON=2;
    var STR_TYPE_TEXT=3;
    var STR_TYPE_CONFIRM=4;
    var STR_TYPE_ERROR=5;

 STR_TYPE_MAP=new Array("SETUP_MENU_","SETUP_TITLE_","SETUP_BUTTON_","SETUP_TEXT_","SETUP_CONFIRM_","SETUP_ERROR_");

//Look Reference galio/src/unicode/languages.h
var LANG_MAP=new Array("en","tw","cn");
var LANG_TEXT=new Array("English","繁體中文","简体中文");

//For Browser Display MultiLanguage Text 
var FULLNAME_ARR=new Array("en","zh-TW","zh-CN");
var LANG_NAME="en";//reference upper FULLNAME_ARR
//Current Language 
//so we can't temporary change it 
//when using wizard or something else
var LANG_CUR=stb.language;

function LANGINIT(){
    for(var i=0;i<LANG_MAP.length;i++){if(LANG_CUR==LANG_MAP[i])LANG_NAME=FULLNAME_ARR[i];}
}
LANGINIT();

// Setting Pages
// if you don't enabled it ,please leavel blank in "" rather than delete it...
var SETUP_PAGES = {
    MENU_SETUP:        "setup.htm",
    MENU_NETWORK:      "network.htm",
    MENU_AV:           "av.htm",
    MENU_AVTEST:       "avtest.htm",
    MENU_SERVER:       "server.htm",
    MENU_DVBT:         "dvbt.htm",
    MENU_DVBS:         "dvbs.htm",
    MENU_DVBS_CH:      "dvbs_channel.htm",
    MENU_DVBC:         "dvbc.htm",
    MENU_VOD:          "vod.htm",
    MENU_IPTV:         "iptv.htm",
    MENU_MISC:         "misc.htm",
    MENU_CA:           "ca.htm",
    MENU_CA_VERIMATRIX:"ca_verimatrix.htm",
    MENU_CA_SECUREMEDIA:"ca_securemedia.htm",
    MENU_SCAN:         "scan.htm",
    MENU_INTERNET:     "internet.htm",
    MENU_MAIL:         "main.htm",
    MENU_LANGUAGE:     "language.htm",
    MENU_DATETIME:     "datetime.htm",
    MENU_SYSTEM:       "system.htm",
    MENU_WIZARD:       "wizard.htm",
    MENU_LOG:          "log.htm",
    MENU_UPDATE:       "update.htm",
    MENU_UPDATE_CHECK: "update_check.htm",
    MENU_UPDATE_URL:   "update_url.htm",
    MENU_DSTUPDATE_URL:"dstupdate_url.htm",
    MENU_ABOUT:        "about.htm",
    MENU_RESET:        "reset.htm",
    MENU_BOOKMARKS:    "bookmarks.htm",
    MENU_SYSTEM:       "system.htm",
    MENU_PASSWORD:     "password.htm",
    MENU_ADMIN:        "admin.htm",
    MENU_DONE:         "done.htm"
};

var SETUP_MENU_EN={
    MENU_SETUP:     "Settings",
    MENU_NETWORK:   "Network",
    MENU_NETWORK_PPPOE:   "PPPoE",
    MENU_NETWORK_WIRE:    "Wired",
    MENU_NETWORK_WIRELESS:"Wireless",
    MENU_AV:        "Audio & Video",
    MENU_SERVER:    "Service",
    MENU_DVB:       "DVB",
    MENU_VOD:       "VOD",
    MENU_IPTV:      "IPTV",
    MENU_MISC:      "MISC",
    MENU_CA:        "CA Setting",
    MENU_INTERNET:  "Internet",
    MENU_MAIL:      "Mail",
    MENU_LANGUAGE:  "Language",
    MENU_DATETIME:  "Date & Time",
    MENU_SYSTEM:    "System",
    MENU_WIZARD:    "Wizard",
    MENU_RESET:     "Factory Reset",
    MENU_LOG:       "System Log",
    MENU_UPDATE:    "Update",
    MENU_DSTUPDATE: "DST Config Update",
    MENU_PASSWORD:  "Password",
    MENU_ADMIN:     "Admin",
    MENU_ABOUT:     "About"
}

var SETUP_MENU_TW={
    MENU_SETUP:     "設定",
    MENU_NETWORK:   "網路設定",
    MENU_NETWORK_PPPOE:   "PPPoE",
    MENU_NETWORK_WIRE:    "有線",
    MENU_NETWORK_WIRELESS:"無線",
    MENU_AV:        "影音設定",
    MENU_SERVER:    "服務設定",
    MENU_DVB:       "數位電視",
    MENU_VOD:		"隨選視訊",
    MENU_IPTV:		"網路頻道",
    MENU_MISC:      "其他設定",
    MENU_CA:        "認證設定",
    MENU_INTERNET:  "網際網路",
    MENU_MAIL:      "電子郵件",
    MENU_LANGUAGE:  "語系設定",
    MENU_DATETIME:  "日期時間",
    MENU_SYSTEM:    "系統設定",
    MENU_WIZARD:    "設定精靈",
    MENU_RESET:     "回復預設值",
    MENU_UPDATE:    "更新設備軟體",
    MENU_DSTUPDATE: "日光節約時間設定更新",
    MENU_PASSWORD:  "管理密碼",
    MENU_ADMIN:     "管理頁面",
    MENU_ABOUT:     "設備資訊"
}

var SETUP_MENU_CN={
    MENU_SETUP:     "设置",
    MENU_NETWORK:   "网络设置",
    MENU_NETWORK_PPPOE:   "PPPoE",
    MENU_NETWORK_WIRE:    "有线",
    MENU_NETWORK_WIRELESS:"无线",
    MENU_AV:        "影音视频",
    MENU_SERVER:    "服务设定",
    MENU_DVB:       "数码电视",
    MENU_VOD:		"随选视讯",
    MENU_IPTV:		"网络频道",
    MENU_MISC:      "其他设置",
    MENU_CA:        "认证设置",
    MENU_INTERNET:  "互联网",
    MENU_MAIL:      "电子邮件",
    MENU_LANGUAGE:  "语言设定",
    MENU_DATETIME:  "日期时间",
    MENU_SYSTEM:    "系统设置",
    MENU_WIZARD:    "设置向导",
    MENU_RESET:     "回复初始值",
    MENU_UPDATE:    "装置更新",
    MENU_DSTUPDATE: "日光节约时间设定档更新",
    MENU_PASSWORD:  "管理口令",
    MENU_ADMIN:     "管理设置",
    MENU_ABOUT:     "装置资讯"
}

var SETUP_TITLE_EN={
    NETWORK_WIRE:     "Wire",
    NETWORK_WIRELESS: "Wireless",
    NETWORK_DHCP:     "Dhcp",
    NETWORK_STATIC:   "Static",
    NETWORK_PPPOE:    "PPPoE",
    NETWORK_PPPOE_USER:"UserName",
    NETWORK_PPPOE_PWD:"Password",
    NETWORK_IP:       "IP Address",
    NETWORK_MASK:     "Subnet Mask",
    NETWORK_GATEWAY:  "Gateway",
    NETWORK_DNS1:     "Preferred DNS",
    NETWORK_DNS2:     "Alternate DNS",
    SYSTEM_WIZARD:    "Wizard",
    SYSTEM_LOG:       "System Log",
    SYSTEM_LOG_SERVER:"Log Server",
    SYSTEM_UPDATE:    "Update",
    SYSTEM_UPDATE_URL:"Update URL",
    SYSTEM_DSTUPDATE: "DSTUpdate",
    SYSTEM_RESET:     "Factory Reset",
    SYSTEM_ABOUT:     "About",
    SYSTEM_ADMIN:     "Password",
    SYSTEM_PASSWORD:  "Password",
    SYSTEM_PASSWORDCF:"Confirm",
    SYSTEM_LOG_LOCAL:"Local",
    SYSTEM_LOG_REMOTE:"Remote",
    SYSTEM_UPDATE_USB:"From USB",
    SYSTEM_UPDATE_NETWORK:"From Network",
    SYSTEM_UPDATE_MULTICAST:"Multicast",
    ABOUT_MODEL:      "Model Name",
    ABOUT_VER:        "Version",
    ABOUT_BUILDDATE:  "Build Date",
    ABOUT_INTHD:      "Hard Drive Installed",
    ABOUT_HDCAPACITY: "Hard Drive Capacity",
    ABOUT_HDFREE:     "Hard Drive Avaliable",
    ABOUT_CONNECTION: "Connection",
    ABOUT_NETWORK_TYPE:"Network Type",
    ABOUT_GWADDR:     "Gateway Address",
    ABOUT_IPADDR:     "IP Address",
    ABOUT_MACADDR:    "MAC Address",
    ABOUT_SN:         "Serial No",
    MEDIA_VOD:        "VOD URL",
    MEDIA_IPTV:       "IPTV URL",
    MEDIA_IGMP:       "IGMP Ver.",
    MEDIA_DVB:        "MY TV",
    MEDIA_PVR:        "PVR URL",
    TIME_ZONE:        "Time Zone",
    TIME_DATETIME:    "DateTime",
    TIME_DATE:        "Date",
    TIME_TIME:        "Time",
    TIME_FORMAT:      "Time Format",
    TIME_SERVER:      "NTP Server",
    TIME_DAYLIGHT:    "DST",
    TIME_UPDATE:      "Update Time",
    TIME_UPDATE_MANUAL:"Manual Set",
    TIME_UPDATE_NTP:  "NTP Calibration",
    TIME_UPDATE_DVB:  "DVB Calibration",
    TIME_DAYLIGHT_ON:  "Enabled",
    TIME_DAYLIGHT_OFF:"Disabled",
    INTERNET_HOMEPAGE:"Home Page",
    INTERNET_PROXY:   "Proxy",
    INTERNET_BOOKMARK0:"Bookmark 1",
    INTERNET_BOOKMARK1:"Bookmark 2",
    INTERNET_BOOKMARK2:"Bookmark 3",
    INTERNET_BOOKMARK3:"Bookmark 4",
    INTERNET_BOOKMARK4:"Bookmark 5",
    DVB_TYPE:         "DVB Type",
    DVB_TYPE_C:       "DVB-C",
    DVB_TYPE_T:       "DVB-T",
    DVB_TYPE_S:       "DVB-S",
    DVB_FREQ_START:   "Freq. Start",
    DVB_FREQ_END:     "Freq. End",
    DVB_FREQ:         "Frequency",
    DVB_SYMBOL_RATE:  "Symbol Rate",
    DVB_POLARITY:     "Polarity",
    DVB_SPAN:         "Span",
    DVB_INTERVAL:     "Interval",
    DVB_QAM:          "QAM",
    DVB_QAM_AUTO:     "Auto",
    DVB_CH_COUNT:     " Channels Found",
    PAGE_MAIN:        "Startup Page",
    PAGE_CONFIG:      "Config Page",
    CA_COMP:          "Company",
    CA_USERNAME:      "Username",
    CA_USERPWD:       "Password",
    CA_SERVER:        "CA Server",
    CA_SERVER2:       "CA Server2",
    CA_KEYSERVER:     "CA KeyServer",
    CA_VKS:           "CA VKS",
    CA_UPDATEUSB:     "Update Key(USB)",
    ALL_AUTO:         "Auto",
    ALL_DEFAULT:      "Default"
}

var SETUP_TITLE_TW={
    NETWORK_WIRE:     "有線網路連線",
    NETWORK_WIRELESS: "無線網路連線",
    NETWORK_DHCP:     "動態IP",
    NETWORK_STATIC:   "靜態IP",
    NETWORK_PPPOE:    "PPPoE",
    NETWORK_PPPOE_USER:"帳號",
    NETWORK_PPPOE_PWD:"密碼",
    NETWORK_IP:       "IP 位址",
    NETWORK_MASK:     "子網域遮罩",
    NETWORK_GATEWAY:  "通訊匣道位址",
    NETWORK_DNS1:     "主要 DNS",
    NETWORK_DNS2:     "次要 DNS",
    SYSTEM_WIZARD:    "精靈",
    SYSTEM_LOG:       "紀錄",
    SYSTEM_LOG_SERVER:"紀錄伺服器",
    SYSTEM_UPDATE:    "更新",
    SYSTEM_UPDATE_URL:"更新的網址",
    SYSTEM_DSTUPDATE:"DSTUpdate網址",
    SYSTEM_RESET:     "重置設定",
    SYSTEM_ABOUT:     "關於",
    SYSTEM_ADMIN:     "管理者密碼",
    SYSTEM_PASSWORD:  "密碼",
    SYSTEM_PASSWORDCF:"確認",
    SYSTEM_LOG_LOCAL:"預設",
    SYSTEM_LOG_REMOTE:"遠端",
    SYSTEM_UPDATE_USB:"從USB更新",
    SYSTEM_UPDATE_NETWORK:"從網路更新",
    SYSTEM_UPDATE_MULTICAST:"從廣播位址更新",
    ABOUT_MODEL:      "型號",
    ABOUT_VER:        "版本",
    ABOUT_BUILDDATE:  "建立日期",
    ABOUT_INTHD:      "內建硬碟",
    ABOUT_HDCAPACITY: "硬碟總體容量",
    ABOUT_HDFREE:     "硬碟剩餘空間",
    ABOUT_CONNECTION: "連線種類",
    ABOUT_NETWORK_TYPE:"連線方式",
    ABOUT_GWADDR:     "閘道位址",
    ABOUT_IPADDR:     "目前 IP 位址",
    ABOUT_MACADDR:    "網路卡實體位址",
    ABOUT_SN:         "設備序號",
    MEDIA_VOD:        "VOD 伺服器",
    MEDIA_IPTV:       "IPTV 伺服器",
    MEDIA_IGMP:       "IGMP 版本",
    MEDIA_DVB:        "My TV",
    MEDIA_PVR:        "PVR 伺服器",
    TIME_ZONE:        "所在時區",
    TIME_DATETIME:    "日期時間",
    TIME_DATE:        "日期",
    TIME_TIME:        "時間",
    TIME_FORMAT:      "時間格式",
    TIME_SERVER:      "時間更新伺服器",
    TIME_DAYLIGHT:    "日光節約時間",
    TIME_UPDATE:      "時間更新方式",
    TIME_UPDATE_MANUAL:"手動設定",
    TIME_UPDATE_NTP:  "NTP 網路校時",
    TIME_UPDATE_DVB:  "DVB 校時",
    TIME_DAYLIGHT_ON: "啟用",
    TIME_DAYLIGHT_OFF:"停用",
    INTERNET_HOMEPAGE:"首頁",
    INTERNET_PROXY:   "快取伺服器",
    INTERNET_BOOKMARK0:"我的最愛 1",
    INTERNET_BOOKMARK1:"我的最愛 2",
    INTERNET_BOOKMARK2:"我的最愛 3",
    INTERNET_BOOKMARK3:"我的最愛 4",
    INTERNET_BOOKMARK4:"我的最愛 5",
    DVB_TYPE:         "數位電視種類",
    DVB_TYPE_C:       "DVB-C",
    DVB_TYPE_T:       "DVB-T",
    DVB_TYPE_S:       "DVB-S",
    DVB_FREQ_START:   "開始頻率",
    DVB_FREQ_END:     "結束頻率",
    DVB_FREQ:         "頻率",
    DVB_SYMBOL_RATE:  "Symbol Rate",
    DVB_POLARITY:     "Polarity",
    DVB_SPAN:         "Span",
    DVB_INTERVAL:     "Interval",
    DVB_QAM:          "QAM",
    DVB_QAM_AUTO:     "Auto",
    DVB_CH_COUNT:     " 頻道已找到",
    PAGE_MAIN:        "啟始頁面",
    PAGE_CONFIG:      "設定頁面",
    CA_COMP:          "授權公司",
    CA_USERNAME:      "使用者名稱",
    CA_USERPWD:       "使用者密碼",
    CA_SERVER:        "認證伺服器",
    CA_SERVER2:       "認證伺服器2",
    CA_KEYSERVER:     "憑證伺服器",
    CA_VKS:           "認證VKS伺服器",
    CA_UPDATEUSB:     "更新認證(USB)",
    ALL_AUTO:         "自動",
    ALL_DEFAULT:      "預設值"
}

var SETUP_TITLE_CN={
    NETWORK_WIRE:     "有线连线",
    NETWORK_WIRELESS:"无线连线",
    NETWORK_DHCP:     "动态IP",
    NETWORK_STATIC:   "静态IP",
    NETWORK_PPPOE:    "PPPoE",
    NETWORK_PPPOE_USER:"用户",
    NETWORK_PPPOE_PWD: "口令",
    NETWORK_IP:       "IP 地址",
    NETWORK_MASK:     "子网掩码",
    NETWORK_GATEWAY:  "网关地址",
    NETWORK_DNS1:     "主要 DNS",
    NETWORK_DNS2:     "次要 DNS",
    SYSTEM_WIZARD:    "设置向导",
    SYSTEM_LOG:       "纪录",
    SYSTEM_LOG_SERVER:"纪录服务器",
    SYSTEM_UPDATE:    "更新",
    SYSTEM_UPDATE_URL: "更新的地址",
    SYSTEM_DSTUPDATE:"DSTUpdate地址",
    SYSTEM_RESET:     "重置设定",
    SYSTEM_ABOUT:     "关於",
    SYSTEM_ADMIN:     "管理者口令",
    SYSTEM_PASSWORD:  "口令",
    SYSTEM_PASSWORDCF:"确认",
    SYSTEM_LOG_LOCAL:"本地",
    SYSTEM_LOG_REMOTE:"远端",
    SYSTEM_UPDATE_USB:"从USB更新",
    SYSTEM_UPDATE_NETWORK:"从网络更新",
    SYSTEM_UPDATE_MULTICAST:"从广播地址更新",
    ABOUT_MODEL:      "型号",
    ABOUT_VER:        "版本",
    ABOUT_BUILDDATE:  "建立日期",
    ABOUT_INTHD:      "内置硬盘",
    ABOUT_HDCAPACITY:"硬盘总体容量",
    ABOUT_HDFREE:     "硬盘剩余空间",
    ABOUT_CONNECTION:"连线种类",
    ABOUT_NETWORK_TYPE: "连线方式",
    ABOUT_GWADDR:     "网关地址",
    ABOUT_IPADDR:     "目前 IP 地址",
    ABOUT_MACADDR:    "网络卡实体地址",
    ABOUT_SN:         "装置序列号",
    MEDIA_VOD:        "VOD 服务器",
    MEDIA_IPTV:       "IPTV 服务器",
    MEDIA_IGMP:       "IGMP 版本",
    MEDIA_DVB:        "MY TV",
    MEDIA_PVR:        "PVR 服务器",
    TIME_ZONE:        "所在时区",
    TIME_DATETIME:    "日期时间",
    TIME_DATE:        "日期",
    TIME_TIME:        "时间",
    TIME_FORMAT:      "时间格式",
    TIME_SERVER:      "时间更新服務器",
    TIME_DAYLIGHT:    "日光节约时间",
    TIME_UPDATE:      "时间更新方式",
    TIME_UPDATE_MANUAL:"无(手动设定)",
    TIME_UPDATE_NTP:  "NTP 网络校时",
    TIME_UPDATE_DVB:  "DVB 校时",
    TIME_DAYLIGHT_ON: "启用",
    TIME_DAYLIGHT_OFF:"禁用",
    INTERNET_HOMEPAGE:"主页",
    INTERNET_PROXY:   "代理服务器",
    INTERNET_BOOKMARK0:"收藏夹 1",
    INTERNET_BOOKMARK1:"收藏夹 2",
    INTERNET_BOOKMARK2:"收藏夹 3",
    INTERNET_BOOKMARK3:"收藏夹 4",
    INTERNET_BOOKMARK4:"收藏夹 5",
    DVB_TYPE:         "数码电视种类",
    DVB_TYPE_C:       "DVB-C",
    DVB_TYPE_T:       "DVB-T",
    DVB_TYPE_S:       "DVB-S",
    DVB_FREQ_START:   "开始频率",
    DVB_FREQ_END:     "结束频率",
    DVB_FREQ:         "频率",
    DVB_SYMBOL_RATE:  "Symbol Rate",
    DVB_POLARITY:     "Polarity",
    DVB_SPAN:         "Span",
    DVB_INTERVAL:     "Interval",
    DVB_QAM:          "QAM",
    DVB_QAM_AUTO:     "Auto",
    DVB_CH_COUNT:     " 频道已找到",
    PAGE_MAIN:        "启始页面",
    PAGE_CONFIG:      "设定页面",
    CA_COMP:          "授权公司",
    CA_USERNAME:      "使用者名称",
    CA_USERPWD:       "使用者密码",
    CA_SERVER:        "认证服务器",
    CA_SERVER2:       "认证服务器2",
    CA_KEYSERVER:     "凭证服务器",
    CA_VKS:           "认证VKS服务器",
    CA_UPDATEUSB:     "更新认证(USB)",
    ALL_AUTO:         "自动",
    ALL_DEFAULT:      "预设值"
}

var SETUP_BUTTON_EN={
    BTN_OK:     "OK",
    BTN_CANCEL: "CANCEL",
    BTN_YES:    "YES",
    BTN_NO:     "NO",
    BTN_PREV:   "BACK",
    BTN_NEXT:   "NEXT",
    BTN_ADD:    "ADD",
    BTN_RESET:  "RESET",
    BTN_FINISH: "FINISH",
    BTN_HOME:   "HOME",
    BTN_CONNECT:"CONNECT"
}

var SETUP_BUTTON_TW={
    BTN_OK:     "確定",
    BTN_CANCEL: "取消",
    BTN_YES:    "是",
    BTN_NO:     "否",
    BTN_PREV:   "上一步",
    BTN_NEXT:   "下一步",
    BTN_ADD:    "新增",
    BTN_RESET:  "預設值",
    BTN_FINISH: "完成",
    BTN_HOME:   "首頁",
    BTN_CONNECT:"連線"
}

var SETUP_BUTTON_CN={
    BTN_OK:     "确定",
    BTN_CANCEL: "取消",
    BTN_YES:    "是",
    BTN_NO:     "否",
    BTN_PREV:   "上一步",
    BTN_NEXT:   "下一步",
    BTN_ADD:    "添加",
    BTN_RESET:  "重置",
    BTN_FINISH: "完成",
    BTN_HOME:   "主页",
    BTN_CONNECT:"连接"
}

var SETUP_TEXT_EN={
    FUNC_DISABLED:      "This Item Disabled",
    FUNC_UNAVAILABLE:   "This Item Is Unavailable",
    FUNC_DONE:          "Setting Done",
    DHCP_ENABLED:       "DHCP Mode Enabled",
    CHOOSE_LANGUAGE:    "Choose Your Language",
    CHOOSE_NETWORK_WIRE:"Choose Your Connection Type",
    CHOOSE_NETWORK_TYPE:"Choose Your Network Type",
    DVB_SCAN:           "DVB Scanning...",
    UPDATE:             "Firmware Checking ...",
    UPDATE_DOWNLOAD:    "Firmware Downloading ...",
    UPDATE_WRITE:       "Firmware Upgrading ...Please Don't Turn Off Power!",
    UPDATE_DONE:        "Upgrade Done. Device Will Reboot!",
    ROLLBACK:           "Cancel After ",
    TIME_SEC:           " Secs",
}

var SETUP_TEXT_TW={
    FUNC_DISABLED:      "此項目目前停用",
    FUNC_UNAVAILABLE:   "此項目目前無法設置",
    FUNC_DONE:          "設定完成",
    DHCP_ENABLED:       "DHCP 動態IP模式已啟用",
    CHOOSE_LANGUAGE:    "請選擇你的語系",
    CHOOSE_NETWORK_WIRE:"請選擇你的網路連線種類",
    CHOOSE_NETWORK_TYPE:"請選擇你的網路連線方式",
    DVB_SCAN:           "DVB 訊號搜尋中...",
    UPDATE:             "尋找更新中...",
    UPDATE_DOWNLOAD:    "下載更新中 ...",
    UPDATE_WRITE:       "更新中 ...請勿關閉電源!",
    UPDATE_DONE:        "更新完成，準備重新啟動!",
    ROLLBACK:           "復原原本設定剩 ",
    TIME_SEC:           " 秒",
}

var SETUP_TEXT_CN={
    FUNC_DISABLED:      "目前此项功能停用",
    FUNC_UNAVAILABLE:   "目前此项功能无法设置",
    FUNC_DONE:          "设置完成",
    DHCP_ENABLED:       "DHCP 动态IP模式已启用",
    CHOOSE_LANGUAGE:    "请选择你的语言",
    CHOOSE_NETWORK_WIRE:"请选择你的网络连接种类",
    CHOOSE_NETWORK_TYPE:"请选择你的网络连接方式",
    DVB_SCAN:           "DVB 讯号搜寻中...",
    UPDATE:             "寻找更新中...",
    UPDATE_DOWNLOAD:    "下载更新中 ...",
    UPDATE_WRITE:       "更新中 ...请勿关闭电源",
    UPDATE_DONE:        "更新完成，准备重新启动!",
    ROLLBACK:           "返回旧设定剩 ",
    TIME_SEC:           " 秒",
}

var SETUP_CONFIRM_EN={
    LANGUAGE:"Confirm Language Change?",
    RESET:   "Are you sure that you want to reset?",
    APPLY:   "Press 'Finish' to apply.",
}

var SETUP_CONFIRM_TW={
    LANGUAGE:"你想要套用變更語系嗎?",
    RESET:   "你想要重置所有的設定嗎?",
    APPLY:   "按\"完成\" 套用新的設定",
}

var SETUP_CONFIRM_CN={
    LANGUAGE:"你想要套用变更语系吗?",
    RESET:   "你想要重置所有的设置吗?",
    APPLY:   "按\"完成\" 套用新的设定",
}

var SETUP_ERROR_EN={
    NETWORK_IP_INVALID:   "Address Invalid",
    NETWORK_PPPOE_INVALID:"Please Input Username and Password",
    DVB_SCAN_FAIL:        "Scan Fail",
    UPDATE_NONE:          "No Newer Update Found.",
    UPDATE_USB_ERROR:     "USB Device not Found!",
    UPDATE_FILE_ERROR:    "Update File Fail or No Newer Update Found!",
    UPDATE_DOWNLOAD_FAIL: "Download Update Fail!"
}

var SETUP_ERROR_TW={
    NETWORK_IP_INVALID:   "網路位址設定錯誤",
    NETWORK_PPPOE_INVALID:"請輸入連線帳號及密碼",
    DVB_SCAN_FAIL:        "無法偵測到DVB訊號",
    UPDATE_NONE:          "目前尚無新的更新.",
    UPDATE_USB_ERROR:     "USB 裝置讀取錯誤，請確定是否接妥!",
    UPDATE_FILE_ERROR:    "目前尚無新的更新或更新檔案有誤.",
    UPDATE_DOWNLOAD_FAIL: "下載更新錯誤!"
}

var SETUP_ERROR_CN={
    NETWORK_IP_INVALID:   "网络位址设定错误",
    NETWORK_PPPOE_INVALID:"请输入连线用户及口令",
    DVB_SCAN_FAIL:        "无法搜寻DVB信号",
    UPDATE_NONE:          "目前尚无新的版本更新.",
    UPDATE_USB_ERROR:     "USB盘读取错误，请确定是否接妥!",
    UPDATE_FILE_ERROR:    "更新的档案错误!",
    UPDATE_DOWNLOAD_FAIL: "下载更新错误!"
}

function GetLangStr(type,id)
{
    var rv="";
    var pending=LANG_CUR.toUpperCase();

    try {
        eval("rv="+STR_TYPE_MAP[type]+pending+"['"+id+"'];");
    } catch(err) {
        //Err Process
        stb.print('#FATAL ERR# No String Matched '+STR_TYPE_MAP[type]+pending+"["+id+"]");
        rv="";
    }
    finally{
        return rv;
    }
}

function GetUrlParamValue(str,param)
{
    if(str.length>2)
    {
        var tmpstr=str.substr(1,str.length-1);
        var tmparr=tmpstr.split('&');
        for(var i=0;i<tmparr.length;i++)
        {
            if(tmparr[i].indexOf(param+'=')==0)
            {
                var start=param.length+1;
                var end=tmparr[i].length-start;
                //stb.print("Param"+i+"["+param+"]:"+tmparr[i].substr(start,end));
                return tmparr[i].substr(start,end);
            }
        }
    }
}

