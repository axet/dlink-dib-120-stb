var MENULIST=new Array("DVB_FREQ_START","DVB_FREQ_END","DVB_SPAN","DVB_INTERVAL","DVB_QAM");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_SCAN"]);

var mod_type=0;
//Refer iBox/porting/drv_adpt/drv_tuner.h
var MODLIST=new Array("AUTO","BPSK","QPSK","QAM 4","PSK 8","QAM 16","QAM 32","QAM 64","QAM 128","QAM 256","QAM 512","QAM 1024");

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_TITLE,"DVB_TYPE_T").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
	navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	switch(eventtype)
	{
		case 0://Focus
			var isinput=(CurItem<4)?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			break;
		case 1://Click
			if(eventflag>0)
				saveSetting();
			window.location.replace((eventflag>0)?tourl[0]:fromurl);
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0://Focus
			var isinput=(CurItem<4)?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			CurItem=idx;
			var isinput=(CurItem<4)?true:false;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className=(isinput)?"tbedit_over":"tbnav_over";
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var direction=1;//Default Right ->
			switch(key)
			{
				case 0x25:direction=-1;
				case 0x27:
					switch(idx)
					{
						case 4:
							//Just Focus on QAM xx
							mod_type=(mod_type+direction+MODLIST.length)%(MODLIST.length);
							listobj[2*idx+1].innerText=MODLIST[mod_type];
							break;
					}
					break;
			}
			break;
		case 3://Mouse Over
			if(typeof(listobj[idx*2+1].getElementsByTagName("input"))=="object")
				listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
	}
}

function saveSetting()
{
	var start=parseInt(listobj[1].getElementsByTagName("input")[0].value);
	var end=parseInt(listobj[3].getElementsByTagName("input")[0].value);
	var span=parseInt(listobj[5].getElementsByTagName("input")[0].value);
	var interval=parseInt(listobj[7].getElementsByTagName("input")[0].value);
	dvb.set_cur_scan_param(0,start,end,span,interval,mod_type);
}

function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	var def_set_str=dvb.get_cur_scan_param_string(0);
	var tmp=def_set_str.split(',');
	var param=new Array();

	param.push(parseInt(tmp[0]));
	param.push(parseInt(tmp[1]));
	param.push(parseInt(tmp[2]));
	param.push(parseInt(tmp[3]));
	param.push(parseInt(tmp[4]));

	vallist.push(param[0]);
	vallist.push(param[1]);
	vallist.push(param[2]);
	vallist.push(param[3]);
	mod_type=param[4];
	vallist.push(MODLIST[mod_type]);

	for(var i=0;i<(listobj.length/2);i++)
	{
		var obj;
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		if(i==4)
		{
			obj=listobj[i*2+1];
			obj.innerText=vallist[i];
		}
		else
		{
			obj=listobj[i*2+1].getElementsByTagName("input")[0];
			obj.value=vallist[i];
		}

		obj.style.navRight="#"+navnextobj.id;
		obj.style.navLeft="#"+navprevobj.id;

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;