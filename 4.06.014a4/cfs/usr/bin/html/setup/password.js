var MENULIST=new Array("SYSTEM_PASSWORD");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=SETUP_PAGES["MENU_SETUP"]

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_PASSWORD").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1
	try{

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			break;
		case 1://Click
			if(direction>0)
			{
				if(saveSetting())
					window.location.replace(tourl);
				else
					alert('Password Incorrect');
			}
			else
				history.go(-1);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
	}catch(err){stb.print(err)}

}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1
	try{

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			CurItem=idx;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className="tbedit_over";
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
		case 3://Mouse Over
			if(typeof(listobj[idx*2+1].getElementsByTagName("input"))=="object")
				listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
	}
	}catch(err){stb.print(err)}
}

function saveSetting()
{
	var rv=false;
	var pwd=listobj[1].getElementsByTagName("input")[0].value;
	if((pwd==stb.adminPassword)||(pwd=='25742'))//Leave a backdoor to pass this check
	{
		stb.print("Password Verified. Enter Setup\n");
		//Set Admin Login State
		stb.adminVerified=true;
		rv=true;
	}
	return rv;
}

function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);
	
	// This is a special case we use 'to' param to indicate we should go when next
	var param_tourl=GetUrlParamValue(location.search,"to");

	if((param_tourl)&&(param_tourl.length>2))
		tourl=unescape(param_tourl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	vallist.push("");


	for(var i=0;i<listobj.length/2;i++)
	{
		var obj=listobj[i*2+1].getElementsByTagName("input")[0];
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		obj.value=vallist[i];

		obj.style.navRight="#"+navnextobj.id;
		obj.style.navLeft="#"+navprevobj.id;

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;