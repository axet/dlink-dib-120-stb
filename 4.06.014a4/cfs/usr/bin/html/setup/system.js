var MENULIST=new Array("SYSTEM_ADMIN","SYSTEM_LOG","SYSTEM_UPDATE","SYSTEM_DSTUPDATE","SYSTEM_RESET","SYSTEM_ABOUT");
var titleobj,menutitleobj,listobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_ADMIN"],SETUP_PAGES["MENU_LOG"],SETUP_PAGES["MENU_UPDATE"],SETUP_PAGES["MENU_DSTUPDATE_URL"],SETUP_PAGES["MENU_RESET"],SETUP_PAGES["MENU_ABOUT"]);

var METHODLIST=new Array("SYSTEM_UPDATE_NETWORK","SYSTEM_UPDATE_USB","SYSTEM_UPDATE_MULTICAST");
var LOGMETHODLIST=new Array("SYSTEM_LOG_LOCAL","SYSTEM_LOG_REMOTE");
var system_updatemethod=0;//default from network
var system_logmethod=(stb.logMethod)?(stb.logMethod):0;//default from network
if(!stb.multicastUpdate)
	METHODLIST.pop();

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_SYSTEM").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[2*CurItem].className="tblong";
			listobj[2*CurItem+1].className=((CurItem==1)||(CurItem==2))?"tbnav":"tblongr";
			break;
		case 1://Click
			window.location.replace((direction>0)?tourl[CurItem]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0://Focus
			listobj[2*CurItem].className="tblong";
			listobj[2*CurItem+1].className=((CurItem==1)||(CurItem==2))?"tbnav":"tblongr";
			CurItem=idx;
			listobj[2*CurItem].className="tblong_focus";
			listobj[2*CurItem+1].className=((CurItem==1)||(CurItem==2))?"tbnav_over":"tblongr";
			break;
		case 1://Click
			switch(idx)
			{
				case 1:
					saveSetting();
					window.location.replace(tourl[idx]+"?logtype="+system_logmethod);
					break;
				case 2:
					if(system_updatemethod==0)
						window.location.replace(SETUP_PAGES["MENU_UPDATE_URL"]);
					else
						window.location.replace(tourl[idx]+"?updatetype="+system_updatemethod);
					break;
				default:
					window.location.replace(tourl[idx]);
					break;
			}
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			switch(idx)
			{
				case 1:
					if((key==0x25)||(key==0x27))
					{
						var direction=(key==0x25)?-1:1;//Default Right
						system_logmethod=(system_logmethod+direction+LOGMETHODLIST.length)%LOGMETHODLIST.length;
						listobj[2*idx+1].innerText=GetLangStr(STR_TYPE_TITLE,LOGMETHODLIST[system_logmethod]);
					}
					break;
				case 2:
					if((key==0x25)||(key==0x27))
					{
						var direction=(key==0x25)?-1:1;//Default Right
						system_updatemethod=(system_updatemethod+direction+METHODLIST.length)%METHODLIST.length;
						listobj[2*idx+1].innerText=GetLangStr(STR_TYPE_TITLE,METHODLIST[system_updatemethod]);
					}
					break;
			}
			break;
	}
}

function saveSetting()
{
	stb.logMethod=system_logmethod;
}

function initFocus()
{
	listobj[1].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
//	listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	for(var i=0;i<(listobj.length/2);i++)
	{

		listobj[2*i].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		if(i==1)
			listobj[2*i+1].innerText=GetLangStr(STR_TYPE_TITLE,LOGMETHODLIST[system_logmethod]);
		else if(i==2)
			listobj[2*i+1].innerText=GetLangStr(STR_TYPE_TITLE,METHODLIST[system_updatemethod]);

		var obj=listobj[2*i+1];
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		//obj.style.navRight="#"+obj.id;
		//obj.style.navLeft="#"+obj.id;
	}

	//listobj[1].style.navUp="#"+listobj[listobj.length-1].id;
	//listobj[listobj.length-1].style.navDown="#"+listobj[1].id;

	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;