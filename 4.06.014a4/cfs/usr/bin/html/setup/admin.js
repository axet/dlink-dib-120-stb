var MENULIST=new Array("SYSTEM_PASSWORD","SYSTEM_PASSWORDCF");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SYSTEM"];
var tourl=SETUP_PAGES["MENU_DONE"]

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_ADMIN").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[2*CurItem].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			break;
		case 1://Click
			if(direction>0)
			{
				if(saveSetting())
					window.location.replace(tourl);
			}
			else
				window.location.replace(fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			CurItem=idx;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className="tbedit_over";
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
		case 3://Mouse Over
			listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
	}
}

function saveSetting()
{
	try{
	var rv=false;
	var pwd1=listobj[1].getElementsByTagName("input")[0].value;
	var pwd2=listobj[3].getElementsByTagName("input")[0].value;
	if(pwd1==pwd2)
	{
		//Set Admin Login State
		stb.adminPassword=pwd1;
		
		rv=true;
	}
	else 
		alert('Password Dismatch');
	}catch(err){stb.print(err)}
	stb.print('OK');
	return rv;
}

function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);
	
	// This is a special case we use 'to' param to indicate we should go when next
	var param_tourl=GetUrlParamValue(location.search,"to");

	if((param_tourl)&&(param_tourl.length>2))
		tourl=unescape(param_tourl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	//listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	for(var i=0;i<listobj.length/2;i++)
	{
		var obj;
		//listobj[i].getElementsByTagName("div")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		listobj[2*i].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		//obj=listobj[i].getElementsByTagName("input")[0];
		obj=listobj[2*i+1].getElementsByTagName("input")[0];

		obj.style.navRight="#"+obj.id;
		obj.style.navLeft="#"+obj.id;
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;