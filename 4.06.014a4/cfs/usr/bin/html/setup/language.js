﻿var titleobj,listobj,navprevobj,navnextobj;
var selectedIndex=0;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_LANGUAGE").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			CurItem=idx;
			break;
		case 1://Click
			saveSetting();
			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function saveSetting()
{
	stb.language=LANG_MAP[CurItem];
}

function initFocus()
{
	listobj[selectedIndex].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	//listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	for(var i=0;i<LANG_MAP.length;i++)
	{
		if(stb.language==LANG_MAP[i])
		{
			selectedIndex=i;
		}
	}

	for(var i=0;i<listobj.length;i++)
	{
		var obj=listobj[i];
		
		if(i>LANG_MAP.length-1)
			break;
		
		obj.style.navLeft="#"+navprevobj.id;
		obj.style.navRight="#"+((navnextobj)?navnextobj.id:obj.id);
		obj.lang=FULLNAME_ARR[i];
		if(i==selectedIndex)
			obj.innerHTML='<div class="star">'+LANG_TEXT[i]+'</div>';
		else
			obj.innerText=LANG_TEXT[i];
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
	}

	listobj[0].style.navUp="#"+listobj[i-1].id;
	listobj[i-1].style.navDown="#"+listobj[0].id;

	while(i<listobj.length)
	{
		listobj[i].style.visibility="hidden";
		i++;
	}

	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;