var listobj,titleobj,navprevobj,navnextobj,navresetobj;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_SCAN"]);
var DBIsChanged=false;//If Changed ,we save DB at exit;

function SATELLITE()
{
	this.load_data();//Load Current Country Selected List
	this.start_idx=0;
	this.listobj=new Array();
}
SATELLITE.prototype.load_data=function(){
	this.sate_status=new Array();
	for(var i=0;i<dvb.satellites.length;i++)
	{this.sate_status[i]=false;}

	var ulist=dvb.get_cur_scan_param_string(2);
	this.sateIndex_selected=ulist.split(',');
	for(var i=0;i<this.sateIndex_selected.length;i++)
	{
		var idx=parseInt(this.sateIndex_selected[i]);
		if(idx<this.sate_status.length)
		{
			this.sate_status[idx]=true;
		}
	}
}

SATELLITE.prototype.processItem=function(event,eventflag,eventtype){
	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			window.location.replace(SETUP_PAGES["MENU_DVBS_CH"]+"?sat_id="+eventflag);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var direction=(key==0x25)?-1:1;

			switch(key)
			{
				case 0x25:
				case 0x27:
					var idx=this.start_idx+eventflag;
					this.sate_status[idx]=!this.sate_status[idx];
					this.listobj[eventflag].getElementsByTagName("DIV")[1].innerText=(this.sate_status[idx])?"ON":"OFF";
					break;
				case 0x21:/*PAGE UP*/
				satellite.nav(-1);
				break;
				case 0x22:/*PAGE DOWN*/
				satellite.nav(1);
				break;
			}
			break;
	}
}
SATELLITE.prototype.bindObject=function(rootid,pgupid,pgdownid){
	var me=this;

	this.pgupobj=document.getElementById(pgupid);
	this.pgdownobj=document.getElementById(pgdownid);

	this.listobj=document.getElementById(rootid).getElementsByTagName("UL")[0].getElementsByTagName("LI");
	document.getElementById(rootid).lang=LANG_NAME;

	for(var i=0;i<this.listobj.length;i++)
	{
		var obj=this.listobj[i];
		eval('obj.addEventListener("focus", function(event){me.processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){me.processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){me.processItem(event,'+i+',2)}, false);');
		obj.style.navLeft="#"+obj.id;
		obj.style.navRight="#"+obj.id;
	}
	this.listobj[this.listobj.length-1].style.navDown="#"+navnextobj.id;
}
SATELLITE.prototype.show_page_navigator=function(){
	if(dvb.satellites.length==0)
	{
		this.pgupobj.className="pgup0";
		this.pgdownobj.className="pgdown0";
		
	}
	else
	{
		this.pgupobj.className=(this.start_idx>0)?"pgup1":"pgup0";
		if(dvb.channels.length>this.listobj.length)
		{
			this.pgdownobj.className=((this.start_idx+this.listobj.length)<=(dvb.satellites.length-1))?"pgdown1":"pgdown0";
		}
	}

}

SATELLITE.prototype.update=function(){
	for(var i=0;i<this.listobj.length;i++)
	{
		var idx=this.start_idx+i;

		if(idx>=dvb.satellites.length)
			break;
		this.listobj[i].className="itemnavshort";
		this.listobj[i].getElementsByTagName("DIV")[0].innerText=dvb.satellites[idx].name;
		this.listobj[i].getElementsByTagName("DIV")[1].innerText=(this.sate_status[idx])?"ON":"OFF";
	}

	while(i<this.listobj.length)
	{
		this.listobj[i].className="item0";
		i++;
	}
	this.show_page_navigator();
}
SATELLITE.prototype.nav=function(direction){
	switch(direction)
	{
		case -1://Page Prev
			this.start_idx -=this.listobj.length;
			this.start_idx=(this.start_idx<0)?0:this.start_idx;
			this.update();
			break;
		case 1://Page Next
			if(dvb.satellites.length>this.listobj.length)
			{
				this.start_idx +=this.listobj.length;
				this.start_idx=(this.start_idx>=this.listobj.length)?dvb.satellites.length-this.listobj.length:this.start_idx;
				this.update();
			}
			break;
	}
}
SATELLITE.prototype.verify=function(){
	var isvalid=false;
	//At Least One Satellite Used
	for(var i=0;i<satellite.sate_status.length;i++)
	{
		if(satellite.sate_status[i])
		{
			isvalid=true;
			break;
		}
	}

	return isvalid;
}
var satellite=new SATELLITE;

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_TITLE,"DVB_TYPE_S").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
	{
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
		navnextobj.className="funcfinish";
	}

	if(navresetobj)
		navresetobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_RESET");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem].getElementsByTagName("div")[0].className="title";
			break;
		case 1://Click
			switch(eventflag)
			{
				case 1:
					if(!satellite.verify())
					{
						alert("You must select one satellite 'ON' at least.");
						break;
					}
					else
						saveSetting();
				case -1:
					window.location.replace((direction>0)?tourl[0]:fromurl);
					break;
				case 3://Reset
					dvb.satellites.reset_country_default();
					satellite.load_data();
					satellite.update();
					break;
			}
			break;
	}
}

function saveSetting()
{
	var prefix="";
	var satestr="";

	for(var i=0;i<satellite.sate_status.length;i++)
	{
		if(satellite.sate_status[i])
		{
			satestr += prefix+i;
			prefix=",";
		}
	}
	eval('dvb.satellites.set_country_satellite('+satestr+');');
	DBIsChanged=true;
}

function initFocus()
{
	satellite.listobj[0].focus();
}

function exitPage()
{
	if(DBIsChanged)
		dvb.satellites.save();
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	navresetobj=document.getElementById("navreset");
	navresetobj.addEventListener("click", function(event){processFunc(event,3,1)}, false);
	navresetobj.lang=LANG_NAME;

	satellite.bindObject("setup_menu","pgup","pgdown");
	satellite.update();
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;