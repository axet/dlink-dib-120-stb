var titleobj,menutitleobj,listobj,navprevobj,navnextobj,resultobj;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_SERVER"]);
var update_timer;
var progressobj;

var TYPEPAGE=new Array(SETUP_PAGES["MENU_DVBT"],SETUP_PAGES["MENU_DVBC"],SETUP_PAGES["MENU_DVBS"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_DVB").toUpperCase();
	menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"DVB_SCAN");

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting(true);

			window.location.replace((direction>0)?tourl[0]:TYPEPAGE[dvb.dvb_type]);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
}

function saveSetting(cancel_update_screen)
{
	if(!cancel_update_screen)
		update();

	//If one or more Found goto View DVB 
	//Else Go Scan Parameter to try again
	var tmpurl=TYPEPAGE[dvb.dvb_type];

	if(dvb.channels.length>0)
		tmpurl=(webbrowser.dvbServer.length>0)?webbrowser.dvbServer:def_dvb_page;

	window.location.replace(tmpurl);
}

function cancelDVBScan()
{
	if(dvb.get_scan_status()!=0)
		dvb.scan();
	isModal=false;
}

function DVBScan()
{

	var status=dvb.get_scan_status();

	if(status==2)/* Previous Scan Failed */
	{
		cancelDVBScan();
	}
	else if(status==0)
	{
		if(dvb.scan())
		{	
			isModal=true;
		}
	}
}

function updateStatus()
{
	var progress_percent=100;
	var progress_unit=50;

	switch(dvb.get_scan_status())
	{
		case 0:
			menutitleobj.innerText="";
			resulttitleobj.innerText="";
			isModal=false;
			saveSetting();
			break;
		case 1:
			isModal=true;
			progress_percent=dvb.get_scan_percent();
			progress_unit=parseInt((progress_percent*50)/100);
			resulttitleobj.innerText=dvb.channels.length+GetLangStr(STR_TYPE_TITLE,"DVB_CH_COUNT");
			if(progress_percent>=100)
			{
				if(dvb.get_scan_status()==0)
				{
					cancelDVBScan();/* STOP Scan */
					saveSetting();
				}
			}
			break;
		default:
			menutitleobj.innerText=GetLangStr(STR_TYPE_ERROR,"DVB_SCAN_FAIL");
			resulttitleobj.innerText="";
			break;
	}

	progressobj.getElementsByTagName("span")[0].style.width=progress_unit*9;
	progressobj.getElementsByTagName("span")[1].innerText=progress_percent+" %";
}

function LoadData()
{
	DVBScan();	
	update_timer=setInterval(updateStatus,1000);
}

function initSelected()
{
}

function initFocus()
{
	navprevobj.focus();
}

function exitPage()
{
	if(update_timer)
		clearInterval(update_timer);

	cancelDVBScan();
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	menutitleobj = document.getElementById("menu_title");
	menutitleobj.lang=LANG_NAME;
	resulttitleobj = document.getElementById("result_title");
	resulttitleobj.lang=LANG_NAME;
	progressobj=document.getElementById("progress");

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	initSelected();
	update();
	LoadData();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;