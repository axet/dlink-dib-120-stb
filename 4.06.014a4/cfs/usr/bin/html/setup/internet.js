var MENULIST=new Array("INTERNET_HOMEPAGE","INTERNET_PROXY","INTERNET_BOOKMARK0","INTERNET_BOOKMARK1","INTERNET_BOOKMARK2","INTERNET_BOOKMARK3","INTERNET_BOOKMARK4");
var listobj,titleobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_INTERNET").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			//listobj[CurItem].getElementsByTagName("div")[0].className="title";
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			break;
		case 1://Click
			if(direction>0)
				saveSetting(true);

			window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_INTERNET"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}

}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbedit";
			CurItem=idx;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className="tbedit_over";
/*
			listobj[CurItem].getElementsByTagName("div")[0].className="title";
			CurItem=idx;
			listobj[CurItem].getElementsByTagName("div")[0].className="title1";
*/
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
		case 3://Mouse Over
			listobj[idx*2+1].getElementsByTagName("input")[0].focus();
	}
}

function saveSetting(cancel_update_screen)
{
	//Save HomePage
	webbrowser.homepage=listobj[1].getElementsByTagName("input")[0].value;

	//Save Proxy
	var tmp_url = listobj[3].getElementsByTagName("input")[0].value;

	if(tmp_url.length>3 && tmp_url[0]== '/')//Ignore imcomplete url
	{
		var tmp_index = tmp_url.indexOf("http://", 0);
		webbrowser.httpProxy = (tmp_index == -1)?tmp_url:tmp_url.substring(7, tmp_url.length) ;
		webbrowser.enableProxy=1;
	}
	else
	{
		webbrowser.httpProxy="";
		webbrowser.enableProxy=0;
	}
/*
	for(var i=2;i<6;i++)//2-6 is Bookmark
	{
		webbrowser.saveBookmark(i-1, listobj[2*i+1].getElementsByTagName("input")[0].value);//Index is 1 as start not 0 !!
	}
*/
	if(!cancel_update_screen)
		update();
}


function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
try{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	vallist.push(webbrowser.homepage);
	vallist.push(webbrowser.httpProxy);

	for(var i=0;i<(listobj.length/2);i++)
	{
		//var obj=listobj[i].getElementsByTagName("input")[0];
		var obj=listobj[i*2+1].getElementsByTagName("input")[0];

		//listobj[i].getElementsByTagName("div")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		obj.value=vallist[i];

		obj.style.navRight="#"+navnextobj.id;
		obj.style.navLeft="#"+navprevobj.id;

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();
}catch(err){alert(err)}

}

window.onload=initPage;
window.onunload=exitPage;