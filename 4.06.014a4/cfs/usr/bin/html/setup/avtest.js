var titleobj,menutitleobj,navprevobj,navnextobj,listobj,menuobj;
var CurItem=0;//Cur Focused New Setting
var fromurl=SETUP_PAGES["MENU_AV"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);

var displaymodedeList=new Array("NTSC","NTSC-JAPAN","PAL-M","PAL-N","PAL-NC","PAL-B","PAL-B1","PAL-D","PAL-D1","PAL","PAL-H","PAL-K","PAL-I","SECAM","480P","576P","1080I","1080I-50HZ","1080P","1080P-24HZ","1080P-25HZ","1080P-30HZ","1250I-50HZ","720P","720P-50HZ","720P-24HZ");
var waitsecs=30;
var	origDisplayMode=stb.displaymode;
var newDisplayMode=-1;
var dispmode_rollbacktimer=null;
var countdown_timer=null;

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_AV").toUpperCase();
	menutitleobj.innerText="";
	menuobj.getElementsByTagName("td")[0].innerHTML="<img src='images/info.jpg' width='43' height='43'>";
	menuobj.getElementsByTagName("td")[1].innerText=GetLangStr(STR_TYPE_CONFIRM,"APPLY");

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();
			else
				restoreDisplayMode();

			window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_AV"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function countdown()
{
	if(waitsecs>0)
	{
		menuobj.getElementsByTagName("td")[2].innerText=GetLangStr(STR_TYPE_TEXT,"ROLLBACK")+waitsecs+GetLangStr(STR_TYPE_TEXT,"TIME_SEC");
		waitsecs--;
	}
	else
		menuobj.getElementsByTagName("td")[2].innerText="";
}

function restoreDisplayMode()
{/* Rollback orig setting*/
	stb.displaymode=origDisplayMode;

	if(countdown_timer)
		clearInterval(countdown_timer);

	if(dispmode_rollbacktimer)
		clearTimeout(dispmode_rollbacktimer);

	dispmode_rollbacktimer=null;
}

function changeDisplayMode()
{
	stb.displaymode=newDisplayMode;
	countdown_timer=setInterval(countdown,1000);
	dispmode_rollbacktimer=setTimeout(exitSetting,(waitsecs+2)*1000);/* timeout to rollback */
}
function exitSetting()
{
	window.location.replace(fromurl);
	restoreDisplayMode();
}
function saveSetting()
{
	//Save Setting at exit;
	stb.save_osd();
}

function exitPage()
{
}

function initFocus()
{
	if(typeof(listobj)=='object')
		listobj[0].focus();
	else if(navprevobj)
		navprevobj.focus();
	else if(navnextobj)
	 	navnextobj.focus();
}
function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	var param_newmode=GetUrlParamValue(location.search,"dispmode");
	if((param_newmode)&&(param_newmode.length>0))
		newDisplayMode=parseInt(param_newmode);

	if((newDisplayMode<0)||(newDisplayMode>=displaymodedeList.length))
		window.location.replace(fromurl);
	else
		changeDisplayMode();

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	menutitleobj = document.getElementById("menu_title");
	menutitleobj.lang=LANG_NAME;

	menuobj=document.getElementById("menu_done");
	menuobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;
