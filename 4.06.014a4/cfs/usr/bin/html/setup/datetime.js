try{
var MENULIST=new Array("TIME_ZONE","TIME_DAYLIGHT","TIME_SERVER","TIME_UPDATE","TIME_DATE","TIME_TIME");
var titleobj,listobj,navprevobj,navnextobj,inputobj=new Array();
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_SETUP"]);

var METHODLIST=new Array("TIME_UPDATE_MANUAL","TIME_UPDATE_NTP");
var DAYLIGHTLIST=new Array("TIME_DAYLIGHT_OFF","TIME_DAYLIGHT_ON");
var DVB_ENABLED=((typeof(dvb)=='undefined')||(dvb.dvb_type==-1))?false:true;

if(DVB_ENABLED)
	METHODLIST.push("TIME_UPDATE_DVB");

var datetime_timezone;
var datetime_method;
var datetime_timeserver="";
var datetime_daylight;
var now=new Date();

//This Table must sync with porting/db_locale.c
var timezone_list = new Array (
	"USA",                   "-10:00",	/* ID=01 */
	"USA",                   "-9:00",	/* ID=02 */
	"USA",                   "-8:00",	/* ID=03 */ 
	"USA",                   "-7:00",	/* ID=04 */ 
	"USA",                   "-6:00",	/* ID=05 */
	"USA",                   "-5:00",	/* ID=06 */ 
	"Faroe Islands",         "",		/* ID=07 */ 
	"Ireland",               "",		/* ID=08 */ 
	"Portugal",              "",		/* ID=09 */
	"UK",                    "",		/* ID=10 */
	"Austria",               "+1:00",	/* ID=11 */
	"Belgium",               "+1:00",	/* ID=12 */ 
	"Bosnia Herzegovina",    "+1:00",	/* ID=13 */ 
	"Croatia",               "+1:00",	/* ID=14 */
	"Czech Republic",        "+1:00",	/* ID=15 */ 
	"Denmark",               "+1:00",	/* ID=16 */
	"France",                "+1:00",	/* ID=17 */
	"Germany",               "+1:00",	/* ID=18 */
	"Hungary",               "+1:00",	/* ID=19 */ 
	"Italy",                 "+1:00",	/* ID=20 */ 
	"Luxembourg",            "+1:00",	/* ID=21 */
	"Macedonia",             "+1:00",	/* ID=22 */
	"Malta",                 "+1:00",	/* ID=23 */
	"Namibia",               "+1:00",	/* ID=24 */ 
	"Netherlands",           "+1:00",	/* ID=25 */ 
	"Norway",                "+1:00",	/* ID=26 */
	"Poland",                "+1:00",	/* ID=27 */
	"Slovakia",              "+1:00",	/* ID=28 */
	"Slovenia",              "+1:00",	/* ID=29 */
	"Spain",                 "+1:00",	/* ID=30 */ 
	"Sweden",                "+1:00",	/* ID=31 */
	"Switzerland",           "+1:00",	/* ID=32 */
	"Belarus",               "+2:00",	/* ID=33 */
	"Estonia",               "+2:00",	/* ID=34 */
	"Finland",               "+2:00",	/* ID=35 */ 
	"Greece",                "+2:00",	/* ID=36 */
	"Israel",                "+2:00",	/* ID=37 */
	"Latvia",                "+2:00",	/* ID=38 */
	"Lithuania",             "+2:00",	/* ID=39 */
	"Moldova",               "+2:00",	/* ID=40 */
	"Romania",               "+2:00",	/* ID=41 */
	"Russian Federation",    "+2:00",	/* ID=42 */
	"South Africa",          "+2:00",	/* ID=43 */
	"Turkey",                "+2:00",	/* ID=44 */ 
	"Ethiopia",              "+3:00",	/* ID=45 */
	"Russian Federation",    "+3:00",	/* ID=46 */
	"Iran",                  "+3:30",	/* ID=47 */
	"Azerbaijan",            "+4:00",	/* ID=48 */
	"Mauritius",             "+4:00",	/* ID=49 */
	"Russian Federation",    "+4:00",	/* ID=50 */
	"Pakistan",              "+5:00",	/* ID=51 */
	"Russian Federation",    "+5:00",	/* ID=52 */
	"India",                 "+5:30",	/* ID=53 */ 
	"Sri Lanka",             "+5:30",	/* ID=54 */
	"Russian Federation",    "+6:00",	/* ID=55 */
	"Indonesia",             "+7:00",	/* ID=56 */
	"Russian Federation",    "+7:00",	/* ID=57 */
	"Thailand",              "+7:00",	/* ID=58 */
	"Vietnam",               "+7:00",	/* ID=59 */
	"Australia",             "+8:00",	/* ID=60 */
	"Brunei",                "+8:00",	/* ID=61 */ 
	"China",                 "+8:00",	/* ID=62 */
	"Hong Kong",             "+8:00",	/* ID=63 */
	"Indonesia",             "+8:00",	/* ID=64 */ 
	"Malaysia",              "+8:00",	/* ID=65 */
	"Philippines",           "+8:00",	/* ID=66 */ 
	"Russian Federation",    "+8:00",	/* ID=67 */
	"Singapore",             "+8:00",	/* ID=68 */
	"Taiwan",                "+8:00",	/* ID=69 */ 
	"Australia",             "+9:00",	/* ID=70 */
	"Indonesia",             "+9:00",	/* ID=71 */ 
	"Russian Federation",    "+9:00",	/* ID=72 */ 
	"South Korea",           "+9:00",	/* ID=73 */
	"Australia",             "+10:00",	/* ID=74 */
	"Russian Federation",    "+10:00",	/* ID=75 */
	"Australia",             "+11:00",	/* ID=76 */
	"Russian Federation",    "+11:00",	/* ID=77 */ 
	"New Zealand",           "+12:00",	/* ID=78 */ 
	"Russian Federation",    "+12:00"	/* ID=79 */
);

function updateTime()
{
	var tmpnow=new Date();
	var tar="";
	var hh=tmpnow.getHours();
	var mm=tmpnow.getMinutes();
	var ss=tmpnow.getSeconds();

	hh=(hh<10)?"0"+hh:hh;
	mm=(mm<10)?"0"+mm:mm;
	ss=(ss<10)?"0"+ss:ss;
	tar=hh+":"+mm+":"+ss;

	if(CurItem!=4)
		inputobj[2].value=tar;
}

function parse_time(timetype,src)
{
	var result=new Array();
	var xx="",yy="",zz="";
	var colcount=0;

	if(src.length>=4)
	{
		if(src.length==8)
		{
			xx=src.substr(0,4);
			yy=src.substr(4,2);
			zz=src.substr(6,2);
			colcount=3;
		}
		else if(src.length>=4)
		{
			xx=src.substr(0,2);
			yy=src.substr(2,2);
			colcount=2;
			if(src.length==6)
			{
				zz=src.substr(4,2);
				colcount=3;
			}
		}
	}
	xx=(xx.charAt(0)=='0')?xx.substr(1,1):xx;
	xx=parseInt(xx);
	yy=(yy.charAt(0)=='0')?yy.substr(1,1):yy;
	yy=parseInt(yy);
	if(colcount==3)
	{
		zz=(zz.charAt(0)=='0')?zz.substr(1,1):zz;
		zz=parseInt(zz);
	}


	if(timetype==0)//Date
	{
		if(colcount==2)
		{
			zz=yy;
			yy=xx;
			xx=now.getFullYear();
		}
		else if(colcount==3)
		{
			xx=(xx<100)?xx+2000:xx;
		}
		xx=((xx>=0)&&(xx<2099))?xx:now.getFullYear();
		yy=((yy>0)&&(yy<=12))?yy-1:now.getMonth();
		zz=((zz>0)&&(zz<=31))?zz:now.getDate();
		now.setYear(xx);
		now.setMonth(yy);
		now.setDate(zz);
		xx=now.getFullYear();
		yy=now.getMonth()+1;
		zz=now.getDate();
	}
	else //Time
	{
		xx=((xx>=0)&&(xx<24))?xx:now.getHours();
		yy=((yy>=0)&&(yy<60))?yy:now.getMinutes();
		zz=((zz>=0)&&(zz<60))?zz:now.getSeconds();
		now.setHours(xx);
		now.setMinutes(yy);
		now.setSeconds(zz);
		xx=now.getHours();
		yy=now.getMinutes();
		zz=now.getSeconds();
	}

	result.push(xx);
	result.push(yy);
	result.push(zz);
	return result;
}

function time_str_convert(src,convert_type)
{
	var tar="",val_arr;
	var yy,mm,dd,hh,mm,ss;
	var hh,mm,ss;
	switch(convert_type)
	{
		case 1:
			val_arr=src.split("/");
			tar +=(parseInt(val_arr[0])<10)?"0"+val_arr[0]:val_arr[0];
			tar +=(parseInt(val_arr[1])<10)?"0"+val_arr[1]:val_arr[1];
			tar +=(parseInt(val_arr[2])<10)?"0"+val_arr[2]:val_arr[2];
			break;
		case 2:
			val_arr=src.split(":");
			tar=val_arr.join("");
			break;
		case -1:
			val_arr=parse_time(0,src);
			tar=val_arr.join("/");
			break;
		case -2:
			val_arr=parse_time(1,src);
			tar +=(val_arr[0]<10)?"0"+val_arr[0]+":":val_arr[0]+":";
			tar +=(val_arr[1]<10)?"0"+val_arr[1]+":":val_arr[1]+":";
			tar +=(val_arr[2]<10)?"0"+val_arr[2]:val_arr[2];
			break;
	}
	return tar;
}

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_DATETIME").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
	navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			var isinput=((CurItem==2)||(CurItem>3))?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0://Focus
			var isinput=((CurItem==2)||(CurItem>3))?true:false;
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(isinput)?"tbedit":"tbnav";
			CurItem=idx;
			isinput=((CurItem==2)||(CurItem>3))?true:false;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className=(isinput)?"tbedit_over":"tbnav_over";
			switch(CurItem)
			{
				case 4:
					inputobj[CurItem-2].value=time_str_convert(inputobj[CurItem-2].value,1);
					break;
				case 5:
					inputobj[CurItem-2].value=time_str_convert(inputobj[CurItem-2].value,2);
					break;
			}
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			if((key==0x25)||(key==0x27))
			{
				var direction=(key==0x27)?1:-1;
				switch(idx)
				{
					case 0:
						datetime_timezone=(datetime_timezone+direction+(timezone_list.length/2))%(timezone_list.length/2);
						listobj[2*idx+1].innerText="(GMT" + timezone_list[2*datetime_timezone+1] + ") " + timezone_list[2*datetime_timezone].substr(0,13);
						break;
					case 1:
						datetime_daylight=(datetime_daylight +1)%2;
						listobj[2*idx+1].innerText=GetLangStr(STR_TYPE_TITLE,DAYLIGHTLIST[datetime_daylight]);
						break;
					case 3:
						datetime_timemethod=(datetime_timemethod+direction+METHODLIST.length)%METHODLIST.length;
						listobj[2*idx+1].innerText=GetLangStr(STR_TYPE_TITLE,METHODLIST[datetime_timemethod]);

						if(datetime_timemethod==0)
							updateTime();

						var disp=(datetime_timemethod!=0)?"none":"";
						listobj[8].style.display=disp;
						listobj[9].style.display=disp;
						listobj[10].style.display=disp;
						listobj[11].style.display=disp;
						break;
				}
			}
			break;
		case 3://Mouse Over
			if(typeof(listobj[idx*2+1].getElementsByTagName("input"))=="object")
				listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
		case 4://Blur
			switch(idx)
			{
				case 1:
					var rv=true;
					datetime_timeserver=listobj[3].getElementsByTagName("input")[0].value;
					break;
				case 4://Convert to Correct Time String Type
					inputobj[1].value=time_str_convert(inputobj[1].value,-1);
					break;
				case 5:
					inputobj[2].value=time_str_convert(inputobj[2].value,-2);
					break;
			}
			break;
	}
}


function saveSetting()
{
	try{
		stb.timeZone=datetime_timezone+1;
		stb.timeMethod=datetime_timemethod;
		stb.timeServer=datetime_timeserver;
		stb.daylight=datetime_daylight;

		switch(stb.timeMethod)
		{
			case 0:
				stb.setSystemTime(now.getFullYear(),now.getMonth()+1,now.getDate(),now.getHours(),now.getMinutes(),now.getSeconds());
				break;
			case 1:
				stb.ntpUpdate();
				break;
		}
	}catch(err){stb.print(err)}
}

function initFocus()
{
	listobj[1].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
//	listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	datetime_timezone=parseInt(stb.timeZone)-1;
	datetime_timemethod=parseInt(stb.timeMethod);
	datetime_timemeserver=stb.timeServer;
	vallist.push("(GMT" + timezone_list[2*datetime_timezone+1] + ") " + (timezone_list[2*datetime_timezone]).substr(0,13));
	datetime_daylight=(stb.daylight)?1:0;
	vallist.push(GetLangStr(STR_TYPE_TITLE,DAYLIGHTLIST[datetime_daylight]));
	vallist.push(datetime_timemeserver);
	vallist.push(GetLangStr(STR_TYPE_TITLE,METHODLIST[datetime_timemethod]));
	vallist.push(now.getFullYear()+"/"+(now.getMonth()+1)+"/"+now.getDate());
	vallist.push(now.getHours()+":"+now.getMinutes()+":"+now.getSeconds());
try{
	for(var i=0;i<(listobj.length/2);i++)
	{
//		listobj[i].getElementsByTagName("div")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		if((i==2)||(i>3))
		{
			//var obj=listobj[i].getElementsByTagName("input")[0];
			var obj=listobj[i*2+1].getElementsByTagName("input")[0];
			inputobj.push(obj);
			obj.value=vallist[i];
		}
		else
		{
			//var obj=listobj[i].getElementsByTagName("div")[1];
			var obj=listobj[i*2+1];
			obj.innerText=vallist[i];

		}

		obj.style.navLeft="#"+obj.id;
		obj.style.navRight="#"+obj.id;
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
		eval('obj.addEventListener("blur", function(event){processItem(event,'+i+',4)}, false);');
	}
}catch(err){stb.print(err)}

	if(stb.timeMethod!=0)
	{
		listobj[8].style.display="none";
		listobj[9].style.display="none";
		listobj[10].style.display="none";
		listobj[11].style.display="none";
	}

	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;
}catch(err){stb.print(err)}