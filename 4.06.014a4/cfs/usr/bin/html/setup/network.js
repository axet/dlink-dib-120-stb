var titleobj,menutitleobj,navprevobj,navnextobj;
var CurItem=0;//Cur Focused New Setting
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_SETUP"]);
var NetPages=new Array("setup_network_wire","setup_network_wireless","setup_network_mode","setup_network_static","setup_network_dhcp","setup_network_pppoe","setup_network_done");
var StartPageIdx=2;
var EndPageIdx=NetPages.length-1;
var CurPageIdx=StartPageIdx;

var PagePrefix=new Array("w","z","m","s","z","p","z");
function PagePrev(){
	document.getElementById(NetPages[CurPageIdx]).style.display="none";
	navprevobj.focus();

	if(CurPageIdx==2)
	{
		CurPageIdx -= 2;//Always to wired when wireless disable
	}
	else if((CurPageIdx==3)||(CurPageIdx==4)||(CurPageIdx==5))
	{
		CurPageIdx = 2;
	}
	else if(CurPageIdx==6)
	{
		CurPageIdx -= 3-network.netmode;
		if(CurPageIdx==4)
			CurPageIdx=2;
	}
	else
		CurPageIdx--;

	if(CurPageIdx<StartPageIdx)//All Done
		window.location.replace(fromurl);
	else
	{
		if((CurPageIdx==0)||(CurPageIdx==2))
		{
			navnextobj.className="funcnone";
			navnextobj.innerHTML="&nbsp;";
		}
		else
		{
			navnextobj.className=(CurPageIdx==EndPageIdx)?"funcfinish":"funcnext";
			navnextobj.innerText=(CurPageIdx==EndPageIdx)?GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH"):GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
		}
		document.getElementById(NetPages[CurPageIdx]).style.display="";	
		initNetPage();
	}
}

function PageNext(){
	if(saveSetting()!=true)return false;
	document.getElementById(NetPages[CurPageIdx]).style.display="none";

	navprevobj.focus();

	if(CurPageIdx==0)
	{
		CurPageIdx +=2-CurItem;
	}
	else if(CurPageIdx==1)
	{
		CurPageIdx--;//Return to Previous Page
	}
	else if(CurPageIdx==2)
	{
		CurPageIdx += CurItem+1;
	}
	else if((CurPageIdx==3)||(CurPageIdx==4)||(CurPageIdx==5))
	{
		CurPageIdx=EndPageIdx;
	}
	else
		CurPageIdx++;

	if(CurPageIdx>=NetPages.length)//All Done
		window.location.replace(tourl[0]);
	else
	{
		if((CurPageIdx==0)||(CurPageIdx==2))
		{
			navnextobj.className="funcnone";
			navnextobj.innerHTML="&nbsp;";
		}
		else
		{
			navnextobj.className=(CurPageIdx==EndPageIdx)?"funcfinish":"funcnext";
			navnextobj.innerText=(CurPageIdx==EndPageIdx)?GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH"):GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
		}
		document.getElementById(NetPages[CurPageIdx]).style.display="";
		initNetPage();
	}
}

function initNetPage()
{
	var pageid=NetPages[CurPageIdx];
	var mainobj=document.getElementById(pageid);
	var firstfocusitem=null;
	var listobj;

	if(mainobj.getElementsByTagName("UL")[0]!=null)
	{
		listobj  = mainobj.getElementsByTagName("UL")[0].getElementsByTagName("LI");
	}

	CurItem=0;

	var titlelist=new Array();
	var vallist=new Array();
	switch(CurPageIdx)
	{
		case 0://Wire or Wireless Selection
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"CHOOSE_NETWORK_WIRE");
			var MENULIST=new Array("NETWORK_WIRE","NETWORK_WIRELESS");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			break;
		case 1://Wireless
			menutitleobj.innerText="";
			var htmltext="<img src='images/info.jpg' width='43' height='43'>&nbsp;"+GetLangStr(STR_TYPE_TEXT,"FUNC_DISABLED");
			mainobj.getElementsByTagName("DIV")[0].innerHTML=htmltext;
			break;
		case 2://Network Mode
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"CHOOSE_NETWORK_TYPE");
			var MENULIST=new Array("NETWORK_STATIC","NETWORK_DHCP","NETWORK_PPPOE");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			break;
		case 3://Static IP
			menutitleobj.innerText="";
			var MENULIST=new Array("NETWORK_IP","NETWORK_MASK","NETWORK_GATEWAY","NETWORK_DNS1","NETWORK_DNS2");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			vallist.push(network.netip);
			vallist.push(network.netmask);
			vallist.push(network.netgateway);
			vallist.push(network.netdns);
			vallist.push(network.netdns2);
			break;
		case 4://DHCP
			menutitleobj.innerText="";
			var htmltext="<img src='images/info.jpg' width='43' height='43'>&nbsp;"+GetLangStr(STR_TYPE_TEXT,"DHCP_ENABLED");
			mainobj.getElementsByTagName("DIV")[0].innerHTML=htmltext;
			break;
		case 5://PPPOE
			menutitleobj.innerText="";
			var MENULIST=new Array("NETWORK_PPPOE_USER","NETWORK_PPPOE_PWD");
			for(var i=0;i<listobj.length;i++)
			{titlelist.push(GetLangStr(STR_TYPE_TITLE,MENULIST[i]));}
			vallist.push(network.pppoeUserName);
			vallist.push(network.pppoePassword);
			break;
		case 6://Done
			menutitleobj.innerText="";
			var htmltext="<img src='images/info.jpg' width='43' height='43'>&nbsp;<div style='top:-54;left:50;position:relative;'>"+GetLangStr(STR_TYPE_TEXT,"FUNC_DONE")+"</div>";
			mainobj.getElementsByTagName("DIV")[0].innerHTML=htmltext;
			break;
	}


	if(typeof(listobj)=='object')
	{
		for(var i=0;i<listobj.length;i++)
		{
			var prev = ((i-1)+listobj.length)%listobj.length;
			var next = (i+1)%listobj.length;
			inp=listobj[i].getElementsByTagName("input")[0];
			if(inp&&(i<vallist.length))
			{
				listobj[i].getElementsByTagName("div")[0].innerText=titlelist[i];
				inp.addEventListener("focus", function(event){processItem(event,0)}, false);
				inp.addEventListener("mouseover", function(event){processItem(event,3)}, false);
				inp.value=vallist[i];
				if(!firstfocusitem)
					firstfocusitem=inp;
					
			}
			else
			{
				listobj[i].addEventListener("focus", function(event){processItem(event,0)}, false);
				listobj[i].addEventListener("click", function(event){processItem(event,1)}, false);
				listobj[i].addEventListener("keypress", function(event){processItem(event,2)}, false);
				listobj[i].innerText=titlelist[i].toUpperCase();
				if(!firstfocusitem)
					firstfocusitem=listobj[i];
			}

			listobj[i].style.navRight="#"+navprevobj.id;
			listobj[i].style.navLeft="#"+navprevobj.id;
		}
	}

	if(firstfocusitem)
		firstfocusitem.focus();
	else
	{
		if((CurPageIdx==EndPageIdx) && navnextobj)
			navnextobj.focus();
		else
			navprevobj.focus();
	}

}

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_NETWORK").toUpperCase();

	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if((CurPageIdx==0)||(CurPageIdx==2))
	{
		navnextobj.className="funcnone";
		navnextobj.innerHTML="&nbsp;";
	}
}

function checkip(addr)
{
	var rv=false;
	var pattern=/^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/;

	for(var i=0;i<5;i++)
	{
		if(i==1)continue;//Skip Netmask Check because it's not a ip address
		eval('rv=pattern.test(document.getElementById("s'+i+'").value);');
		if(!rv)
		{
			//DNS2 is optional data if leave blank
			if((i==4)&&(document.getElementById("s4").value==""))
			{
				rv=true;
				break;
			}

			eval('document.getElementById("s'+i+'").focus()');
			alert('Address invalid.');
			break;
		}
	}

	return rv;
}

function processFunc(event,eventtype)
{
	var direction=(event.target.id=='navprev')?-1:1;// back:-1 next:1
	var clicked=false;

	switch(eventtype)
	{
		case 0://Focus
			var pageid=NetPages[CurPageIdx];
			var mainobj=document.getElementById(pageid);
			var listobj;

			if(mainobj.getElementsByTagName("UL")[0]!=null)
			{
				listobj  = mainobj.getElementsByTagName("UL")[0].getElementsByTagName("LI");
				listobj[CurItem].getElementsByTagName("div")[0].className="title";
			}
			break;
		case 1://Click
			clicked=true;
			if(direction>0)
				PageNext();
			else
				PagePrev();
			break;
	}
}

function processItem(event,eventtype)
{
	var direction=1;//Default Right ->
	var clicked=false;
	var tmp=event.target.id;
	var idx=parseInt(tmp.substr(1,10));
	var item_id_prefix=tmp.charAt(0);
	var input_prefix_filter=new Array("s","p");
	var isinputitem=false;

	for(var i=0;i<input_prefix_filter.length;i++)
	{
		if(item_id_prefix.indexOf(input_prefix_filter[i])>=0)
			isinputitem=true;
	}

	switch(eventtype)
	{
		case 0://Focus
			if(isinputitem)
			{
				var listobj  = document.getElementById(NetPages[CurPageIdx]).getElementsByTagName("UL")[0].getElementsByTagName("LI");
				listobj[CurItem].getElementsByTagName("div")[0].className="title";
				CurItem=idx;
				listobj[CurItem].getElementsByTagName("div")[0].className="title1";
			}
			break;
		case 1://Click
			clicked=true;
			CurItem=idx;
			if(!isinputitem)
			{
				if(direction>0)
					PageNext();
				else
					PagePrev();
			}
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
		case 3://Mouse Over
			if(isinputitem)
			{
				var listobj  = document.getElementById(NetPages[CurPageIdx]).getElementsByTagName("UL")[0].getElementsByTagName("LI");

				if(typeof(listobj[idx].getElementsByTagName("input"))=="object")
					listobj[idx].getElementsByTagName("input")[0].focus();
				else
					stb.print("No");
			}
			break;
	}
}

function saveSetting()
{
	var rv=true;//true:no error  false:error

	switch(CurPageIdx)
	{
		case 0://Wired or Wireless
			if(CurItem==0)//Wireless Disable so don't save wireless setting
				network.wired=(CurItem==0)?1:0;
			break;
		case 1://Wireless
			break;
		case 2://Network type
			//Save at IP/DHCP/PPPoE Page
			break;
		case 3://STATIC IP
			var is_modified=((document.getElementById("s0").value != network.netip)
							||(document.getElementById("s1").value != network.netmask)
							||(document.getElementById("s2").value != network.netgateway)
							||(document.getElementById("s3").value != network.netdns)
							||(document.getElementById("s4").value != network.netdns2));

			if((is_modified)||(network.netmode==1))//If IP Modified or Network Mode Changed
			{
				//Check IP
				rv=checkip();

				if(rv)
				{
					network.netsetup(	document.getElementById("s0").value,
										document.getElementById("s1").value,
										document.getElementById("s2").value,
										document.getElementById("s3").value,
										document.getElementById("s4").value);
		    		network.netmode=0;
	    		}
			}
			break;
		case 4://DHCP
	    	network.netmode=1;
			break;
		case 5://PPPOE
		    network.pppoeUserName=document.getElementById("p0").value;
		    network.pppoePassword=document.getElementById("p1").value;
		    network.netmode=2;
			break;
	}

	return rv;
}

function exitPage()
{
}

function initWizard(){
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	menutitleobj = document.getElementById("menu_title");
	menutitleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,1)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1)}, false);
	navnextobj.lang=LANG_NAME;
	navnextobj.style.navRight="#"+navnextobj.id;

	//Initialize Language
	for(var i=0;i<NetPages.length;i++)
	{
		var obj=document.getElementById(NetPages[i]);
		obj.style.display="none";
		obj.lang=LANG_NAME;
	}

	//First Page;
	document.getElementById(NetPages[CurPageIdx]).style.display="";
	initNetPage();

	update();
}

window.onload=initPage;
window.onunload=exitPage;