var MENULIST=new Array("BTN_YES","BTN_NO");
var titleobj,menutitleobj,listobj,navprevobj,navnextobj;
var CurItem=0;//Cur Focused New Setting
var fromurl=SETUP_PAGES["MENU_SYSTEM"];
var tourl=new Array(SETUP_PAGES["MENU_SYSTEM"]);
var progressobj;

/* firmware upgrade status */
var FWUP_UNKNOW_STATUS         = 0;
var FWUP_WAIT_STATUS           = 1;
var FWUP_BUSY_STATUS           = 2;
var FWUP_DOWNLOAD_START        = 3;
var FWUP_DOWNLOAD_FINISH       = 4;
var FWUP_INFO_DOWNLOAD_START   = 5;
var FWUP_INFO_DOWNLOAD_FINISH  = 6;
var FWUP_WRITE_START           = 7;
var FWUP_WRITE_FINISH          = 8;
var FWUP_DOWNLOAD_FAIL         = 9;
var FWUP_INFO_DOWNLOAD_FAIL    = 10;
var FWUP_WRITE_FAIL            = 11;
var FWUP_RAMDISK_FAIL          = 12;
var FWUP_USB_FAIL              = 13;
var FWUP_FIRM_FILE_FAIL        = 14;
var FWUP_URL_FILE_FAIL         = 15;
var FWUP_INFO_FILE_FAIL        = 16;
var FWUP_EXECMD_FAIL           = 17;
var FWUP_FIRM_MD5_FAIL         = 18;
var FWUP_USER_CANCEL           = 19;


function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_UPDATE").toUpperCase();
	menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"UPDATE");

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventtype)
{
	var direction=(event.target.id=='navprev')?-1:1;// back:-1 next:1
	var clicked=false;

	if(stb.ismodal==1)
		return false;

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			clicked=true;
			//Contiue without break
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;

			if(key==0x25)
				direction=-1;

			if( ((direction>0)&&((key==0x27)||(key==0xD)||clicked))//Next
			  ||((direction<0)&&((key==0x25)||(key==0xD)||clicked))//Prev
			)
			{
				saveSetting();
			}
			break;
	}
}

function processItem(event,eventtype)
{
	var direction=1;//Default Right ->
	var clicked=false;

	if(stb.ismodal==1) 
		return false;

	CurItem=parseInt(event.target.id);

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			clicked=true;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;

			if( ((direction>0)&&((key==0x27)||(key==0xD)||clicked))//Next
			  ||((direction<0)&&((key==0x25)||(key==0xD)||clicked))//Prev
			)
			{
				saveSetting();
			}
			break;
	}
}

function saveSetting(cancel_update_screen)
{
	if(!cancel_update_screen)
		update();

	window.location.replace(fromurl);
}

function updateStatus()
{
	var progress_percent=0;
	var progress_unit=0;
	var next_alarm=true;

	switch (stb.fwUpgradeStatus)
	{
		case FWUP_DOWNLOAD_START:
			stb.ismodal=1;//Global Modal-Mode to reject all operation even reboot key by joe
			//Because Even Download Fail still will reboot
			//Hide Back Item
			navprevobj.style.visibility="hidden";
			navprevobj.style.display="none";
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"UPDATE_DOWNLOAD");
			progress_percent=stb.fwUpgradeDownloadPercent;
			progress_unit=parseInt((progress_percent*50)/100);
			break;
		case FWUP_DOWNLOAD_FINISH:
			break;
		case FWUP_INFO_DOWNLOAD_START:    
			break;
		case FWUP_INFO_DOWNLOAD_FINISH:
			break;
		case FWUP_WRITE_START:
			stb.ismodal=1;//Global Modal-Mode to reject all operation even reboot key
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"UPDATE_WRITE");
			//progress_percent=stb.fwUpgradeWritePercent;
			//progress_unit=parseInt((progress_percent*50)/100);
			//Hide Back Item
			navprevobj.style.visibility="hidden";
			navprevobj.style.display="none";
			next_alarm=false;
			break;
		case FWUP_WRITE_FINISH:
			stb.ismodal=0;
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"UPDATE_DONE");
			break;
		case FWUP_WRITE_FAIL:
			stb.ismodal=0;
			menutitleobj.innerText = "Firmware Write Fail.";
			next_alarm=false;
			break;
		case FWUP_RAMDISK_FAIL:
			menutitleobj.innerText = "Ramdisk Fail.";
			next_alarm=false;
			break;
		case FWUP_USB_FAIL:
			var htmltext="<img src='images/info.jpg' width='43' height='43' style='top:14;position:relative'>&nbsp;"+GetLangStr(STR_TYPE_ERROR,"UPDATE_USB_ERROR");
			menutitleobj.innerHTML=htmltext;
			next_alarm=false;
			break;
		case FWUP_DOWNLOAD_FAIL:
			var htmltext="<img src='images/info.jpg' width='43' height='43' style='top:14;position:relative'>&nbsp;"+GetLangStr(STR_TYPE_ERROR,"UPDATE_DOWNLOAD_FAIL");
			menutitleobj.innerHTML=htmltext;
			next_alarm=false;
			break;
		case FWUP_INFO_DOWNLOAD_FAIL:
			menutitleobj.innerText = "Firmware Info Fail";
			next_alarm=false;
			break;
		case FWUP_FIRM_FILE_FAIL :
		case FWUP_URL_FILE_FAIL:
		case FWUP_INFO_FILE_FAIL:
			var htmltext="<img src='images/info.jpg' width='43' height='43' style='top:14;position:relative'>&nbsp;"+GetLangStr(STR_TYPE_ERROR,"UPDATE_FILE_ERROR");
			menutitleobj.innerHTML=htmltext;
			next_alarm=false;
			break;
		case FWUP_USER_CANCEL:
			stb.ismodal=0;
			menutitleobj.innerText = "Cancel Firware Update.";
			next_alarm=false;
			break;
		default:
			menutitleobj.innerText=GetLangStr(STR_TYPE_TEXT,"UPDATE");
			break;
	}

	progressobj.getElementsByTagName("span")[0].style.width=progress_unit*9;
	progressobj.getElementsByTagName("span")[1].innerText=progress_percent+" %";

	if(next_alarm)
		setTimeout(updateStatus,1000);
}

function LoadData()
{
	var result=false;

	var param_updatetype=GetUrlParamValue(location.search,"updatetype");
	progressobj=document.getElementById("progress");
	if(param_updatetype==1)
		stb.fwUpgrade(2);//From USB
	else if(param_updatetype==2)
	    stb.fwUpgrade(3);//From Multicast
	else
		stb.fwUpgrade(1);//From Network (Default)

	setTimeout(updateStatus,1000);
	//stb.ismodal=1;
	return false;
}

function initSelected()
{
}

function initFocus()
{
	navprevobj.focus();
}

function exitPage()
{
	stb.cancelFWUpgrade();
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	menutitleobj = document.getElementById("menu_title");
	menutitleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	initSelected();

	update();
	LoadData();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;
