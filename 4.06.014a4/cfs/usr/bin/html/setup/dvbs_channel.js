var listobj,titleobj,navprevobj,navnextobj,navfuncextobj,navaddobj,navresetobj;
var fromurl=SETUP_PAGES["MENU_DVBS"];
var tourl=new Array(SETUP_PAGES["MENU_SCAN"]);
var sat_id=0;
var editbox;
var DBIsChanged=false;//If Changed ,we save DB at exit;

function SATCHANNEL()
{
	this.start_idx=0;
	this.listobj=new Array();
}

SATCHANNEL.prototype.processItem=function(event,eventflag,eventtype){
	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			var idx=this.start_idx+eventflag;
			editbox.modify(idx);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var direction=(key==0x25)?-1:1;

			switch(key)
			{
				case 0x8:/* Delete */
					var idx=eventflag;
					if((idx<dvb.satellites[sat_id].satchannels.length)
						&&(dvb.satellites[sat_id].satchannels.length>1))
					{
						dvb.satellites[sat_id].satchannels[idx].del();
						DBIsChanged=true;
						this.update();
						idx=(idx>1)?idx-1:0;
						if(dvb.satellites[sat_id].satchannels.length>0)
							this.focus(idx);
						else
							navnextobj.focus();
					}
					break;
				case 0x21:/*PAGE UP*/
					satch.nav(-1);
					break;
				case 0x22:/*PAGE DOWN*/
					satch.nav(1);
					break;
			}
			break;
	}
}
SATCHANNEL.prototype.bindObject=function(rootid,pgupid,pgdownid){
	var me=this;
	this.rootobj=document.getElementById(rootid);
	this.rootobj.lang=LANG_NAME;

	this.pgupobj=document.getElementById(pgupid);
	this.pgdownobj=document.getElementById(pgdownid);

	this.listobj=this.rootobj.getElementsByTagName("UL")[0].getElementsByTagName("LI");

	for(var i=0;i<this.listobj.length;i++)
	{
		var obj=this.listobj[i];
		eval('obj.addEventListener("focus", function(event){me.processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){me.processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){me.processItem(event,'+i+',2)}, false);');
		obj.style.navLeft="#"+obj.id;
		obj.style.navRight="#"+obj.id;
	}
}

SATCHANNEL.prototype.show_page_navigator=function(){
	if(dvb.satellites[sat_id].satchannels.length==0)
	{
		this.pgupobj.className="pgup0";
		this.pgdownobj.className="pgdown0";
	}
	else
	{
		this.pgupobj.className=(this.start_idx>0)?"pgup1":"pgup0";
		if(dvb.satellites[sat_id].satchannels.length>this.listobj.length)
			this.pgdownobj.className=((this.start_idx+this.listobj.length)<=(dvb.satellites[sat_id].satchannels.length-1))?"pgdown1":"pgdown0";
	}
}

SATCHANNEL.prototype.update=function(){

	for(var i=0;i<this.listobj.length;i++)
	{
		var idx=this.start_idx+i;
		if(idx>=dvb.satellites[sat_id].satchannels.length)
			break;
		this.listobj[i].className="itemnavlong";
		var tmp=0;
		tmp=parseInt(dvb.satellites[sat_id].satchannels[idx].freq);
		tmp=(tmp/1000000)-((tmp%10000)/1000000);
		this.listobj[i].getElementsByTagName("DIV")[0].innerText="Freq:"+tmp+" Mhz";
		tmp=parseInt(dvb.satellites[sat_id].satchannels[idx].symbol_rate);
		tmp=(tmp/1000000)-((tmp%10000)/1000000);
		this.listobj[i].getElementsByTagName("DIV")[1].innerText="SymRate:"+tmp+" Mhz";
	}

	while(i<this.listobj.length)
	{
		this.listobj[i].className="item0";
		i++;
	}

	this.show_page_navigator();
}
SATCHANNEL.prototype.nav=function(direction){
	switch(direction)
	{
		case -1://Page Prev
			this.start_idx -=this.listobj.length;
			this.start_idx=(this.start_idx<0)?0:this.start_idx;
			this.update();
			break;
		case 1://Page Next
			if(dvb.satellites[sat_id].satchannels.length>this.listobj.length)
			{
				this.start_idx +=this.listobj.length;
				this.start_idx=(this.start_idx>=this.listobj.length)?dvb.satellites[sat_id].satchannels.length-this.listobj.length:this.start_idx;
				this.update();
			}
			break;
	}
}
SATCHANNEL.prototype.focus=function(idx){
	if(idx<satch.listobj.length)
		satch.listobj[idx].focus();
}
SATCHANNEL.prototype.show=function(){
	this.rootobj.style.visibility="inherit";
}

SATCHANNEL.prototype.hide=function(){
	this.pgupobj.className="pgup0";
	this.pgdownobj.className="pgdown0";
	this.rootobj.style.visibility="hidden";
}

var satch=new SATCHANNEL;

function EDITBOX(){
	var MENULIST=new Array("DVB_FREQ","DVB_SYMBOL_RATE","DVB_POLARITY");
	this.POLAR_LIST=new Array("13V","18V");
	this.POLAR_VAL_LIST=new Array(13,18);
	this.rootobj=document.getElementById("setup_menu_add");
	this.rootobj.style.visibility="hidden";
	this.rootobj.lang=LANG_NAME;
	this.visible=false;
	this.opmode=0;//0-Add 1-Mod
	this.freq=0;
	this.symbol_rate=0;
	this.polarity=0;
	var me=this;
	this.listobj=this.rootobj.getElementsByTagName("UL")[0].getElementsByTagName("LI");
	for(var i=0;i<this.listobj.length;i++)
	{
		this.listobj[i].getElementsByTagName("div")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		if(i==2)
		{
			obj=this.listobj[i].getElementsByTagName("div")[1];
		}
		else
		{
			obj=this.listobj[i].getElementsByTagName("input")[0];
		}
		eval('obj.addEventListener("focus", function(event){me.processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){me.processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){me.processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("blur", function(event){me.processItem(event,'+i+',3)}, false);');
		obj.style.navLeft="#"+obj.id;
		obj.style.navRight="#"+obj.id;
	}
}
EDITBOX.prototype.add=function(){
	this.opmode=0;
	this.freq=0;
	this.symbol_rate=0;
	this.show();
}
EDITBOX.prototype.modify=function(ch_idx){
	if(ch_idx<dvb.satellites[sat_id].satchannels.length)
	{
		this.ch_idx=ch_idx;
		this.opmode=1;
		this.freq=dvb.satellites[sat_id].satchannels[this.ch_idx].freq;
		this.symbol_rate=dvb.satellites[sat_id].satchannels[this.ch_idx].symbol_rate;
		for(var i=0;i<this.POLAR_VAL_LIST.length;i++)
		{
			if(this.POLAR_VAL_LIST[i]==dvb.satellites[sat_id].satchannels[this.ch_idx].diseqc_polarity)
				this.polarity=i;
		}
		this.show();
	}
}

EDITBOX.prototype.show=function(){
	this.rootobj.style.visibility="inherit";
	this.visible=true;
	this.update();
	satch.hide();
	navaddobj.style.visibility="hidden";
	navresetobj.style.visibility="hidden";
	navprevobj.focus();
}
EDITBOX.prototype.hide=function(){
	this.rootobj.style.visibility="hidden";
	this.visible=false;
	satch.focus(this.ch_idx);
	satch.update();
	satch.show();
	navaddobj.style.visibility="inherit";
	navresetobj.style.visibility="inherit";
}

EDITBOX.prototype.update=function(){
	var vallist=new Array();
	vallist.push(this.freq);
	vallist.push(this.symbol_rate);
	vallist.push(this.POLAR_LIST[this.polarity]);

	for(var i=0;i<this.listobj.length;i++)
	{
		if(i<2)
			this.listobj[i].getElementsByTagName("input")[0].value=vallist[i];
		else
			this.listobj[i].getElementsByTagName("div")[1].innerText=vallist[i];
	}
}
EDITBOX.prototype.verify=function(idx){
	var isvalid=true;
	var tmp=0;
	tmp=parseInt(this.listobj[0].getElementsByTagName("input")[0].value);
	if((tmp<=0)||(tmp>=4294967296))
		isvalid=false;

	tmp=parseInt(this.listobj[1].getElementsByTagName("input")[0].value);
	if((tmp<=0)||(tmp>=4294967296))
		isvalid=false;

	return isvalid;
}
EDITBOX.prototype.save=function(){
	if(this.verify())
	{
		this.freq=parseInt(this.listobj[0].getElementsByTagName("input")[0].value);
		this.symbol_rate=parseInt(this.listobj[1].getElementsByTagName("input")[0].value);

		if(this.opmode==0)
			dvb.satellites[sat_id].satchannels.addchannel(this.freq,this.symbol_rate,this.POLAR_VAL_LIST[this.polarity]);
		else if(this.opmode==1)
			dvb.satellites[sat_id].satchannels[this.ch_idx].set(this.freq,this.symbol_rate,this.POLAR_VAL_LIST[this.polarity]);

		this.hide();
		DBIsChanged=true;
	}
	else
		alert('Please Input Valid Value');
}

EDITBOX.prototype.processItem=function(event,eventflag,eventtype){
	switch(eventtype)
	{
		case 0://Focus
			var tmpstyle="title1";
			if(eventflag==2)
				tmpstyle="title_nav";
			this.listobj[eventflag].getElementsByTagName("div")[0].className=tmpstyle;
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var direction=(key==0x25)?-1:1;
			if((key==0x25)||(key==0x27))
			{
				switch(eventflag)
				{
					case 2:
						this.polarity=(this.polarity+direction)%this.POLAR_LIST.length;
						this.listobj[eventflag].getElementsByTagName("div")[1].innerText=this.POLAR_LIST[this.polarity];
						break;
				}
			}
			break;
		case 3://Blur
			this.listobj[eventflag].getElementsByTagName("div")[0].className="title";
			break;
	}
}

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_TITLE,"DVB_TYPE_S").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
	{
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_FINISH");
		navnextobj.className="funcfinish";
	}

	if(navaddobj)
		navaddobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_ADD");

	if(navresetobj)
		navresetobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_RESET");
}

function processFunc(event,eventflag,eventtype)
{
	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			switch(eventflag)
			{
				case 1:
					if(editbox.visible)
						editbox.save();
					else
					{
						saveSetting();
						window.location.replace(tourl[0]);
					}
					break;
				case -1:
					if(editbox.visible)
						editbox.hide();
					else
						window.location.replace(fromurl);
					break;
				case 2://Add
					editbox.add();
					break;
				case 3://Reset
					dvb.satellites[sat_id].satchannels.resetchannel();
					satch.update();
					DBIsChanged=true;
					break;
			}
			break;
	}
}

function saveSetting()
{
}

function initFocus()
{
	if(dvb.satellites[sat_id].satchannels.length>0)
		satch.listobj[0].focus();
	else
		navprevobj.focus();
}

function exitPage()
{
	if(DBIsChanged)
		dvb.satellites.save();
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	var param_sat_id=GetUrlParamValue(location.search,"sat_id");
	if((!param_sat_id)||(param_sat_id<0)||(param_sat_id>=dvb.satellites.length))
		document.location.replace(fromurl);
	else
		sat_id=param_sat_id;

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	navaddobj=document.getElementById("navadd");
	navaddobj.addEventListener("click", function(event){processFunc(event,2,1)}, false);
	navaddobj.lang=LANG_NAME;

	navresetobj=document.getElementById("navreset");
	navresetobj.addEventListener("click", function(event){processFunc(event,3,1)}, false);
	navresetobj.lang=LANG_NAME;

	satch.bindObject("setup_menu","pgup","pgdown");
	satch.update();
	update();

	editbox=new EDITBOX;

	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;