var MENULIST=new Array("TV Mode","Output","Aspect","Content","Macrovision");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_AVTEST"]);

var	origDisplayMode=stb.displaymode;
var newDisplayMode;

/* Display Text Definition */
var displaymodedeList=new Array("NTSC","NTSC-JAPAN","PAL-M","PAL-N","PAL-NC","PAL-B","PAL-B1","PAL-D","PAL-D1","PAL","PAL-H","PAL-K","PAL-I","SECAM","480P","576P","1080I","1080I-50HZ","1080P","1080P-24HZ","1080P-25HZ","1080P-30HZ","1250I-50HZ","720P","720P-50HZ","720P-24HZ");
var displaymodeSkip=new Array(18,19,20,21);
//var outputtypeList=new Array("RGB","YPbPr");
//var aspectList=new Array("4:3","16:9");
//var contentList=new Array("Zoom","Box","PanScan","Full","Full-NonLiner");
//var macrovisionList=new Array("Disable","AGC","AGC-2Lines","AGC-4Lines","Custom","AGC-RGB","AGC-2Lines-RGB","AGC-4Lines-RGB");

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_AV").toUpperCase();
	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1
	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbnav";
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?(tourl[0]+"?dispmode="+newDisplayMode):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function checkValidDisp(mod)
{
	for(var i=0;i<displaymodeSkip.length;i++)
	{
		if(displaymodeSkip[i]==newDisplayMode)
			return false;
	}
	return true;
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className="tbnav";
			CurItem=idx;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className="tbnav_over";
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var direction=(key==0x25)?-1:1;
				if ((key==0x25)||(key==0x27))
				{
					switch(idx)
					{
						case 0:
							do
							{
								newDisplayMode=(newDisplayMode+direction+displaymodedeList.length)%(displaymodedeList.length);
							}
							while(!checkValidDisp(newDisplayMode));

							listobj[idx*2+1].innerText=displaymodedeList[newDisplayMode];
							navnextobj.style.visibility=(newDisplayMode!=origDisplayMode)?"inherit":"hidden";
							break;
/*
						case 1:
							stb.outputtype=(stb.outputtype+direction+outputtypeList.length)%(outputtypeList.length);
							listobj[idx*2+1].innerText=outputtypeList[stb.outputtype];
							break;
						case 2:
							stb.aspect=(stb.aspect+direction+aspectList.length)%(aspectList.length);
							listobj[idx*2+1].innerText=aspectList[stb.aspect];
							break;
						case 3:
							stb.content=(stb.content+direction+contentList.length)%(contentList.length);
							listobj[idx*2+1].innerText=contentList[stb.content];
							break;
						case 4:
							stb.macrovision=(stb.macrovision+direction+macrovisionList.length)%(macrovisionList.length);
							listobj[idx*2+1].innerText=macrovisionList[stb.macrovision];
							break;
*/
					}
				}
			break;
	}
}

function saveSetting()
{
}

function initFocus()
{
	listobj[1].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");

	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;


	//Save Original Mode before new mode applied
	newDisplayMode=stb.displaymode;

	var vallist=new Array();
	vallist.push(displaymodedeList[newDisplayMode]);
	//vallist.push(outputtypeList[stb.outputtype]);
	//vallist.push(aspectList[stb.aspect]);
	//vallist.push(contentList[stb.content]);
	//vallist.push(macrovisionList[stb.macrovision]);

	for(var i=0;i<(listobj.length/2);i++)
	{
		var obj=listobj[i*2+1];
		listobj[i*2].innerText=MENULIST[i];
		obj.innerText=vallist[i];

		obj.style.navLeft="#"+obj.id;
		obj.style.navRight="#"+obj.id;

		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
	}

	navnextobj.style.visibility="hidden";

	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;