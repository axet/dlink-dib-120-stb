var MENULIST=new Array("MENU_DVB","MENU_VOD","MENU_IPTV","MENU_CA","MENU_MISC");
var DVBPAGE=new Array(SETUP_PAGES["MENU_DVBT"],SETUP_PAGES["MENU_DVBC"],SETUP_PAGES["MENU_DVBS"]);
var DRMPAGE=new Array(SETUP_PAGES["MENU_CA_VERIMATRIX"],SETUP_PAGES["MENU_CA_SECUREMEDIA"]);
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES[MENULIST[0]],SETUP_PAGES[MENULIST[1]],SETUP_PAGES[MENULIST[2]],SETUP_PAGES[MENULIST[3]],SETUP_PAGES[MENULIST[4]]);
var DVB_ENABLED=((typeof(dvb)=='undefined')||(!dvb)||(dvb.dvb_type==-1))?false:true;
var egg_pwd="1397";
var usr_pwd="";
var buffer_timer=null;

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_SERVER").toUpperCase();
	navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_SERVER"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click

			if((idx==0)&&(DVB_ENABLED))
			{
				var dvb_type=dvb.dvb_type;
				window.location.replace(DVBPAGE[dvb_type]);
			}
			else if(((idx==3)&&(DVB_ENABLED))||((idx==2)&&(!DVB_ENABLED)))
			{
				var drm_type=drm.drm_type;
				window.location.replace(DRMPAGE[drm_type]);
			}
			else
			{
				if(!DVB_ENABLED)
					idx++;
				window.location.replace(tourl[idx]);
			}
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function clearPwdBuffer()
{
	usr_pwd="";
}

function handleKeypress(event)
{
	var key = event.keyCode ? event.keyCode : event.which;

	if(usr_pwd.length>egg_pwd.length)
	{
		if(buffer_timer)
		{
			clearTimeout(buffer_timer);
			buffer_timer=null;
		}
		usr_pwd="";
	}

	if((key>=48)&&(key<=57))
		usr_pwd+=(key-48).toString();
	
	//Egg
	if(usr_pwd==egg_pwd)
	{
		var last=listobj.length-1;
		if(!DVB_ENABLED)
			last--;

		listobj[last].style.visibility="inherit";
		listobj[last-1].style.navDown="#"+listobj[last].id;
		listobj[last].style.navDown="#"+listobj[0].id;
		listobj[0].style.navUp="#"+listobj[last].id;
	}

	if(buffer_timer)
	{
		clearTimeout(buffer_timer);
		buffer_timer=null;
	}
	buffer_timer=setTimeout(clearPwdBuffer,2000);
}

function saveSetting()
{
}

function initFocus()
{
	listobj[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	for(var i=0;i<listobj.length;i++)
	{
		var obj=listobj[i];
		var shiftval=(DVB_ENABLED)?0:1;

		if(i+shiftval>MENULIST.length-1)
			break;

		obj.innerText=GetLangStr(STR_TYPE_MENU,MENULIST[i+shiftval]);

		obj.style.navRight="#"+obj.id;
		obj.style.navLeft="#"+navprevobj.id;
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
	}

	listobj[0].style.navUp="#"+listobj[i-1].id;
	listobj[i-1].style.navDown="#"+listobj[0].id;

	//---Easten Egg
	listobj[i-1].style.visibility="hidden";
	listobj[i-2].style.navDown="#"+listobj[0].id;
	listobj[0].style.navUp="#"+listobj[i-2].id;

	while(i<listobj.length)
	{
		listobj[i].style.visibility="hidden";
		i++;
	}

	update();
	document.addEventListener("keypress",handleKeypress,false);
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;