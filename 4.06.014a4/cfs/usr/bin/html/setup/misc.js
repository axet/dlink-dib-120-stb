var MENULIST=new Array("PAGE_MAIN");
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_MISC").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem].getElementsByTagName("div")[0].className="title";
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_MISC"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem].getElementsByTagName("div")[0].className="title";
			CurItem=idx;
			listobj[CurItem].getElementsByTagName("div")[0].className="title1";
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function saveSetting()
{
	webbrowser.mainpage=listobj[0].getElementsByTagName("input")[0].value;
}

function initFocus()
{
	listobj[0].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	vallist.push(webbrowser.mainpage);


	for(var i=0;i<listobj.length;i++)
	{
		var obj;
		listobj[i].getElementsByTagName("div")[0].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);

		obj=listobj[i].getElementsByTagName("input")[0];
		obj.value=vallist[i];

		obj.style.navRight="#"+obj.id;
		obj.style.navLeft="#"+obj.id;
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
	}
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;