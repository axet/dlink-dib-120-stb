var titleobj,menutitleobj,navprevobj,navnextobj,menuobj;
var CurItem=0;//Cur Focused New Setting
var fromurl=SETUP_PAGES["MENU_SETUP"];
var tourl=new Array(SETUP_PAGES["MENU_SETUP"]);

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_SETUP").toUpperCase();
	menutitleobj.innerText="";
	menuobj.getElementsByTagName("td")[0].innerHTML="<img src='images/info.jpg' width='43' height='43'>";
	menuobj.getElementsByTagName("td")[1].innerText=GetLangStr(STR_TYPE_TEXT,"FUNC_DONE");

	navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_OK");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			break;
		case 1://Click
			if(direction>0)
				saveSetting();

			window.location.replace((direction>0)?tourl[0]:fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}


function saveSetting()
{
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	menutitleobj = document.getElementById("menu_title");
	menutitleobj.lang=LANG_NAME;

	menuobj=document.getElementById("menu_done");
	menuobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	update();
	navnextobj.focus();
}

window.onload=initPage;
window.onunload=exitPage;