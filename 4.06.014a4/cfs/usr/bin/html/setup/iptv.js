var MENULIST=new Array("MEDIA_IPTV","MEDIA_IGMP");
var IGMPLIST=new Array(3,1,2);//(Default(IGMPv3),IGMPv1,IGMPv2);
var listobj,titleobj,navprevobj,navnextobj;
var CurItem=0;
var fromurl=SETUP_PAGES["MENU_SERVER"];
var tourl=new Array(SETUP_PAGES["MENU_DONE"]);
var igmpver=network.igmp;

function update()
{
	titleobj.innerText=GetLangStr(STR_TYPE_MENU,"MENU_IPTV").toUpperCase();

	if(navprevobj)
		navprevobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_PREV");

	if(navnextobj)
		navnextobj.innerText=GetLangStr(STR_TYPE_BUTTON,"BTN_NEXT");
}

function processFunc(event,eventflag,eventtype)
{
	var direction=eventflag;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(CurItem==0)?"tbedit":"tbnav";
			break;
		case 1://Click
			if(direction>0)
				if (saveSetting()!= true)
					break;

			window.location.replace((direction>0)?(tourl[0]+"?from="+SETUP_PAGES["MENU_IPTV"]):fromurl);
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			break;
	}
}

function processItem(event,eventflag,eventtype)
{
	var idx=eventflag;
	var direction=1;// back:-1 next:1

	switch(eventtype)
	{
		case 0://Focus
			listobj[CurItem*2].className="tbtitle";
			listobj[CurItem*2+1].className=(CurItem==0)?"tbedit":"tbnav";
			CurItem=idx;
			listobj[CurItem*2].className="tbtitle_focus";
			listobj[CurItem*2+1].className=(CurItem==0)?"tbedit_over":"tbnav_over";
			break;
		case 1://Click
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			switch(key)
			{
				case 0x25:direction=-1;
				case 0x27:
					switch(idx)
					{
						case 1:
							igmpver=(igmpver+direction+IGMPLIST.length)%(IGMPLIST.length);
							listobj[2*idx+1].innerText=IGMPLIST[igmpver];
							break;
					}
					break;
			}
			break;
		case 3://Mouse Over
			if(typeof(listobj[idx*2+1].getElementsByTagName("input"))=="object")
				listobj[idx*2+1].getElementsByTagName("input")[0].focus();
			break;
	}
}

function saveSetting()
{
	var rv=true;//true:no error  false:error
	var iptvServer = listobj[1].getElementsByTagName("input")[0].value;

	if (iptvServer[0]== '/')
	{	
		rv = false;
		eval('listobj[1].getElementsByTagName("input")[0].focus()');
		alert('iptv server url invalid.');

	}
	else
	{
		webbrowser.iptvServer=listobj[1].getElementsByTagName("input")[0].value;
		network.igmp=igmpver;
	}
	return rv;
}

function initFocus()
{
	listobj[1].getElementsByTagName("input")[0].focus();
}

function exitPage()
{
}

function initPage()
{
	var param_fromurl=GetUrlParamValue(location.search,"from");

	if((param_fromurl)&&(param_fromurl.length>2))
		fromurl=unescape(param_fromurl);

	document.getElementById("setup_menu").lang=LANG_NAME;
	listobj  = document.getElementById("setup_menu").getElementsByTagName("TD");
	titleobj = document.getElementById("page_title");
	titleobj.lang=LANG_NAME;

	navprevobj = document.getElementById("navprev");
	navprevobj.addEventListener("focus", function(event){processFunc(event,-1,0)}, false);
	navprevobj.addEventListener("click", function(event){processFunc(event,-1,1)}, false);
	navprevobj.addEventListener("keypress", function(event){processFunc(event,-1,2)}, false);
	navprevobj.lang=LANG_NAME;

	navnextobj = document.getElementById("navnext");
	navnextobj.addEventListener("focus", function(event){processFunc(event,1,0)}, false);
	navnextobj.addEventListener("click", function(event){processFunc(event,1,1)}, false);
	navnextobj.addEventListener("keypress", function(event){processFunc(event,1,2)}, false);
	navnextobj.lang=LANG_NAME;

	var vallist=new Array();
	vallist.push(webbrowser.iptvServer);
	vallist.push(IGMPLIST[igmpver]);

	for(var i=0;i<(listobj.length/2);i++)
	{
		var obj;
		listobj[i*2].innerText=GetLangStr(STR_TYPE_TITLE,MENULIST[i]);
		if(i==1)
		{
			obj=listobj[i*2+1];
			obj.innerText=vallist[i];
		}
		else
		{
			obj=listobj[i*2+1].getElementsByTagName("input")[0];
			obj.value=vallist[i];
		}

		obj.style.navRight="#"+obj.id;
		obj.style.navLeft="#"+obj.id;
		eval('obj.addEventListener("focus", function(event){processItem(event,'+i+',0)}, false);');
		eval('obj.addEventListener("click", function(event){processItem(event,'+i+',1)}, false);');
		eval('obj.addEventListener("keypress", function(event){processItem(event,'+i+',2)}, false);');
		eval('obj.addEventListener("mouseover", function(event){processItem(event,'+i+',3)}, false);');
	}
	update();
	initFocus();
}

window.onload=initPage;
window.onunload=exitPage;