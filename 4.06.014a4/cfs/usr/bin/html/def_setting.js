/*
    ===========
      CAUTION
	===========
	[start.htm] and [def_setting.js] 's filename and location can't be changed
	it must be putted under iBox's Subdirectory 'html'
*/

var def_html_dir="/usr/bin/html/";
var def_main_page="file://"+def_html_dir+"main.htm";
var def_config_page="file://"+def_html_dir+"setup/setup.htm";
var def_vod_page="file://"+def_html_dir+"vod.htm";
var def_iptv_page="file://"+def_html_dir+"iptv.htm";
var def_dvb_page="file://"+def_html_dir+"dvb.htm";
var def_pvr_page="file://"+def_html_dir+"pvr.htm";
var def_internet_page="about:/blank.htm";

