try{
	var CurFocus=0;//0-Left 1-Right
	//Channel Variable
	var OnePageCountCH=7;
	var CurCHItem=0;
	var epg_ch_start_idx=0;
	//Program Variable
	var CurPGItem=0;
	var EPGCHLST,EPGPGLST;
	var EPGNAV;
	var EPGCHTIMER=null;
	var totalcount=dvb.channels.length;
	var time_inited=false;

function NAVDAY(id,ch_list_obj,prog_list_obj,pgup,pgdown){
	var obj=document.getElementById(id);
	var me=this;

	if((!obj)||(!prog_list_obj))
		return null;

	this.id=id;
	this.navobj=obj;
	this.navprevobj=obj.getElementsByTagName("span")[1];
	this.navdayobj=obj.getElementsByTagName("span")[0];
	this.navnextobj=obj.getElementsByTagName("span")[2];
	this.channels=ch_list_obj;
	this.programs=prog_list_obj;
	this.pgupdate_timer=null;
	this.PGOnePageCount=this.programs.length;//List Count
	this.programs_time=new Array();
	this.programs_name=new Array();
	this.program_start_idx=0;
	this.PG_PREV_PAGE=0;
	this.pgupobj=pgup;
	this.PG_NEXT_PAGE=0;
	this.pgdownobj=pgdown;
	this.daynext=0; //Tomorrow Program Available
	this.dayprev=0; //Yesterday Program Available

	for(var i=0;i<this.PGOnePageCount;i++)
	{
		this.programs_time.push(this.programs[i].getElementsByTagName("span")[0]);
		this.programs_name.push(this.programs[i].getElementsByTagName("span")[1]);
	}

	this.navobj.style.navLeft="#"+this.navobj.id;
	this.navobj.style.navRight="#"+this.navobj.id;
	this.navobj.addEventListener("focus",function(e) {me.focusListener(e) }, false);
	this.navobj.addEventListener("keypress",function(e) {me.keyListener(e) }, false);

	this.ch_no=(totalcount>0)?dvb.channels[0].ch_no:0;
	this.ch_idx=0;
	this.prog_idx=0;
}

NAVDAY.prototype.focus=function(){
	this.navobj.focus();
}

NAVDAY.prototype.show_day_navigator=function(){
	var now= new Date();
	var weekdayname=new Array("Sun.","Mon.","Tue.","Wed.","Thu.","Fri.","Sat.");
	var monthdayname=new Array("Jan.","Feb.","Mar.","Apr.","May.","Jun.","Jul.","Aug.","Sep.","Oct.","Nov.","Dec."); //cyber
	now.setTime(this.start_time);
	now.setHours(0);
	now.setMinutes(0);
	now.setSeconds(0);
	var todaystarttime=now.getTime();
	
	if(this.ch_idx<totalcount)
	{
		this.dayprev=(dvb.channels[this.ch_idx].progs.get_program_count_of_periodtime(todaystarttime/1000,-86400)>0)?1:0;
		this.daynext=(dvb.channels[this.ch_idx].progs.get_program_count_of_periodtime((todaystarttime+86400000)/1000,86400)>0)?1:0;
	}
	else
	{
		this.dayprev=0;
		this.daynext=0;
	}

	var paddingm=" ";//((now.getMonth()+1)<10)?"/0":"/";
	var paddingd="";//((now.getDate())<10)?"/0":"/";

	//this.navdayobj.innerText=(now.getFullYear())+paddingm+(now.getMonth()+1)+paddingd+(now.getDate())+" "+weekdayname[now.getDay()];
	this.navdayobj.innerText=(now.getDate())+paddingm+monthdayname[now.getMonth()]+paddingd+(now.getFullYear())+" "+weekdayname[now.getDay()]; //cyber

	if(this.navprevobj)
		this.navprevobj.className="navleft"+this.dayprev.toString();
	if(this.navnextobj)
		this.navnextobj.className="navright"+this.daynext.toString();

	now=null;
	weekdayname=null;
	monthdayname=null;
	todaystarttime=null;
	paddingm=null;
	paddingd=null;
}

NAVDAY.prototype.focusItem=function(idx){
	if(document.getElementById("pginfo").className=="pginfo1")
		ShowPGInfo(CurPGItem);
}

NAVDAY.prototype.clickItem=function(idx){
	var progidx=EPGNAV.program_start_idx+idx;

	//Check HD Worked?
	if(stb.hd_exist)
	{
		if(progidx<dvb.channels[EPGNAV.ch_idx].progs.length)
		{
			var tmpPG=dvb.channels[EPGNAV.ch_idx].progs[progidx];
			if(tmpPG.isvalid)
			{
				if(!tmpPG.isnowprogram)
				{
					stb.print("OK GO REC");
					if(exitRecConfirm(true))
					{
						var fromurl=(webbrowser.dvbServer!="")?webbrowser.dvbServer:def_dvb_page;
						var prefix=(fromurl.indexOf('?')<0)?'?':'&';
						fromurl += prefix+"ch_id="+tmpPG.ch_id;
						window.location.replace("rec.htm?op=2&ch_no="+tmpPG.ch_no+"&ch_id="+tmpPG.ch_id+"&rectime="+tmpPG.starttime+"&duration="+tmpPG.proglength+"&from="+escape(fromurl));
					}
				}
			}
		}
	}
}

NAVDAY.prototype.nav_to_neighbor=function(direction){
	var starttime=this.start_time;
	var timelength=(direction>0)?86400000:-86400000;
	var trycount=0;
	var progidx=-1;

	if(dvb.channels[this.ch_idx].progs.length>0)
	{
		while(trycount<=14)//2 Weeks
		{
			starttime += timelength;
			progidx=dvb.channels[this.ch_idx].progs.get_program_idx_of_periodtime(starttime/1000,86400);
			if(progidx>=0)
				break;
			trycount +=1;
		}
	}	
	
	if(progidx>=0)//Found
	{
		this.start_time=starttime;
		this.program_start_idx=progidx;
	}
	else
	{	//Still Not Found Use Now Time as start
		var now=new Date();
		this.start_time=now.getTime();
		this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_periodtime(this.start_time/1000,86400);
		this.program_start_idx=(this.program_start_idx>0)?this.program_start_idx:0;
	}
}

NAVDAY.prototype.nav=function(direction){
	var now=new Date();
	var another_now=new Date();

	switch(direction)
	{
		case 100://for auto update /*cyber*/
			if(now.getTime() > (this.start_time+900))
			{
				this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_time();
				this.program_start_idx=(this.program_start_idx<0)?0:this.program_start_idx;
				this.start_time=now.getTime();
			}
			else
			{
				this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_periodtime(this.start_time/1000,86400);
				this.program_start_idx=(this.program_start_idx<0)?0:this.program_start_idx;
			}
			break;
		case -2://Yesterday
			now.setTime(this.start_time);
			now.setHours(0);
			now.setMinutes(0);
			now.setSeconds(0);
			this.start_time=now.getTime();
			this.start_time -= 86400000;
			if(another_now.getTime() > this.start_time) /*come to the first page*//*cyber*/
			{
				//this.nav_to_neighbor(-1);
				this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_time();
				this.program_start_idx=(this.program_start_idx<0)?0:this.program_start_idx;
				this.start_time=another_now.getTime();
				break;
			}
			now.setTime(this.start_time);
			this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_periodtime(this.start_time/1000,86400);
			if(this.program_start_idx<0)
			{
				this.nav_to_neighbor(1);
			}
			break;
		case 0://Today and Now
			this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_time();
			this.program_start_idx=(this.program_start_idx<0)?0:this.program_start_idx;
			this.start_time=now.getTime();
			break;
		case 2://Tomorrow
			now.setTime(this.start_time);
			now.setHours(0);
			now.setMinutes(0);
			now.setSeconds(0);
			this.start_time=now.getTime();
			this.start_time += 86400000;
			now.setTime(this.start_time);
			this.program_start_idx=dvb.channels[this.ch_idx].progs.get_program_idx_of_periodtime(this.start_time/1000,86400);
			if(this.program_start_idx<0)
			{
				this.nav_to_neighbor(-1);
			}
			break;
		case -1://Page Decrease
			if(dvb.channels[this.ch_idx].progs)
			{
				this.program_start_idx -=this.PGOnePageCount;
				if(this.program_start_idx<0)
					this.program_start_idx=0;

				if(dvb.channels[this.ch_idx].progs[this.program_start_idx])
					this.start_time=1000*(dvb.channels[this.ch_idx].progs[this.program_start_idx].starttime);
			}
			break;
		case 1://Page Increase
			if(dvb.channels[this.ch_idx].progs)
			{
				if((this.program_start_idx+this.PGOnePageCount)<(dvb.channels[this.ch_idx].progs.length))
					this.program_start_idx += this.PGOnePageCount;

				if(dvb.channels[this.ch_idx].progs[this.program_start_idx])
					this.start_time=1000*(dvb.channels[this.ch_idx].progs[this.program_start_idx].starttime);
			}
			break;
	}

	now=null;
	another_now=null;
	this.update();
}

NAVDAY.prototype.channelchange=function(ch_idx){
	this.clearPG();
	this.ch_idx=ch_idx;
	this.ch_no=dvb.channels[ch_idx].ch_no;
	var now=new Date();
	this.start_time=now.getTime();//use now as show time;
	//this.nav(0);//0-Now Prgram as start
	this.show_day_navigator();
	now=null;
}

NAVDAY.prototype.show_page_navigator=function (){
	this.pgupobj.className=(this.program_start_idx>0)?"epgpgup1":"epgpgup0";
	this.pgdownobj.className=((dvb.channels[this.ch_idx].progs.length-this.program_start_idx)>this.PGOnePageCount)?"epgpgdown1":"epgpgdown0";
}

NAVDAY.prototype.clearPG=function (){
	for(var i=0;i<this.PGOnePageCount;i++)
	{
		this.programs_time[i].innerText="";
		this.programs_name[i].innerText="";
	}
	this.pgupobj.className="epgpgup0";
	this.pgdownobj.className="epgpgdown0";
}
NAVDAY.prototype.update=function (){
	var last=0;
	var pidx=this.program_start_idx;
	var me=this;
	var epgch=dvb.channels[this.ch_idx];

	this.show_day_navigator();
	this.show_page_navigator();

	for(var i=0;i<this.PGOnePageCount;i++)
	{
		prev = ((i-1)+this.PGOnePageCount)%this.PGOnePageCount;
		next = (i+1)%this.PGOnePageCount;
		if(pidx>=epgch.progs.length)
			break;
		this.programs_time[i].innerText=epgch.progs[pidx].starttimestr;
		this.programs_name[i].innerText=epgch.progs[pidx].ch_name.substr(0,22);
		this.programs[i].style.navUp="#"+this.programs[prev].id;
		this.programs[i].style.navDown="#"+this.programs[next].id;
		pidx ++;
	}

  	var last=(i>1)?i-1:0;
	this.programs[0].style.navUp="#"+this.id;
	this.programs[last].style.navDown="#"+this.programs[0].id;

   while(i < this.PGOnePageCount)
   {
		this.programs_time[i].innerText="";
		if(i==0)
			this.programs_name[i].innerText=GetLangStr(STR_TYPE_TEXT,"EPG_WAITING");
		else
			this.programs_name[i].innerText="";
		i++;
   }
}
NAVDAY.prototype.updatePGList=function(){
	//Update Program List Interval
	if((EPGIsOn())&&(CurFocus==1))
	{
		var origtime=new Date();
		origtime.setTime(EPGNAV.start_time);

		if(origtime.getFullYear()<2007)
		{
			var now=new Date();
			EPGNAV.start_time=now.getTime();
			now=null;
		}
		//EPGNAV.update();
		EPGNAV.nav(100); /*cyber*/
		EPGNAV.show_day_navigator();
		if(document.getElementById("pginfo").className!="pginfo0")
			ShowPGInfo(CurPGItem);
		origtime=null;
	}
}
NAVDAY.prototype.focusListener=function(event){
	document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_NAVDAY");
	HidePGInfo();
}
NAVDAY.prototype.keyListener=function(event){
	var handled=true;
	switch (event.keyCode)
	{
		case 0x26://Now
		    this.nav(0);
		    break;
	  	case 0x27://Tomorrow
	  		if(this.daynext>0)
		    	this.nav(2);
		    break;
	  	case 0x25://Yesterday
	  		if(this.dayprev>0)
		    this.nav(-2);
			break;
		case 0x28:
			document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_PG");
			handled=false;
			break;
		default:
			handled=false;
			break;
  }

	this.show_day_navigator();
	
	return !handled;
}

/* Common Utility*/
function EPG_PAGE_UPDOWN(direction)
{
	if(direction==0)
	{
		if(CurFocus==1)
			EPGNAV.nav(-1);
	}
	else
	{
		if(CurFocus==1)
			EPGNAV.nav(1);
	}
}

function ShowPGInfo(idx)
{
	var chidx=epg_ch_start_idx+CurCHItem;
	var progidx=EPGNAV.program_start_idx+idx;
	var showstate=0;
	if(progidx<dvb.channels[chidx].progs.length)
	{
		if(EPGIsOn())
		{
			showstate=1;
			document.getElementById("pginfo").getElementsByTagName("span")[0].innerText=dvb.channels[chidx].progs[progidx].ch_name.substr(0,15);
			document.getElementById("pginfo").getElementsByTagName("textarea")[0].value=dvb.channels[chidx].progs[progidx].desc;
			if(document.getElementById("pginfo").className!="pginfo1")
				document.getElementById("pginfo").className="pginfo1";
		}
	}
	document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_PGINFO");
	document.getElementById("pginfo").className="pginfo"+(showstate);
	chidx=null;
	progidx=null;
}

function HidePGInfo()
{
	if(document.getElementById("pginfo").className!="pginfo0")
	{
		document.getElementById("pginfo").className="pginfo0";
		document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_PG");
	}
}


function ShowPageCH(){
	var upflag=0,downflag=0;

	if(totalcount>0)
	{
		upflag=(epg_ch_start_idx>0)?1:0;
		if(totalcount>OnePageCountCH)
			downflag=((epg_ch_start_idx+OnePageCountCH)<=(totalcount-1))?1:0;
	}

	document.getElementById("cpgup").className="pgup"+upflag;
	document.getElementById("cpgdown").className="pgdown"+downflag;
}

function WriteListCH(){
	var idx=epg_ch_start_idx;

	if(epg_ch_start_idx<totalcount)
	{
	  for (var i = 0; i < OnePageCountCH; i++)
	  {
			if(idx>=totalcount)
				break;
			var tmptext=dvb.channels[idx].ch_no+" ";
			if(dvb.channels[idx].ch_name.indexOf("empty_channel_name")>=0)
				tmptext += GetLangStr(STR_TYPE_TEXT,"CHANNEL_UNKNOWN");
			else
				tmptext += dvb.channels[idx].ch_name;

			//EPGCHLST[i].lang=dvb.channels[idx].ch_name_lang;
			EPGCHLST[i].innerText=tmptext.substr(0,17);
			idx ++;
	  }
	  
		while(i < OnePageCountCH)
		{
			EPGCHLST[i].innerText="";
	   		i++;
		}

	   ShowPageCH();
    }
}


function navCH(count){
	//Prevent Scroll when no more page

	if(totalcount>0)
	{
		var pagecount=(count<0)?count:OnePageCountCH;
		var countlen=(count<pagecount)?count:2*pagecount;
			
		if(epg_ch_start_idx+countlen<totalcount)
		{
			if(epg_ch_start_idx+pagecount<totalcount)
				epg_ch_start_idx += count;
		}
		else
		{
			epg_ch_start_idx=totalcount-pagecount;
		}
		epg_ch_start_idx=(epg_ch_start_idx<0)?0:epg_ch_start_idx;
	}

	WriteListCH();
}

function processCH(event,eventflag,eventtype)
{
	var idx=eventflag;

	switch(eventtype)
	{
		case 0://Focus
			HidePGInfo();

//			if(EPGCHLST[CurCHItem].className!="item")
//				EPGCHLST[CurCHItem].className="item";
			CurCHItem=idx;
			EPGNAV.ch_idx=epg_ch_start_idx+CurCHItem;

			if(EPGCHTIMER)
			{
				clearTimeout(EPGCHTIMER);
				EPGCHTIMER=null;
			}
			EPGCHTIMER=setTimeout(ChannelChange_Timer,400);
			
			if(CurFocus!=0)
				document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_CH");
			CurFocus=0;
			break;
		case 1://Click
			if(EPGIsOn())
			{
				var chidx=epg_ch_start_idx+CurCHItem;
				if(chidx<=totalcount)
				{
					if(exitRecConfirm())//function in 'dvb_ui.js'
					{
						dvb.play_channel(dvb.channels[chidx].ch_id);
						updateChInfo(dvb.channels[chidx].ch_id);
					}
				}
			}
			break;
		case 2://KeyPress
			var key = event.keyCode ? event.keyCode : event.which;
			var handled=true;
			switch (key)
			{
				case 0x27://RIGHT
					var change_ch_ok=false;
					EPGNAV.ch_idx=epg_ch_start_idx+CurCHItem;
					if(!dvb.channels[EPGNAV.ch_idx].isplaying)
					{
						if(exitRecConfirm())//function in 'dvb_ui.js'
						{
							dvb.play_channel(dvb.channels[EPGNAV.ch_idx].ch_id);
							updateChInfo(dvb.channels[EPGNAV.ch_idx].ch_id);
							change_ch_ok=true;
						}
					}else change_ch_ok=true;
					var now=new Date();
					EPGNAV.start_time=now.getTime();//use now as show time;
					if(EPGNAV.pgupdate_timer)
					{
						clearInterval(EPGNAV.pgupdate_timer);
						EPGNAV.pgupdate_timer=null;
					}
					EPGNAV.pgupdate_timer=setInterval(EPGNAV.updatePGList,3000);//Update Program List Timer Start
					EPGNAV.nav(0);//0-Now Prgram as start
					EPGNAV.show_day_navigator();
					EPGCHLST[CurCHItem].className=(change_ch_ok)?"selected":"item";
					handled=!change_ch_ok;//true- cancel focus to program  false:default focus will move to program
					break;
				case 0x26://Up
					if(idx==0)
					{
						if((epg_ch_start_idx==0)&&(totalcount>OnePageCountCH))
						{
							epg_ch_start_idx=totalcount-OnePageCountCH;
							WriteListCH();
							EPGCHLST[OnePageCountCH-1].focus();
						}
						else
				    		navCH(-1);
				    }
					else
						handled=false;
				    break;
				case 0x28://Down
					if(idx==OnePageCountCH-1)
					{
						if((epg_ch_start_idx==totalcount-OnePageCountCH)&&(totalcount>OnePageCountCH))
						{
							epg_ch_start_idx=0;
							WriteListCH();
							EPGCHLST[0].focus();
						}
						else
						    navCH(1);
					}
					else
						handled=false;
				    break;
	    		case 0x21:/* PAGE UP*/
	    			if(EPGIsOn())
						navCH(-OnePageCountCH);
					else
						handled=false;
	    			break;
	    		case 0x22:/* PAGE DOWN*/
	    			if(EPGIsOn())
						navCH(OnePageCountCH);
					else
						handled=false;
	    			break;
				default:
						handled=false;
			}
			return !handled;
			break;
	}
}

function ChannelChange_Timer(){
	EPGCHTIMER=null;
	EPGNAV.channelchange(epg_ch_start_idx+CurCHItem);
}

/****************************
	Program Functions
*****************************/
function clickPG(e,eventflag)
{
	CurPGItem=eventflag;
	if(EPGIsOn())
		EPGNAV.clickItem(CurPGItem);
}

function focusPG(e,eventflag) {
	// id is a string not number!	
	CurPGItem=eventflag;
	if(CurFocus!=1)
		document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_PG");
	CurFocus=1;
	for(var i=0;i<EPGPGLST.length;i++)
	{
		EPGPGLST[i].style.navLeft="#"+EPGCHLST[CurCHItem].id;
	}
	
	EPGNAV.focusItem(CurPGItem);
}

function keypressPG(e,eventflag)
{
	var key = event.keyCode ? event.keyCode : event.which;
	var idx=eventflag;
	var handled=true;
	switch (key)
	{
    	case 0x27://RIGHT
	   		ShowPGInfo(idx);
	    	break;
		case 0x25://LEFT
			if(document.getElementById("pginfo").className!="pginfo0")
	   			HidePGInfo();
	   		else
	   		{
				EPGCHLST[CurCHItem].className="item";
				if(EPGNAV.pgupdate_timer)
				{
					clearInterval(EPGNAV.pgupdate_timer);
					EPGNAV.pgupdate_timer=null;
				}
	   			handled=false;
	   		}
	    	break;
	    default:
	    	handled=false;
	    	break;
    }
    return !handled;
}

function EPGlocateCh(ch_idx)
{
	EPGCHLST[CurCHItem].className="item";
	if(ch_idx>=0)
	{
		CurCHItem=parseInt(ch_idx % OnePageCountCH);
		epg_ch_start_idx=parseInt(ch_idx/OnePageCountCH)*OnePageCountCH;

		if(totalcount-ch_idx<OnePageCountCH-CurCHItem)
		{
			epg_ch_start_idx=totalcount-OnePageCountCH;
			CurCHItem=ch_idx-epg_ch_start_idx;
		}
		WriteListCH();
		EPGinitFocus();
	}
}

function EPGIsOn()
{
	return (document.getElementById("epgwin").style.visibility=="hidden")?false:true;
}
function initTime(){
	var now=new Date();
	if(now.getFullYear()>2006)
	{
		setTimeout(EPGTicker,1000);
		EPGNAV.start_time=now.getTime();
		EPGNAV.show_day_navigator();
		stb.print("--------Time Inited--------");
		time_inited=true;
	}else alert('Incorrect system time');
	now=null;
}
function EPGShowHide()
{
	try{
	if(!time_inited)
		initTime();

	if(time_inited)
	{
		if(document.getElementById("epgwin").style.visibility=="hidden")
		{
			document.getElementById("epgwin").style.visibility="inherit";
			WriteListCH();
			EPGinitFocus();
		}
		else
		{
			HidePGInfo();
			if(EPGNAV.pgupdate_timer)
			{
				clearInterval(EPGNAV.pgupdate_timer);
				EPGNAV.pgupdate_timer=null;
			}
			EPGCHLST[CurCHItem].className="item";
			document.getElementById("epgwin").style.visibility="hidden";
		}
	}
	}catch(err){stb.print("EPG:"+err);}
}

function EPGinitFocus()
{
	EPGCHLST[CurCHItem].focus();
	document.getElementById("epgtips").innerHTML=GetLangStr(STR_TYPE_TEXT,"EPG_TIPS_CH");
}

function EPGTicker(){
	stb.print("EPGTicker()");
	var now=new Date();
	//var ampm=new Array(" AM"," PM");
	//var ampm_idx=0;
	var hh=now.getHours();
	var s="";
	if(hh<10)
		s+='0';
	s+=hh.toString();
	s+=':';
	var mm=now.getMinutes();
	if(mm<10)
		s+='0';
	s+=mm.toString();

	document.getElementById("epgtime").innerText=s;


	now=null;
	hh=null;
	mm=null;
	s=null;

	//Let Ticker Up in 00 seconds
	setTimeout(EPGTicker,(60-now.getSeconds())*1000);
}

function initEPG() {

	document.getElementById("epgchannels").lang=LANG_NAME;
	EPGCHLST = document.getElementById("epgchannels").getElementsByTagName("UL")[0].getElementsByTagName("LI");
	OnePageCountCH=EPGCHLST.length;

	document.getElementById("epgprogs").lang=LANG_NAME;
	EPGPGLST = document.getElementById("epgprogs").getElementsByTagName("UL")[0].getElementsByTagName("LI");

	for(var i=0;i<OnePageCountCH;i++)
	{
		eval('EPGCHLST['+i+'].addEventListener("focus", function(event){processCH(event,'+i+',0)}, false);');
		eval('EPGCHLST['+i+'].addEventListener("click", function(event){processCH(event,'+i+',1)}, false);');
		eval('EPGCHLST['+i+'].addEventListener("keypress", function(event){return processCH(event,'+i+',2)}, false);');
		EPGCHLST[i].style.navRight="#"+EPGPGLST[0].id;
	}

	EPGCHLST[0].style.navUp="#"+EPGCHLST[0].id;
	EPGCHLST[EPGCHLST.length-1].style.navDown="#"+EPGCHLST[EPGCHLST.length-1].id;

	document.getElementById("epgwin").style.visibility="hidden";

	document.getElementById("pginfo").lang=LANG_NAME;
	document.getElementById("pginfo").getElementsByTagName("textarea")[0].lang=LANG_NAME;

	document.getElementById("epgtips").lang=LANG_NAME;

	//Timer 
	document.getElementById("epgtime").lang=LANG_NAME;
	OnePageCountCH=EPGCHLST.length;

	EPGNAV=new NAVDAY("navdaybg",EPGCHLST,EPGPGLST,document.getElementById("ppgup"),document.getElementById("ppgdown"));
	for(var i=0;i<EPGPGLST.length;i++)
	{
		EPGPGLST[i].style.navRight="#"+EPGPGLST[0].id;
		eval('EPGPGLST['+i+'].addEventListener("focus", function(event){focusPG(event,'+i+')}, false);');
		eval('EPGPGLST['+i+'].addEventListener("click", function(event){clickPG(event,'+i+')}, false);');
		eval('EPGPGLST['+i+'].addEventListener("keypress", function(event){return keypressPG(event,'+i+')}, false);');
	}

	stb.print("initEPG() OK");
}

}catch(err){stb.print(err)}