if [ ! -d /data/share/security ]; then
mkdir -p /data/share/security
fi

if [ ! -f /data/share/security/licstore.hds ]; then
tar -zxf /etc/sysmgmt/drm_key.tgz -C /data/share/security
fi
